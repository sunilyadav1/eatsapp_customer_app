package app.eatsapp.autocomplete;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.FindCurrentPlaceRequest;
import com.google.android.libraries.places.api.net.PlacesClient;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import app.eatsapp.AppController;
import app.eatsapp.AutocompleteAddressAdapter;
import app.eatsapp.R;
import app.eatsapp.autocomplete.adapters.PlacesAutoCompleteAdapter;
import app.eatsapp.autocomplete.listeners.RecyclerItemClickListener;
import app.eatsapp.autocomplete.utility.Constants;


public class PlacesAutoCompleteActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {
    protected GoogleApiClient mGoogleApiClient;

    private static final LatLngBounds BOUNDS_INDIA = new LatLngBounds(
            new LatLng(-0, 0), new LatLng(0, 0));

    private EditText mAutocompleteView;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private PlacesAutoCompleteAdapter mAutoCompleteAdapter;
    ImageView delete;

    RelativeLayout parent_layout;

    Dialog loading;

    public JSONArray jsonArray;
    AutocompleteAddressAdapter addressAdapter;
    ListView listView;
    ArrayList<String> Seq= new ArrayList<String>();
    ArrayList<String>  Names= new ArrayList<String>();
    ArrayList<String> Addresses= new ArrayList<String>();

    StringRequest get_Addresses;

    PlacesClient placesClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppController.getInstance().set_task_bg(this);

        loading = new Dialog(PlacesAutoCompleteActivity.this);
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading.setContentView(R.layout.loading);
        
        buildGoogleApiClient();
        setContentView(R.layout.activity_search);

        Places.initialize(getApplicationContext(), "AIzaSyB7l8i8eWnjEuhts5kw-yyGvkZ6Vl-MyQk");
        placesClient = Places.createClient(this);



        addressAdapter= new AutocompleteAddressAdapter(PlacesAutoCompleteActivity.this,Seq,Names,Addresses,PlacesAutoCompleteActivity.this,true);

        listView=(ListView)findViewById(R.id.listView);
        parent_layout=(RelativeLayout) findViewById(R.id.parent_layout);       
        listView.setAdapter(addressAdapter);

        addressAdapter.notifyDataSetChanged();
  
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("clicled","listView");
            }
        });

        get_Addresses();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Search");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mAutocompleteView = (EditText)findViewById(R.id.autocomplete_places);

        delete=(ImageView)findViewById(R.id.cross);

        mAutoCompleteAdapter =  new PlacesAutoCompleteAdapter(this, R.layout.searchview_adapter,
                mGoogleApiClient, BOUNDS_INDIA);

        mRecyclerView=(RecyclerView)findViewById(R.id.recyclerView);
        mLinearLayoutManager=new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(mAutoCompleteAdapter);
        delete.setOnClickListener(this);
        mAutocompleteView.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,int count) {

                listView.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);

//                if (!s.toString().equals("") && mGoogleApiClient.isConnected()) {
//
//                    mAutoCompleteAdapter.getFilter().filter(s.toString());
//
//                }else if(!mGoogleApiClient.isConnected()){
//
//                    Toast.makeText(getApplicationContext(), Constants.API_NOT_CONNECTED,Toast.LENGTH_SHORT).show();
//                    Log.e(Constants.PlacesTag,Constants.API_NOT_CONNECTED);
//
//                }else if (s.toString().equals(""))
//                {
//                    listView.setVisibility(View.VISIBLE);
//                    mRecyclerView.setVisibility(View.GONE);
//                }

                if (!s.toString().equals("")) {

                    mAutoCompleteAdapter.getFilter().filter(s.toString());
                    mAutoCompleteAdapter.notifyDataSetChanged();

                }else if (s.toString().equals(""))
                {
                    listView.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

            }
        });
        mRecyclerView.addOnItemTouchListener(

                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        final PlacesAutoCompleteAdapter.PlaceAutocomplete item = mAutoCompleteAdapter.getItem(position);
                        final String placeId = String.valueOf(item.placeId);
                        Log.e("TAG", "Autocomplete item selected: " + item.description);
                        /*
                             Issue a request to the Places Geo Data API to retrieve a Place object with additional details about the place.
                         */

                        List<Place.Field> placeFields = Arrays.asList(Place.Field.ADDRESS,Place.Field.LAT_LNG);

                        FetchPlaceRequest fetchPlaceRequest  = FetchPlaceRequest.builder(item.placeId.toString(),placeFields).build();
                        placesClient.fetchPlace(fetchPlaceRequest).addOnSuccessListener(new OnSuccessListener<FetchPlaceResponse>() {
                            @Override
                            public void onSuccess(FetchPlaceResponse fetchPlaceResponse) {

                                Place place = fetchPlaceResponse.getPlace();

                                if(place!=null){

                                    //Do the things here on Click.....

                                    Intent goback = new Intent();
                                    goback.putExtra("Address",place.getAddress());
                                    goback.putExtra("Lat",place.getLatLng().latitude+"");
                                    goback.putExtra("Long",place.getLatLng().longitude+"");
                                    goback.putExtra("name",place.getName());

                                    setResult(RESULT_OK, goback);
                                    finish();

//                                    Toast.makeText(getApplicationContext(),String.valueOf(places.get(0).getLatLng()),Toast.LENGTH_SHORT).show();

                                }else {
                                    Toast.makeText(getApplicationContext(),Constants.SOMETHING_WENT_WRONG,Toast.LENGTH_SHORT).show();
                                }


                            }
                        });

//                        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
//                                .getPlaceById(mGoogleApiClient, placeId);
//                        placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
//                            @Override
//                            public void onResult(PlaceBuffer places) {
//
//                                if(places.getCount()==1){
//
//                                    //Do the things here on Click.....
//
//                                    Intent goback = new Intent();
//                                    goback.putExtra("Address",places.get(0).getAddress());
//                                    goback.putExtra("Lat",places.get(0).getLatLng().latitude+"");
//                                    goback.putExtra("Long",places.get(0).getLatLng().longitude+"");
//                                    goback.putExtra("name",places.get(0).getName());
//
//                                    Log.e("Address",goback.getStringExtra("Address")+"");
//                                    Log.e("Lat",goback.getStringExtra("Lat")+"");
//                                    Log.e("Long",goback.getStringExtra("Long")+"");
//                                    Log.e("name",goback.getStringExtra("name")+"");
//
//                                    Log.e("Address",goback.getExtras().getString("Address")+"");
//                                    Log.e("Lat",goback.getExtras().getString("Lat")+"");
//                                    Log.e("Long",goback.getExtras().getString("Long")+"");
//                                    Log.e("name",goback.getExtras().getString("name")+"");
//
//                                    setResult(RESULT_OK, goback);
//                                    finish();
//
////                                    Toast.makeText(getApplicationContext(),String.valueOf(places.get(0).getLatLng()),Toast.LENGTH_SHORT).show();
//
//                                }else {
//                                    Toast.makeText(getApplicationContext(),Constants.SOMETHING_WENT_WRONG,Toast.LENGTH_SHORT).show();
//                                }
//
//                            }
//                        });
//                        Log.i("TAG", "Clicked: " + item.description);
//                        Log.i("TAG", "Called getPlaceById to get Place details for " + item.placeId);
                    }
                })
        );
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    protected synchronized void buildGoogleApiClient() {
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .addApi(LocationServices.API)
//                .addApi(Places.GEO_DATA_API)
//                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.v("Google API Callback", "Connection Done");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.v("Google API Callback", "Connection Suspended");
        Log.v("Code", String.valueOf(i));
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.v("Google API Callback","Connection Failed");
        Log.v("Error Code", String.valueOf(connectionResult.getErrorCode()));
        Toast.makeText(this, Constants.API_NOT_CONNECTED,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        if(v==delete){
            mAutocompleteView.setText("");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (!mGoogleApiClient.isConnected() && !mGoogleApiClient.isConnecting()){
//            Log.v("Google API","Connecting");
//            mGoogleApiClient.connect();
//        }
    }

    @Override
    public void onPause() {
        super.onPause();
//        if(mGoogleApiClient.isConnected()){
//            Log.v("Google API","Dis-Connecting");
//            mGoogleApiClient.disconnect();
//        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void get_Addresses() {

        loading.show();

        Seq.clear();
        Names.clear();
        Addresses.clear();

        Log.e("get_Addresses_url", "" +getString(R.string.base_url)+getString(R.string.get_Addresses) + AppController.getInstance().sharedPreferences.getString("id",""));

        get_Addresses  = new StringRequest(Request.Method.GET,getString(R.string.base_url)+getString(R.string.get_Addresses) +  AppController.getInstance().sharedPreferences.getString("id",""),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        loading.dismiss();

                        Log.e("response", "" +response);

                            try {
                                jsonArray = new JSONArray(response);

                                String temp_address = "";


                                for (int i = 0;i<jsonArray.length();i++)
                                {
                                    Seq.add(jsonArray.getJSONObject(i).getString("id"));
                                    Names.add(jsonArray.getJSONObject(i).getString("company"));

                                    temp_address = jsonArray.getJSONObject(i).getString("address1")+","+jsonArray.getJSONObject(i).getString("address2")+","
                                            +jsonArray.getJSONObject(i).getString("location0")+","
                                            +jsonArray.getJSONObject(i).getString("location1")+","
                                            +jsonArray.getJSONObject(i).getString("location2")+","
//                                            +jsonArray.getJSONObject(i).getString("zip")+","
                                            +jsonArray.getJSONObject(i).getString("city_state").replace(",",", ")+"-"+jsonArray.getJSONObject(i).getString("zip");

                                    temp_address = temp_address.replaceAll(",,,,",", ");
                                    temp_address = temp_address.replaceAll(",,,",", ");
                                    temp_address = temp_address.replaceAll(",,",", ");
                                    temp_address = temp_address.replaceAll(" ,",", ");

                                    Log.e("temp_address",",,"+temp_address.indexOf(",,"));

                                    Addresses.add(temp_address);

                                }


                                addressAdapter.notifyDataSetChanged();

                            } catch (JSONException e) {

                                Log.e("e",  e.toString());

                            }



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String error_msg="";

                Log.e("error", "" + volleyError.toString());

                loading.dismiss();

                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured, Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error, Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error, Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured, Please try later";
                }

                Toast toast = Toast.makeText(PlacesAutoCompleteActivity.this," "+error_msg+" ",Toast.LENGTH_LONG);
                toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                toast.show();

            }
        }) ;

        AppController.getInstance().getRequestQueue().add(get_Addresses);
    }

    public void build_no_internet_msg() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(PlacesAutoCompleteActivity.this);
        builder.setMessage("Your Internet seems to be turned OFF, do you want to turn it ON?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();

            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));


    }

}
