package app.eatsapp;

import android.app.Application;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
//import com.google.android.gms.location.places.Place;


import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import java.util.Map;

import java.util.ArrayList;

/**
 * Created by developer.nithin@gmail.com
 */
public class PlaceOrder extends AppCompatActivity {

    TextView voucher_discount_value,final_total_cost, use_promo, placeOrder,payCash, Deliverycost, Taxcost,time_proceed,delivery_time_mins,or,select_delivery_time,discount1,discount2,food_total,net_order;
    PlaceOrderAdapter placeOrderAdapter;
    ListView listView;
    RelativeLayout test;
    Calendar calendar;

    ArrayList<String> Seq = new ArrayList<String>();
    ArrayList<String> Names = new ArrayList<String>();
    ArrayList<String> Prices = new ArrayList<String>();

    Dialog promo_popup;

    Dialog delivery_time_popup;

    TextView apply;
    EditText value;

    int min_h,min_m;

    LayoutInflater inflater;

    StringRequest check_coupon_StringRequest,check_order_statusRequest;

    Dialog loading;

    LinearLayout linearlayout;

    String coupoun = "", coupoun_id = "0";
    int coupoun_value = 0;

    Boolean coupoun_applied = false;

    String order_json;

    StringRequest insert_order;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SimpleDateFormat sdf_date = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat time_h = new SimpleDateFormat("HH");
    SimpleDateFormat time_m = new SimpleDateFormat("mm");
    SimpleDateFormat time_a = new SimpleDateFormat("a");

    TimePicker timePicker;

    RelativeLayout delivery_charge_layout;
    RelativeLayout discount2_layout,discount1_layout,voucher_discount,tax_layout;

    String target_string;

    Toast timeToast = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        target_string = getString(R.string.Rs) + " ";

        Log.e("travel_time_mins",AppController.getInstance().travel_time_mins);

//        Toast.makeText(PlaceOrder.this,"Travel : "+AppController.getInstance().travel_time_mins + " mins",Toast.LENGTH_LONG).show();

        Log.e("place order","v12");

        AppController.getInstance().set_task_bg(this);

        setContentView(R.layout.place_order);
        AppController.getInstance().jio_order_id="0";
        inflater = getLayoutInflater();

        LinearLayout listFooterView = (LinearLayout) inflater.inflate(
                R.layout.place_order_footer, null);

        calendar = Calendar.getInstance();

        delivery_time_popup = new Dialog(this);
        delivery_time_popup.requestWindowFeature(Window.FEATURE_NO_TITLE);
        delivery_time_popup.setContentView(R.layout.time_selection);
        delivery_time_popup.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);

        or =(TextView)delivery_time_popup.findViewById(R.id.or);
        select_delivery_time =(TextView)delivery_time_popup.findViewById(R.id.select_delivery_time);
        time_proceed =(TextView)delivery_time_popup.findViewById(R.id.time_proceed);
        delivery_time_mins =(TextView)delivery_time_popup.findViewById(R.id.delivery_time_mins);
        timePicker =(TimePicker) delivery_time_popup.findViewById(R.id.timePicker);

        loading = new Dialog(this);
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading.setContentView(R.layout.loading);

        net_order = (TextView) listFooterView.findViewById(R.id.net_order);
        food_total = (TextView) listFooterView.findViewById(R.id.food_total);
        discount1_layout = (RelativeLayout) listFooterView.findViewById(R.id.discount1_layout);
        tax_layout = (RelativeLayout) listFooterView.findViewById(R.id.tax_layout);
        discount2_layout = (RelativeLayout) listFooterView.findViewById(R.id.discount2_layout);
        voucher_discount = (RelativeLayout) listFooterView.findViewById(R.id.voucher_discount);
        discount1 = (TextView) listFooterView.findViewById(R.id.discount1);
        discount2 = (TextView) listFooterView.findViewById(R.id.discount2);
        voucher_discount_value = (TextView) listFooterView.findViewById(R.id.voucher_discount_value);
        Deliverycost = (TextView) listFooterView.findViewById(R.id.Deliverycost);
        delivery_charge_layout = (RelativeLayout) listFooterView.findViewById(R.id.delivery_charge_layout);
        Taxcost = (TextView) listFooterView.findViewById(R.id.Taxcost);
        linearlayout = (LinearLayout) listFooterView.findViewById(R.id.linearlayout);


        AppController.getInstance().discount1_value = (AppController.getInstance().total_cost*AppController.getInstance().discount1/100);
        AppController.getInstance().net_order = AppController.getInstance().total_cost-AppController.getInstance().discount1_value-AppController.getInstance().discount2;

        Deliverycost.setText(getString(R.string.Rs) + AppController.getInstance().delivery_charge);
        Taxcost.setText(getString(R.string.Rs) + (AppController.getInstance().getGST()));
        discount1.setText("- "+getString(R.string.Rs) +" "+AppController.getInstance().discount1_value);
        discount2.setText("- "+getString(R.string.Rs) +" "+ (AppController.getInstance().discount2) );
        food_total.setText(getString(R.string.Rs) +" "+AppController.getInstance().total_cost);
        net_order.setText(getString(R.string.Rs) +" "+AppController.getInstance().net_order);

        if(AppController.getInstance().discount1_value==0)
        {
            discount1_layout.setVisibility(View.GONE);
        }

        if(AppController.getInstance().discount2==0)
        {
            discount2_layout.setVisibility(View.GONE);
        }

        if(AppController.getInstance().getGST()==0)
        {
            tax_layout.setVisibility(View.GONE);
        }



        promo_popup = new Dialog(this);
        promo_popup.requestWindowFeature(Window.FEATURE_NO_TITLE);
        promo_popup.setContentView(R.layout.apply_voucher);
        apply = (TextView) listFooterView.findViewById(R.id.apply);
        value = (EditText) listFooterView.findViewById(R.id.value);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Review Order");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Seq = getIntent().getExtras().getStringArrayList("final_ids");
        Names = getIntent().getExtras().getStringArrayList("final_name");
        Prices = getIntent().getExtras().getStringArrayList("final_price");

        Log.e("Seq","Seq");
        Log.e("Seq",Seq.toString());
        Log.e("Seq","Seq");

        test = (RelativeLayout) findViewById(R.id.test);

        String temp_name="";
        for(int i =0;i<Names.size();i++)
        {
            temp_name = Names.get(i).toString();
            temp_name = temp_name.replace(", |","|");
            temp_name= temp_name.replace(" ,",", ");
            Log.e("temp_name",temp_name);

            Names.set(i,temp_name);
        }

//
//        Seq.add(Seq.size()+"");
//        Names.add("Delivery Charge");
//        Prices.add("40");
//
//        Seq.add(Seq.size()+"");
//        Names.add("Tax");
//        Prices.add((AppController.getInstance().total_cost*6/100)+"");

        placeOrderAdapter = new PlaceOrderAdapter(this, Seq, Names, Prices);

        final_total_cost = (TextView) findViewById(R.id.final_total_cost);
//        final_total_cost.setText(getString(R.string.Rs) + " " + ((AppController.getInstance().total_cost + 40) + AppController.getInstance().total_cost * 5 / 100));
        final_total_cost.setText(getString(R.string.Rs) + " " + ((AppController.getInstance().net_order + AppController.getInstance().delivery_charge)+(AppController.getInstance().getGST())));

        listView = (ListView) findViewById(R.id.listView);
        placeOrder = (TextView) findViewById(R.id.placeOrder);
        payCash = (TextView) findViewById(R.id.payCash);
        use_promo = (TextView) findViewById(R.id.use_promo);
        listView.addFooterView(listFooterView);
        listView.setAdapter(placeOrderAdapter);

        placeOrderAdapter.notifyDataSetChanged();

        payCash.setVisibility(View.GONE);
        placeOrder.setVisibility(View.GONE);

        if(AppController.getInstance().payment_mode.equals("1"))
        {
            payCash.setVisibility(View.VISIBLE);

        }else if(AppController.getInstance().payment_mode.equals("2"))
        {
            placeOrder.setVisibility(View.VISIBLE);

        }else if(AppController.getInstance().payment_mode.equals("3"))
        {
            payCash.setVisibility(View.VISIBLE);
            placeOrder.setVisibility(View.VISIBLE);
        }

        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("click","test");
                Log.e("jio_order_id",AppController.getInstance().jio_order_id+"");

                delivery_time_popup.show();

            }
        });

        use_promo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!coupoun_applied) {

                    promo_popup.show();

                } else {

//                    Toast.makeText(PlaceOrder.this, "Only one voucher is allowed.", Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(PlaceOrder.this, " Only one voucher is allowed. ",Toast.LENGTH_SHORT);
                    toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                    toast.show();

                }
            }
        });

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!coupoun_applied) {
                    coupoun = value.getText().toString();
                    promo_popup.dismiss();
                    loading.show();
                    check_coupon();
                }
            }
        });

        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {

                Log.e("hourOfDay",hourOfDay+"");

                //minute<min_m || hourOfDay<min_h

                if( (hourOfDay<min_h) ||  (hourOfDay<=min_h && minute<min_m))
                {
//                    Toast.makeText(PlaceOrder.this,"Time cant be less than minimum delivery time",Toast.LENGTH_SHORT).show();

                    if(AppController.getInstance().option_selected!=0)
                    {
                        if(timeToast==null)
                        {
                            timeToast = Toast.makeText(PlaceOrder.this,"  Time cant be less than min delivery time  ",Toast.LENGTH_SHORT);
                        }

                        timeToast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                        timeToast.show();

                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                try
                                {
                                    Thread.sleep(3000);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            timeToast.cancel();
                                        }
                                    });

                                }catch (Exception e)
                                {
                                    Log.e("Exception",e.toString());
                                }
                            }
                        }).start();
                    }


                    if(Build.VERSION.SDK_INT==Build.VERSION_CODES.M)
                    {
                        timePicker.setHour(min_h);
                        timePicker.setMinute(min_m);

                    }else
                    {
                        timePicker.setCurrentHour(min_h);
                        timePicker.setCurrentMinute(min_m);
                    }

                }


                if( (hourOfDay>AppController.getInstance().totime_hr) ||  (hourOfDay>=AppController.getInstance().totime_hr && minute>AppController.getInstance().totime_min)  )
                {
//                    Toast.makeText(PlaceOrder.this,"Time cant be less than minimum delivery time",Toast.LENGTH_SHORT).show();

                    if(AppController.getInstance().option_selected!=0)
                    {
                        Toast toast = Toast.makeText(PlaceOrder.this,"  Time cant be more than store closing time  ",Toast.LENGTH_SHORT);
                        toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                        toast.show();
                    }


                    if(Build.VERSION.SDK_INT==Build.VERSION_CODES.M)
                    {
                        timePicker.setHour(min_h);
                        timePicker.setMinute(min_m);

                    }else
                    {
                        timePicker.setCurrentHour(min_h);
                        timePicker.setCurrentMinute(min_m);
                    }

                }

            }
        });

        time_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Log.e("option_selected",""+AppController.getInstance().option_selected);
                Log.e("getCurrentHour",""+timePicker.getCurrentHour());
                Log.e("totime_hr",""+AppController.getInstance().totime_hr);
                Log.e("getCurrentMinute",""+timePicker.getCurrentMinute());
                Log.e("totime_min",""+AppController.getInstance().totime_min);


                if( AppController.getInstance().option_selected!=0 && ((timePicker.getCurrentHour()>AppController.getInstance().totime_hr) ||  (timePicker.getCurrentHour()>=AppController.getInstance().totime_hr && timePicker.getCurrentMinute()>AppController.getInstance().totime_min)))
                {
//                    Toast.makeText(PlaceOrder.this,"Time cant be less than minimum delivery time",Toast.LENGTH_SHORT).show();

                        AppController.getInstance().show_popup_alert("Sorry! Your order cannot be processed because preparation time is more than the store closing time.",PlaceOrder.this);

                }else
                {

                    AppController.getInstance().coupoun_id = coupoun_id;

                    String delivered_on="" ,keep_ready="";

                    Log.e("travel time",AppController.getInstance().travel_time);

                    calendar.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
                    calendar.set(Calendar.MINUTE, timePicker.getCurrentMinute());

                    calendar.getTime();

                    Log.e("calendar.getTime()",calendar.getTime().toString());

                    delivered_on = sdf.format(calendar.getTimeInMillis());

                    if(AppController.getInstance().option_selected==0)
                    {
                        int travel_time_mins = Integer.parseInt(AppController.getInstance().travel_time_mins);
                        int pitstopTravelTime = Integer.parseInt(AppController.getInstance().pitstopTravelTime);
                        int difference = Math.abs(travel_time_mins - pitstopTravelTime);

                        keep_ready = sdf.format(Calendar.getInstance().getTimeInMillis()+(Math.abs(difference-5)*60*1000));
                        delivered_on = sdf.format(Calendar.getInstance().getTimeInMillis()+(pitstopTravelTime*60*1000));

                    }else if(AppController.getInstance().option_selected==2)
                    {
                        keep_ready = sdf.format(calendar.getTimeInMillis()-(5*60*1000));

                    }else
                    {
                        keep_ready = sdf.format(calendar.getTimeInMillis()-(Integer.parseInt(AppController.getInstance().travel_time_mins)*60*1000));
                    }

                    order_json = "{\n" +
                            "\t\"user_id\": \"" + AppController.getInstance().sharedPreferences.getString("id", "") + "\",\n" +
                            "\t\"restaurant_id\": \"" + AppController.getInstance().restaurant_id + "\",\n" +
                            "\t\"shipping\": \"" + AppController.getInstance().delivery_charge + "\",\n" +
                            "\t\"photo\": \"" + 0 + "\",\n" +
                            "\t\"reimb\": \"" + (AppController.getInstance().reimb) + "\",\n" +
                            "\t\"commission\": \"" + (AppController.getInstance().commission) + "\",\n" +
                            "\t\"penalty\": \"" + (AppController.getInstance().penalty) + "\",\n" +
                            "\t\"del_partner_penalty\": \"" + (AppController.getInstance().del_partner_penalty) + "\",\n" +
                            "\t\"tax\": \"" + (AppController.getInstance().taxWithoutVoucher) + "\",\n" +
                            "\t\"discount1\": \"" + (AppController.getInstance().discount1_value) + "\",\n" +
                            "\t\"discount2\": \"" + (AppController.getInstance().discount2) + "\",\n" +
                            "\t\"payment\": \"" + (AppController.getInstance().paymentMode) + "\",\n" +
                            "\t\"total_amount\": \"" + (AppController.getInstance().total_cost) + "\",\n" +
                            "\t\"net_order_value\": \"" + (AppController.getInstance().net_order) + "\",\n" +
                            "\t\"coupon_id\": \"" +AppController.getInstance().coupoun_id + "\",\n" +
                            "\t\"pitstop_id\": \"" +AppController.getInstance().pitstop_id + "\",\n" +
                            "\t\"coupon_discount\": \"" + coupoun_value + "\",\n" +
                            "\t\"delivered_on\": \"" +delivered_on+ "\",\n" +
                            "\t\"keep_ready\": \"" + keep_ready + "\",\n" +
                            "\t\"order_type\": \"" + (AppController.getInstance().option_selected + 1) + "\",\n" +
                            "\t\"total_cost\": \"" + ((AppController.getInstance().net_order + AppController.getInstance().delivery_charge +Double.parseDouble(AppController.getInstance().taxWithoutVoucher))) + "\",\n" +
                            "\t\"shipping_lat\": \"" + AppController.getInstance().delivery_location.getLatitude() + "\",\n" +
                            "\t\"shipping_long\": \"" + AppController.getInstance().delivery_location.getLongitude() + "\",\n" +
                            "\t\"shipping_address\": \"" + AppController.getInstance().full_address + "\",\n" +

                            "\t\"products\": [";

                    for (int i = 0; i < Seq.size(); i++) {
                        order_json = order_json + "{\n" +
                                "\t\t\"menu_id\": \"" + Seq.get(i) + "\",\n" +
                                "\t\t\"cost\": \"" +  Prices.get(i) + "\",\n" +
                                "\t\t\"contents\": \"" +  Names.get(i) + "\",\n" +
                                "\t\t\"quantity\": \"" + "1" + "\"\t\t\n" +
                                "\t}";
                        if (i != Seq.size() - 1) {
                            order_json = order_json + ",";
                        }
                    }

                    order_json = order_json + "]}";


                    Log.e("order_json",order_json);
                    Log.e("taxWithoutVoucher",AppController.getInstance().taxWithoutVoucher);

                    try {

                        AppController.getInstance().order_json_object = new JSONObject(order_json);

                    } catch (JSONException e) {

                        Log.e("JSONException", e.toString());
                    }


                    if (AppController.getInstance().option_selected != 1) {

//                    insert order
                        if (!ConnectivityReceiver.isConnected()) {
//                        Toast.makeText(PlaceOrder.this, "Not Internet Connection", Toast.LENGTH_LONG);
                            Toast toast = Toast.makeText(PlaceOrder.this," Not Internet Connection ",Toast.LENGTH_LONG);
                            toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                            toast.show();
                            build_no_internet_msg();
                        } else {

                            insert_order();
                        }


                    }else {

                        //

                        startActivityForResult(new Intent(PlaceOrder.this,PhotoUpload.class).putExtra("type","selfie"),11);
                    }


                    delivery_time_popup.dismiss();

                }
            }
        });


        payCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AppController.getInstance().paymentMode = 1;
                placeOrder();
            }
        });

        placeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppController.getInstance().paymentMode = 0;
                placeOrder();

            }
        });


        if(AppController.getInstance().option_selected==2)
        {
            delivery_charge_layout.setVisibility(View.GONE);
        }

        if(AppController.getInstance().option_selected==0) {

            String msg = "";

            int travel_time_mins = Integer.parseInt(AppController.getInstance().travel_time_mins);
            int pitstopTravelTime = Integer.parseInt(AppController.getInstance().pitstopTravelTime);
            int difference = Math.abs(travel_time_mins - pitstopTravelTime);

            msg = "Customer Travel Time " + AppController.getInstance().pitstopTravelTime +
                    "\nDelivery boy Travel Time " + AppController.getInstance().travel_time_mins +
                    "\nX-Y-5 = " + Math.abs(difference - 5)+
                    "\n" ;

//            AppController.getInstance().show_popup_alert(msg, PlaceOrder.this);
        }

    }

    private void placeOrder() {

        Log.e("click","place order");

        AppController.getInstance().total_time=10;

        Log.e("click","total "+AppController.getInstance().total_time+" travel "+Integer.parseInt(AppController.getInstance().travel_time_mins));

        if(AppController.getInstance().option_selected==2) {

            AppController.getInstance().total_time = AppController.getInstance().total_time;

        }else
        {
            AppController.getInstance().total_time = AppController.getInstance().total_time+Integer.parseInt(AppController.getInstance().travel_time_mins);

        }



        Log.e("itemPreparation_time",AppController.getInstance().itemPreparation_time.toString());
        Log.e("Seq","Seq");
        Log.e("Seq",Seq.toString());
        Log.e("Seq","Seq");

        for (int i = 0; i < Seq.size(); i++) {

            AppController.getInstance().total_time=AppController.getInstance().total_time+AppController.getInstance().itemPreparation_time.get(Seq.get(i));

        }

        if(AppController.getInstance().option_selected==0)
        {
            int travel_time_mins = Integer.parseInt(AppController.getInstance().travel_time_mins);
            int pitstopTravelTime = Integer.parseInt(AppController.getInstance().pitstopTravelTime);
            int difference = Math.abs(travel_time_mins - pitstopTravelTime);

            AppController.getInstance().keep_ready=Math.abs(difference- 5);



        }else
        {
            AppController.getInstance().keep_ready=AppController.getInstance().total_time-Integer.parseInt(AppController.getInstance().travel_time_mins);

        }


        Log.e("total_time",AppController.getInstance().total_time+"");
        Log.e("keep_ready",AppController.getInstance().keep_ready+"");
        Log.e("currentTimeMillis",java.lang.System.currentTimeMillis()+"");

        if(AppController.getInstance().option_selected==0)
        {
            delivery_time_mins.setText("Order will reach the Delivery Point in "+AppController.getInstance().pitstopTravelTime+" mins\n");

        }else if(AppController.getInstance().option_selected==2)
        {
            delivery_time_mins.setText("Order can be picked up in "+AppController.getInstance().total_time+" mins\n");
            select_delivery_time.setText("Select pick up time");

        }else
        {
            delivery_time_mins.setText("Order will be delivered in "+AppController.getInstance().total_time+" mins");
        }

        int temp_hr = AppController.getInstance().totime_hr;
        int temp_min = AppController.getInstance().totime_min;
        String am_pm = " AM";


        String display_time =  temp_hr+"";

        if(temp_hr>12)
        {
            temp_hr = temp_hr-12;
            display_time =  temp_hr+"";
            am_pm = " PM";
        }

        if(temp_min<10)
        {
            display_time = display_time +":0"+temp_min;

        }else
        {
            display_time = display_time +":"+temp_min;
        }

        display_time = display_time + am_pm;

        select_delivery_time.setText(select_delivery_time.getText().toString()+" ( before "+display_time+" )");

        //date and time conversion from minutes
        AppController.getInstance().total_time = AppController.getInstance().total_time*60*1000;
        AppController.getInstance().keep_ready = AppController.getInstance().keep_ready*60*1000;

        min_h= Integer.parseInt(time_h.format(AppController.getInstance().total_time+java.lang.System.currentTimeMillis()));
        min_m = Integer.parseInt(time_m.format(AppController.getInstance().total_time+java.lang.System.currentTimeMillis()));

        if(Build.VERSION.SDK_INT==Build.VERSION_CODES.M)
        {
            timePicker.setHour(min_h);
            timePicker.setMinute(min_m);


        }else
        {
            timePicker.setCurrentHour(min_h);
            timePicker.setCurrentMinute(min_m);
        }

        if(!AppController.getInstance().jio_order_id.equals("0"))
        {
            String url = getString(R.string.jio_url)+"purchase.php?order_id="+AppController.getInstance().jio_order_id+"&amount="+(final_total_cost.getText().toString().replace(target_string,""))+".00&ph="+AppController.getInstance().sharedPreferences.getString("phone","");
            Log.e("url",url);

            Log.e("jio_call","tag 2");

            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            CustomTabsIntent customTabsIntent = builder.build();

            builder.setToolbarColor(ContextCompat.getColor(PlaceOrder.this,R.color.white));
            builder.setShowTitle(false);

            customTabsIntent.intent.setPackage("com.android.chrome");

            customTabsIntent.launchUrl(PlaceOrder.this, Uri.parse(url));

        }else
        {
            delivery_time_popup.show();

            if(AppController.getInstance().option_selected!=0)
            {
                select_delivery_time.setVisibility(View.VISIBLE);
                or.setVisibility(View.VISIBLE);

            }else {

                select_delivery_time.setVisibility(View.GONE);
                or.setVisibility(View.GONE);
                timePicker.setVisibility(View.GONE);

            }

        }
    }


    private void insert_order() {

        Log.e("insert_order_url", "" +getString(R.string.base_url)+getString(R.string.ordersinsert));

        loading.show();
        insert_order   = new StringRequest(Request.Method.POST,getString(R.string.base_url)+getString(R.string.ordersinsert),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        loading.dismiss();

                        Log.e("insert_order response", "" +response);


                        if(response.contains("Success"))
                        {
                            try {

                                JSONObject jsonObject = new JSONObject(response);

                                AppController.getInstance().jio_order_id = jsonObject.getString("order_id");
                                AppController.getInstance().Passcode = jsonObject.getString("Passcode");

                                Log.e("total_cost",AppController.getInstance().total_cost+"");
                                Log.e("getGST",+AppController.getInstance().getGST()+"");

                                Log.e("total_cost + getGST",+(AppController.getInstance().total_cost +AppController.getInstance().getGST())+"");

                                if(AppController.getInstance().paymentMode==1) {

                                    startActivityForResult(new Intent(PlaceOrder.this, FinalScreen.class).putExtra("Passcode",AppController.getInstance().Passcode),11);

                                }else
                                {
                                    String url = getString(R.string.jio_url) + "purchase.php?order_id=" + AppController.getInstance().jio_order_id + "&amount=" + (final_total_cost.getText().toString().replace(target_string, "")) + ".00&ph=" + AppController.getInstance().sharedPreferences.getString("phone", "");
                                    Log.e("url", url);

                                    Log.e("jio_call", "tag 1");
                                    Log.e("jio_call", "gst val" + AppController.getInstance().getGST());


                                    CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                                    CustomTabsIntent customTabsIntent = builder.build();

                                    builder.setToolbarColor(ContextCompat.getColor(PlaceOrder.this, R.color.white));
                                    builder.setShowTitle(false);

                                    customTabsIntent.intent.setPackage("com.android.chrome");

                                    customTabsIntent.launchUrl(PlaceOrder.this, Uri.parse(url));
                                }

                            } catch (JSONException e) {
                                Log.e("response", "" +e.toString());
                            }

                        }else
                        {
//                            Toast.makeText(PlaceOrder.this,"Your order couldnt be procesed at this time",Toast.LENGTH_LONG);
                            Toast toast = Toast.makeText(PlaceOrder.this," Your order couldnt be procesed at this time ",Toast.LENGTH_LONG);
                            toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                            toast.show();

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String error_msg="";

                Log.e("error", "" + volleyError.toString());

                loading.dismiss();

                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured, Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error, Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error, Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured, Please try later";
                }

//                Toast.makeText(PlaceOrder.this,error_msg, Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(PlaceOrder.this," "+error_msg+" ",Toast.LENGTH_LONG);
                toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                toast.show();


            }
        }){

            @Override
            public byte[] getBody() throws com.android.volley.AuthFailureError {

                Log.e("order_json", "" +order_json.toString());
//                Log.e("order_json.getBytes()", "" +order_json.getBytes());


                return order_json.getBytes();
            };

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/text");
                headers.put("charset", "TYPE_UTF8_CHARSET");

                Log.e("headers", "" +headers.toString());
                return headers;
            }
        };

        String url = getString(R.string.jio_url)+"purchase.php?order_id="+AppController.getInstance().jio_order_id+"&amount="+(final_total_cost.getText().toString().replace(target_string,""))+".00&ph="+AppController.getInstance().sharedPreferences.getString("phone","");
        Log.e("url",url);

        insert_order = (StringRequest) insert_order.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 6000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 0;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
                Log.e("error", error.toString());
            }
        });



        AppController.getInstance().getRequestQueue().add(insert_order);

        Log.e("insert_order", "" +insert_order.toString());
    }

    public void build_no_internet_msg() {


//            final AlertDialog.Builder builder = new AlertDialog.Builder(PlaceOrder.this);
//            builder.setMessage("Your GPS seems to be disabled, do you want to enable it?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
//
//                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
//                }
//            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
//                public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
//                    dialog.cancel();
//
//                }
//            });
//            final AlertDialog alert = builder.create();
//            alert.show();
//            alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
//            alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));


    }

    private void check_coupon() {

        Log.e("check_coupon_url", "" +getString(R.string.base_url) + getString(R.string.coupon));

         check_coupon_StringRequest = new StringRequest(Request.Method.POST, getString(R.string.base_url) + getString(R.string.coupon),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        loading.dismiss();

                        Log.e("responses", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.e("jsonObject.get(id)", "" + jsonObject.get("id"));

                            if (!jsonObject.get("id").equals("0")) {
                                coupoun_value = Integer.parseInt(jsonObject.get("cost") + "");
                                coupoun_id = jsonObject.get("id") + "";

                                final_total_cost.setText(getString(R.string.Rs) + " " + (((AppController.getInstance().net_order + AppController.getInstance().delivery_charge+(AppController.getInstance().getGST()))) - coupoun_value));

                                AppController.getInstance().net_order = AppController.getInstance().net_order - coupoun_value;
                                net_order.setText(getString(R.string.Rs) +" "+AppController.getInstance().net_order);
                                coupoun_applied = true;

                                Log.e("coupoun_applied", "" + coupoun_applied);
//                                    Toast.makeText(PlaceOrder.this, getString(R.string.Rs) + " "+coupoun_value+" Coupon has been Applied", Toast.LENGTH_SHORT).show();
                                Toast toast = Toast.makeText(PlaceOrder.this," "+ getString(R.string.Rs) + " "+coupoun_value+" Coupon has been Applied"+" ",Toast.LENGTH_SHORT);
                                toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                                toast.show();

                                voucher_discount.setVisibility(View.VISIBLE);
                                voucher_discount_value.setText("- "+ getString(R.string.Rs) + " " +coupoun_value );

//                                Taxcost.setText(getString(R.string.Rs) + (AppController.getInstance().getGST()));

                            } else {

                                loading.dismiss();
                                Log.e("Invalid Coupon", "" + coupoun_applied);
//                                Toast.makeText(PlaceOrder.this, "Invalid Coupon", Toast.LENGTH_SHORT).show();
                                Toast toast = Toast.makeText(PlaceOrder.this, " Invalid Coupon ",Toast.LENGTH_SHORT);
                                toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                                toast.show();
                            }

                        } catch (JSONException e) {
                            Log.e("e", "" + e.toString());

                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                loading.dismiss();

                String error_msg = "";

                Log.e("error", "" + volleyError.toString());

                loading.dismiss();

                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError) {
                    error_msg = "Error Occured, Please try later";

                } else if (volleyError instanceof ServerError) {
                    error_msg = "Server Error, Please try later";

                } else if (volleyError instanceof NetworkError) {
                    error_msg = "Network Error, Please try later";

                } else if (volleyError instanceof ParseError) {
                    error_msg = "Error Occured, Please try later";
                }

                Snackbar snackbar = Snackbar
                        .make(linearlayout, error_msg, Snackbar.LENGTH_LONG);
                snackbar.show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // TODO Auto-generated method stub

                Map<String, String> params = new HashMap<String, String>();

                params.put("coupon_code", coupoun);

                Log.e("params", "" + params.toString());


                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        AppController.getInstance().getRequestQueue().add(check_coupon_StringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==11 && resultCode==RESULT_OK && data!=null )
        {
            if(data.getExtras().getString("order").equals("done"))
            {
                setResult(RESULT_OK,new Intent().putExtra("order","done"));
                finish();
            }

        }else if(requestCode==12)
        {
            if(resultCode==RESULT_OK )
            {
                check_order_status();

            }else
            {
                AppController.getInstance().show_popup_alert("Your transaction was not completed.Please try again",PlaceOrder.this);
            }

        }
    }

    private void check_order_status() {

        Log.e("check_order_status_url", "" +getResources().getString(R.string.base_url)+"GetOrderStatus/"+AppController.getInstance().jio_order_id);


//        http://domainname/index.php/api/GetOrderStatus/8 -> to get order status

//        Returning order_id and order_number in insert order api

        loading.show();

        check_order_statusRequest  = new StringRequest(Request.Method.GET, getResources().getString(R.string.base_url)+"GetOrderStatus/"+AppController.getInstance().jio_order_id,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("response from server", response);
                        loading.dismiss();

                        if(response.contains("Payment pending"))
                        {

                            AppController.getInstance().show_popup_alert_without_title("Your payment was not successful.",PlaceOrder.this);


                        }else if(response.contains("Order Placed"))
                        {
                                final AlertDialog.Builder builder = new AlertDialog.Builder(PlaceOrder.this);
                                builder.setMessage("Your order was sucessfuly placed.").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                                        dialog.dismiss();
                                        startActivityForResult(new Intent(PlaceOrder.this, FinalScreen.class).putExtra("Passcode",AppController.getInstance().Passcode),11);


                                    }
                                });
                                final AlertDialog alert = builder.create();
                                alert.setCancelable(false);
                                alert.show();

                        }else
                        {
                            AppController.getInstance().show_popup_alert_without_title("Info : "+AppController.getInstance().jio_order_id+" \n "+response,PlaceOrder.this);

                        }



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                loading.dismiss();

                String error_msg="";

                Log.e("error", "" + volleyError.toString());


                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured, Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error, Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error, Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured, Please try later";
                }
//
//                Snackbar snackbar = Snackbar
//                        .make(parent_layout, error_msg, Snackbar.LENGTH_LONG) ;
//                snackbar.show();

            }
        });

        AppController.getInstance().getRequestQueue().add(check_order_statusRequest);


    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("onresume","place order");
        if(!AppController.getInstance().jio_order_id.equals("0")) {
            check_order_status();
        }
    }
}
