package app.eatsapp;


/**
 * Created by developer.nithin@gmail.com
 */


import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

public class RestaurantAdapter extends BaseAdapter {
    Activity activity;

    ArrayList<String> Seq;
    ArrayList<String> Names;
    ArrayList<String> Times;
    ArrayList<String> Photo_Url;
    LayoutInflater inflater;
    ImageLoader il;
    String imgaes_base_path;

    public RestaurantAdapter(Activity activity, ArrayList<String> Seq, ArrayList<String> Names, ArrayList<String> Times,ArrayList<String>  Photo_Url) {
        // TODO Auto-generated constructor stub
        this.activity = activity;
        this.Seq = Seq;
        this.Names = Names;
        this.Times = Times;
        this.Photo_Url = Photo_Url;
        il= AppController.getInstance().getImageLoader();

        imgaes_base_path = activity.getResources().getString(R.string.imgaes_base_path);


    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return Seq.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int id) {
        // TODO Auto-generated method stub
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2) {
        // TODO Auto-generated method stub
        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.restaurant_cell, null);
        }

        NetworkImageView photo = (NetworkImageView) convertView.findViewById(R.id.photo);
        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView time = (TextView) convertView.findViewById(R.id.time);

        name.setText(Names.get(position));
        if(Times.get(position).equals("na"))
        {
            time.setVisibility(View.GONE);
        }else {
            time.setText(Times.get(position));
        }

        photo.setDefaultImageResId(R.drawable.default_banner);
        photo.setErrorImageResId(R.drawable.default_banner);
        photo.setImageUrl(imgaes_base_path+Photo_Url.get(position).replace("uploads/images/thumbnails/",""),il);

        return convertView;
    }

}