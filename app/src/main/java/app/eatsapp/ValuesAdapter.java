package app.eatsapp;

import android.app.Activity;
        import android.content.Context;
        import android.graphics.Color;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.BaseAdapter;
        import android.widget.CheckBox;
        import android.widget.CompoundButton;
        import android.widget.LinearLayout;
        import android.widget.RadioButton;
        import android.widget.RelativeLayout;
        import android.widget.TextView;

        import com.android.volley.toolbox.NetworkImageView;

        import java.util.ArrayList;

/**
 * Created by developer.nithin@gmail.com
 */
public class ValuesAdapter  extends BaseAdapter implements View.OnClickListener {


    Activity activity;
    LayoutInflater inflater;

    ArrayList<String> customization_values ;
    ArrayList<Boolean> customization_values_selected ;


    public ValuesAdapter(Activity activity, ArrayList<String> customization_values, ArrayList<Boolean> customization_values_selected)
    {
        this.activity=activity;
        this.customization_values=customization_values;
        this.customization_values_selected=customization_values_selected;

        Log.e(" ValuesAdapter","version 9 ");

    }

    @Override
    public int getCount() {
        return customization_values.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {



        if(inflater == null)
        {
            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if(convertView ==null)
        {
            if(AppController.getInstance().customization_category.contains("Toppings") || AppController.getInstance().customization_category.contains("Add-ons"))
            {
                convertView = inflater.inflate(R.layout.customization_values_checkbox, null);
                convertView.setTag("check");

            }else
            {
                convertView = inflater.inflate(R.layout.customization_values, null);
                convertView.setTag("radio");
            }

        }else
        {
            Log.e("customization_category",AppController.getInstance().customization_category);
            Log.e("getTag",convertView.getTag().toString());

            if((AppController.getInstance().customization_category.contains("Toppings") || AppController.getInstance().customization_category.contains("Add-ons")) && !convertView.getTag().toString().equals("check"))
            {
                convertView = inflater.inflate(R.layout.customization_values_checkbox, null);
                convertView.setTag("check");

            }else if(!(AppController.getInstance().customization_category.contains("Toppings") || AppController.getInstance().customization_category.contains("Add-ons")) &&!convertView.getTag().toString().equals("radio"))
            {
                convertView = inflater.inflate(R.layout.customization_values, null);
                convertView.setTag("radio");
            }
        }

        if(AppController.getInstance().customization_category.contains("Toppings") || AppController.getInstance().customization_category.contains("Add-ons"))
        {
            CheckBox checkBox =(CheckBox)convertView.findViewById(R.id.radio);
            checkBox.setTag(position+"");

            checkBox.setText(customization_values.get(position));

            if(customization_values_selected.get(position))
            {
                checkBox.setChecked(true);

            }else
            {
                checkBox.setChecked(false);
            }

        }else
        {
            RadioButton checkBox =(RadioButton)convertView.findViewById(R.id.radio);
            checkBox.setTag(position+"");

            checkBox.setText(customization_values.get(position));

            if(customization_values_selected.get(position))
            {
                checkBox.setChecked(true);

            }else
            {
                checkBox.setChecked(false);
            }
        }


        LinearLayout row =(LinearLayout)convertView.findViewById(R.id.row);

        row.setTag(position+"");

        row.setOnClickListener(this);



        return convertView;
    }


    @Override
    public void onClick(View v) {

//        Log.e("sel_cus 1", AppController.getInstance().selected_customization.toString());
//        Log.e("current category", AppController.getInstance().customization_category);

//        Log.e(" ValuesAdapter","radio button clicked ");
//        Log.e(" ValuesAdapter","selected customization_category"+AppController.getInstance().customization_category);

        int position = Integer.parseInt(v.getTag().toString());


        if(AppController.getInstance().customization_category.contains("Toppings") || AppController.getInstance().customization_category.contains("Add-ons"))
        {
            if( customization_values_selected.get(position))
            {
                customization_values_selected.set(position,false);

            }else {
                customization_values_selected.set(position,true);
            }

            notifyDataSetChanged();

//            Log.e("selected_customization","before"+  AppController.getInstance().selected_customization.toString() );
//            Log.e("customization_values",customization_values.get(position));

            int sum=0;
            String selected_topping="",current_topping="";

            for(int i=0;i<customization_values.size();i++)
            {
//                Log.e("TTT","customization_values_selected"+customization_values_selected.get(i));
                if(customization_values_selected.get(i))
                {
                    current_topping=customization_values.get(i).toString();
                    selected_topping = selected_topping + current_topping.substring(0,current_topping.indexOf("Rs."))+",";
//                    Log.e("TTT","current_topping"+current_topping);
//                    Log.e("TTT","selected_topping"+selected_topping);
//                    Log.e("TTT","sum"+(current_topping.substring(current_topping.indexOf("Rs.")+3,current_topping.length())));
                    sum = sum+Integer.parseInt((current_topping.substring(current_topping.indexOf("Rs.")+3,current_topping.length())));
                }


            }

            Log.e("TTT","selected_topping"+selected_topping);
            Log.e("TTT","total cost"+sum);

            Log.e("selected_customization","before"+ AppController.getInstance().selected_customization.toString() );

            AppController.getInstance().selected_customization.put( AppController.getInstance().customization_category,selected_topping+" Rs."+sum);

            Log.e("selected_customization","after"+ AppController.getInstance().selected_customization.toString() );

//            ((Menus)activity).update_custimizations(AppController.getInstance().customization_category);

        }else
        {

            for(int i=0;i<customization_values_selected.size();i++)
            {
                customization_values_selected.set(i,false);
            }

            customization_values_selected.set(position,true);

            notifyDataSetChanged();

            Log.e("selected_customization","before"+  AppController.getInstance().selected_customization.toString() );
            Log.e("customization_values",customization_values.get(position));

            AppController.getInstance().selected_customization.put( AppController.getInstance().customization_category,customization_values.get(position));

            Log.e("selected_customization","after"+ AppController.getInstance().selected_customization.toString() );


            if(AppController.getInstance().customization_category.equalsIgnoreCase("size"))
            {
                AppController.getInstance().current_selected_size =  AppController.getInstance().current_sizes.get(position);
            }



            Log.e("current_selected_size",AppController.getInstance().current_selected_size);

            ((Menus)activity).update_custimizations(AppController.getInstance().customization_category);

        }

    }
}
