package app.eatsapp;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by developer.nithin@gmail.com
 */
public class TermsAdapter  extends BaseAdapter {
    Activity activity;

    ArrayList<String> list;

    LayoutInflater inflater;

    public TermsAdapter(Activity activity, ArrayList<String> list ) {
        // TODO Auto-generated constructor stub
        this.activity = activity;
        this.list = list;


    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int id) {
        // TODO Auto-generated method stub
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2) {
        // TODO Auto-generated method stub
        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.terms_cell, null);
        }

        TextView item = (TextView) convertView.findViewById(R.id.text1);



        item.setText(list.get(position));



        return convertView;
    }

}