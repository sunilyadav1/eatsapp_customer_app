package app.eatsapp;

/**
 * Created by developer.nithin@gmail.com
 */
import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by hp on 29/9/2016.
 */
public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        Log.e("onMessageReceived","onMessageReceived");

        try
        {
            Log.e("onMessageReceived",remoteMessage.getData().get("msg").toString());

            if(!remoteMessage.getData().get("msg").toString().equals("logout"))
            {
                AppController.getInstance().create_notification(remoteMessage.getData().get("msg").toString());

            }else {

                AppController.getInstance().verification = false;
                AppController.getInstance().sharedPreferences_editor.putBoolean("login",false);
                AppController.getInstance().sharedPreferences_editor.commit();

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent. FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }


        }catch (Exception e)
        {
            Log.e("Exception",e.toString());
        }

    }


}
