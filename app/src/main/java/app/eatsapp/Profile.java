package app.eatsapp;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class Profile extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    StringRequest get_profile,save_profile_Request;

    TextInputLayout input_layout_name;
    TextInputLayout input_layout_email;
    TextInputLayout input_layout_age;

    Boolean from_server=false;

    TextView input_name;
    TextView input_email;
    TextView input_age;

    RadioGroup gender_radio_group;

    View fragment_view;

    String gender_value="";

    RadioButton gender_male,gender_female;

    Dialog loading;

    LinearLayout parent_layout;

    DatePicker datePicker;

    Dialog d;

    TextView ok;

    String from_value;

    String server_date="0000-00-00";


    Button save_profile;

    public Profile() {
        // Required empty public constructor
    }

    public static Profile newInstance(String param1, String param2) {
        Profile fragment = new Profile();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragment_view = inflater.inflate(R.layout.fragment_profile, container, false);


        d = new Dialog(getActivity());
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.date);

        datePicker = (DatePicker) d.findViewById(R.id.datePicker);
        ok = (TextView) d.findViewById(R.id.ok);


        loading = new Dialog(getActivity());
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading.setContentView(R.layout.loading);

         parent_layout=(LinearLayout)fragment_view.findViewById(R.id.parent_layout);

        if(parent_layout!=null)
        {
            Log.e("not","null");
        }else
        {
            Log.e("was","null");
        }


        input_name=(TextView)fragment_view.findViewById(R.id.input_name);
        input_email =(TextView)fragment_view.findViewById(R.id.input_email );
        input_age =(TextView)fragment_view.findViewById(R.id.input_age );

        from_value = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

        input_age.setText("" + new SimpleDateFormat("dd/MM/yyyy").format(new Date()));


        input_layout_name=(TextInputLayout) fragment_view.findViewById(R.id.input_layout_name);
        input_layout_email =(TextInputLayout) fragment_view.findViewById(R.id.input_layout_email);
        input_layout_age =(TextInputLayout) fragment_view.findViewById(R.id.input_layout_age);

        gender_radio_group=(RadioGroup)fragment_view.findViewById(R.id.gender_radio_group);
        gender_male=(RadioButton) fragment_view.findViewById(R.id.gender_male);
        gender_female=(RadioButton)fragment_view.findViewById(R.id.gender_female);

        input_name.addTextChangedListener(new MyTextWatcher(input_name));
        input_email.addTextChangedListener(new MyTextWatcher(input_email));
        input_age.addTextChangedListener(new MyTextWatcher(input_age));

        save_profile=(Button)fragment_view.findViewById(R.id.save_profile);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        input_name.clearFocus();

        input_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void afterTextChanged(Editable editable) {

                String newline = System.getProperty("line.separator");

                if(input_name.getText().toString().contains(newline))
                {
                    input_name.setText(input_name.getText().toString().replace(newline,""));
                    input_email.requestFocus();
                }
            }
        });

        gender_radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if(checkedId==R.id.gender_male)
                {
                    gender_value="male";

                }else  if(checkedId==R.id.gender_female)
                {
                    gender_value="female";
                }

                Log.e("gender_value",gender_value);


            }
        });


        ok.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View arg0)
            {
                // TODO Auto-generated method stub
                String month,day;

                if((datePicker.getMonth() + 1)<10)
                {
                    month="0"+(datePicker.getMonth() + 1);
                }else
                {
                    month=""+(datePicker.getMonth() + 1);
                }

                if(datePicker.getDayOfMonth()<10)
                {
                    day="0"+datePicker.getDayOfMonth();
                }else
                {
                    day=""+datePicker.getDayOfMonth();
                }

                    input_age.setText("" + day + "/" + month + "/" + datePicker.getYear());
                    from_value = datePicker.getYear() + "-" + month + "-" + day;

                d.dismiss();


            }

        });



        input_age.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                Log.e("touch","yes");

                d.show();

                Log.e("new date", Integer.parseInt(from_value.substring(0,4))+" "+ Integer.parseInt(from_value.substring(5,7))+" "+ Integer.parseInt(from_value.substring(8,10)));
                datePicker.updateDate(Integer.parseInt(from_value.substring(0,4)), (Integer.parseInt(from_value.substring(5,7))-1), Integer.parseInt(from_value.substring(8,10)));


                return true;
            }
        });


        save_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!ConnectivityReceiver.isConnected())
                {
                    Snackbar snackbar = Snackbar
                            .make(parent_layout, "No internet connection!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                    build_no_internet_msg();

                }else
                {
                    if(validateName() & validateAge() & validateEmail())
                    {
                        save_profile();
                    }

                }

            }
        });


        if(!ConnectivityReceiver.isConnected())
        {
//          Toast.makeText(getActivity(),"No Internet Connection",Toast.LENGTH_SHORT).show();
            Toast toast = Toast.makeText(getActivity()," No Internet Connection ",Toast.LENGTH_SHORT);
            toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
            toast.show();
            build_no_internet_msg();

        }else
        {
            get_profile();
        }

        return fragment_view;
    }

    public void build_no_internet_msg() {



        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Your Internet seems to be turned OFF, do you want to turn it ON?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();

            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));


    }


    private void get_profile() {

        Log.e("get_profile_url", "" +getResources().getString(R.string.base_url)+"customer/"+AppController.getInstance().sharedPreferences.getString("id",""));

        loading.show();

        get_profile  = new StringRequest(Request.Method.GET, getResources().getString(R.string.base_url)+"customer/"+AppController.getInstance().sharedPreferences.getString("id","") ,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {



                        Log.e("response from erver", response);

                        try
                        {
                            JSONObject jsonObject = new JSONObject(response);
                            input_name.setText(jsonObject.getString("firstname"));


                            String date_string = jsonObject.getString("dob");

//                            date_string="1234-56-79";

                            Log.e("YEAR",Integer.parseInt(date_string.substring(0,date_string.indexOf("-")))+"");
                            Log.e("MONTH", Integer.parseInt(date_string.substring(date_string.indexOf("-")+1,date_string.lastIndexOf("-")))+"");
                            Log.e("DAY_OF_MONTH",Integer.parseInt(date_string.substring(date_string.lastIndexOf("-")+1,date_string.length()))+"");



                            if(!date_string.equals("0000-00-00"))
                            {
                                from_value = date_string;
                                input_age.setText(Integer.parseInt(date_string.substring(date_string.lastIndexOf("-")+1,date_string.length()))+"/"+Integer.parseInt(date_string.substring(date_string.indexOf("-")+1,date_string.lastIndexOf("-")))+"/"+ Integer.parseInt(date_string.substring(0,date_string.indexOf("-")))+"");

                            }

                            if(!jsonObject.getString("email").isEmpty())
                            {
                                input_email.setText(jsonObject.getString("email"));
                            }

                            if(!jsonObject.getString("email").isEmpty())
                            {
                                input_email.setText(jsonObject.getString("email"));
                            }

                            if(!jsonObject.getString("gender").isEmpty())
                            {
                                if(jsonObject.getString("gender").equals("male"))
                                {
                                    gender_radio_group.check(R.id.gender_male);
                                }else
                                {
                                    gender_radio_group.check(R.id.gender_female);
                                }
                            }

                            loading.dismiss();


                        } catch (JSONException e)
                        {
                            // TODO Auto-generated catch block
                            Log.e("JSONException", e.toString());

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                loading.dismiss();

                String error_msg="";

                Log.e("error", "" + volleyError.toString());


                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured, Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error, Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error, Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured, Please try later";
                }

                Snackbar snackbar = Snackbar
                        .make(parent_layout, error_msg, Snackbar.LENGTH_LONG) ;
                snackbar.show();

            }
        });

//        get_profile.setRetryPolicy(new DefaultRetryPolicy(10 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().getRequestQueue().add(get_profile);
    }

    private void save_profile() {

        Log.e("save_profile_url", "" +getResources().getString(R.string.base_url)+""+getString(R.string.update));

        loading.show();

        //incomplete change the url for updating the profile
        save_profile_Request  = new StringRequest(Request.Method.POST, getResources().getString(R.string.base_url)+""+getString(R.string.update),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        loading.dismiss();

                        Log.e("response", "" +response);

                        if(response.contains("true"))
                        {
                            AppController.getInstance().sharedPreferences_editor.putString("firstname",input_name.getText().toString());
                            AppController.getInstance().sharedPreferences_editor.putString("email",input_email.getText().toString());
                            AppController.getInstance().sharedPreferences_editor.commit();

//                            Toast.makeText(getActivity(),"Profile Updated",Toast.LENGTH_LONG).show();
                            Toast toast = Toast.makeText(getActivity()," Profile Updated ",Toast.LENGTH_LONG);
                            toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                            toast.show();

                            getActivity().onBackPressed();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                loading.dismiss();

                String error_msg="";

                Log.e("error", "" + volleyError.toString());


                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured,Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error,Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error,Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured,Please try later";
                }

                Snackbar snackbar = Snackbar
                        .make(parent_layout, error_msg, Snackbar.LENGTH_LONG) ;
                snackbar.show();

            }
        }){
            @Override
            protected Map<String, String> getParams()
            {
                // TODO Auto-generated method stub

                Map<String, String> params = new HashMap<String, String>();

                params.put(getResources().getString(R.string.sl),""+AppController.getInstance().sharedPreferences.getString("id",""));
                params.put(getResources().getString(R.string.Name),""+input_name.getText().toString());
                params.put(getResources().getString(R.string.mail),""+input_email.getText().toString());
                params.put(getResources().getString(R.string.date_of_birth),""+input_age.getText().toString());
                params.put(getResources().getString(R.string.user_gender),""+gender_value);

                Log.e("params", "" + params.toString());


                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        AppController.getInstance().getRequestQueue().add(save_profile_Request);
    }

    private boolean validateName() {
        if (input_name.getText().toString().length() == 0) {
            input_layout_name.setError("Enter your name");
            requestFocus(input_name);
            return false;
        } else {
            input_layout_name.setErrorEnabled(false);
        }

        return true;
    }


    private boolean validateEmail() {
        if (input_email.getText().toString().length() == 0) {
            input_layout_email.setError("Enter your Email");
            requestFocus(input_email);
            return false;
        } else {
            input_layout_email.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateAge() {
        if (input_age.getText().toString().length() == 0) {
            input_layout_age.setError("Enter your Date of Birth");
            requestFocus(input_layout_age);
            return false;
        } else {
            input_layout_age.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_name:
                    validateName();
                    break;
                case R.id.input_email:
                    validateEmail();
                    break;
                case R.id.input_age:
                    validateAge();
                    break;

            }
        }

    }
}
