package app.eatsapp;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by developer.nithin@gmail.com
 */
public class AppController extends Application {

    String error_msg = "";

    StringRequest verify_or_add_user_request;
    private static AppController mInstance;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    public Boolean check_sms = false, verification = false;
    public String verification_code = "", restaurant_id = "";
    public SharedPreferences sharedPreferences;
    SharedPreferences.Editor sharedPreferences_editor;
    Boolean changed = false, opened = false;
    LruBitmapCache lruBitmapCache;
    HashMap<String, Integer> quantity = new HashMap<String, Integer>();
    HashMap<String, Integer> final_quantity = new HashMap<String, Integer>();
    HashMap<String, Integer> itemPreparation_time = new HashMap<String, Integer>();
    int n_items = 0, total_cost = 0;
    Boolean veg = false;
    int option_selected;
    Location delivery_location = new Location("delivery_location");
    String full_address, coupoun_id, yourBitmap_string = "0", travel_time, travel_time_mins,pitstopTravelTime;
    Bitmap yourBitmap = null;
    JSONObject order_json_object = null;
    Boolean added = false;
    int total_time = 0, keep_ready = 0;

    int no_of_notifications = 0;
    NotificationCompat.Builder builder;
    Intent go_to_notifications;
    PendingIntent pendingIntent;
    Uri alarmSound;
    NotificationManager nm;

    public String token_value;
    public String customization_category = "", current_item = "", current_item_name = "";
    Boolean token = false, valid = false;

    String pitstop_id = "0";
    int delivery_charge = 0, servicetax = 0;
    int minordervalue = 0, discount1 = 0, discount2 = 0, discount1_value = 0, net_order = 0, voucher_amount = 0;
    String payment_mode;
    int reimb = 0, commission = 0, penalty = 0,del_partner_penalty=0;
    int totime_hr = 0, totime_min = 0;

    HashMap<String, String> customization = new HashMap<String, String>();
    HashMap<String, String> selected_customization = new HashMap<String, String>();
    ArrayList<MenuItem> cart = new ArrayList<MenuItem>();
    int current_value = 0;
    public Menu menu;
    String jio_order_id = "", Passcode = "";

    String current_selected_size = "";
    ArrayList<String> current_sizes = new ArrayList<>();
    String taxWithoutVoucher;

    DecimalFormat decimalFormat = new DecimalFormat("########");

    int paymentMode = 0;

    String collection_point_latitude = "", collection_point_langitude = "";

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();

        mInstance = this;
        lruBitmapCache = new LruBitmapCache();
        sharedPreferences = getSharedPreferences("basic_parameters", 0);
        sharedPreferences_editor = sharedPreferences.edit();

        go_to_notifications = new Intent(this, Notifications.class);
        builder = new NotificationCompat.Builder(this);
        nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

    }

    public AppController() {
        super();
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }


    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());

        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue, lruBitmapCache);
        }


        return this.mImageLoader;
    }


    public void set_login(Boolean value) {
        // TODO Auto-generated method stub
        sharedPreferences_editor.putBoolean("login", true);
        sharedPreferences_editor.commit();


    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }


    public void sendNotification(String title, String messageBody) {

        Intent intent = null;
        intent = new Intent(this, Notifications.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_stat_ic_notification_a)
                        .setColor(getResources().getColor(R.color.colorAccent))
                        .setContentTitle(title)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        notificationBuilder.setStyle(new NotificationCompat.BigTextStyle()
                .bigText(messageBody));

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel(channelId,
                    title,
                    NotificationManager.IMPORTANCE_HIGH);
            nm.createNotificationChannel(channel);
        }

        no_of_notifications++;
        nm.notify(no_of_notifications, notificationBuilder.build());
    }


    public void create_notification(String data) {
        // TODO Auto-generated method stub

        sendNotification("eatsapp",data);

//        no_of_notifications++;
//
//        // Use NotificationCompat.Builder to set up our notification.
//
//        // icon appears in device notification bar and right hand corner of
//        // notification
//
////        builder.setSmallIcon(R.mipmap.ic_launcher);
//        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
//        builder.setSmallIcon(R.mipmap.ic_launcher);
//        builder.setColor(Color.parseColor("#d32f2f"));
////        builder.setColor(Color.parseColor("#ffffff"));
//
//        // This intent is fired when notification is clicked
//        go_to_notifications.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//
//        pendingIntent = PendingIntent.getActivity(this, 0, go_to_notifications, 0);
//
//        // Set the intent that will fire when the user taps the notification.
//        builder.setContentIntent(pendingIntent);
//
//        // Large icon appears on the left of the notification
//        // builder.setLargeIcon(BitmapFactory.decodeResource(getResources(),
//        // R.drawable.ic_launcher));
//
//        // Content title, which appears in large type at the top of the
//        // notification
//        builder.setContentTitle("eatsapp");
//        builder.setContentText(data);
////        builder.setSubText("You have " + no_of_notifications + " notifications");
//
//        // Content text, which appears in smaller text below the title
////        if(no_of_notifications==1)
////        {
////
////            builder.setSubText(data);
////
////        }else {
////            builder.setContentText("You have " + no_of_notifications + " notifications");
////        }
//
//        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(data).setBigContentTitle("Eatsapp"));
//
//        // The subtext, which appears under the text on newer devices.
//        // This will show-up in the devices with Android 4.2 and above only
//        // builder.setSubText("Tap to view documentation about notifications.");
//
//        builder.setAutoCancel(true);
//
//        builder.setLights(Color.BLUE, 500, 500);
//
//        long[] pattern = {200, 200, 100, 100};
//        builder.setVibrate(pattern);
//
////        builder.setStyle(new NotificationCompat.InboxStyle());
//        alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        if (alarmSound != null) {
//            builder.setSound(alarmSound);
//        }
//
//        // Will display the notification in the notification bar
//        nm.notify(0, builder.build());

    }

    void register_user() {


        if (token && valid) {

            Verification.getInstance().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Verification.getInstance().d.show();

                }
            });

            Log.e("verify_add_usr_req_url", "" + getResources().getString(R.string.base_url) + "customercheck");


            verify_or_add_user_request = new StringRequest(Request.Method.POST, getResources().getString(R.string.base_url) + "customercheck", new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    // TODO Auto-generated method stub

                    Log.e("response", response);

                    try {

                        JSONObject jsonObject = new JSONObject(response);

                        AppController.getInstance().sharedPreferences_editor.putString("id", jsonObject.getString("id"));
                        AppController.getInstance().sharedPreferences_editor.putString("email", jsonObject.getString("email"));

                        if (jsonObject.has("firstname")) {
                            AppController.getInstance().sharedPreferences_editor.putString("firstname", jsonObject.getString("firstname"));
                        }

                        AppController.getInstance().sharedPreferences_editor.commit();

                        AppController.getInstance().set_login(true);

                        Verification.getInstance().finish_activity();


                    } catch (Exception e) {
                        Verification.getInstance().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Verification.getInstance().d.dismiss();

                            }
                        });
                        Log.e("e", e.toString());
                    }

                    Verification.getInstance().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Verification.getInstance().d.dismiss();

                        }
                    });

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    // TODO Auto-generated method stub


                    Log.e("error", "" + volleyError.toString());

                    Verification.getInstance().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Verification.getInstance().d.dismiss();

                        }
                    });

                    if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {
                        error_msg = "No Internet Connection";
                        Verification.getInstance().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Verification.getInstance().build_no_internet_msg();

                            }
                        });

                    } else if (volleyError instanceof AuthFailureError) {
                        error_msg = "Error Occured,Please try later";

                    } else if (volleyError instanceof ServerError) {
                        error_msg = "Server Error,Please try later";

                    } else if (volleyError instanceof NetworkError) {
                        error_msg = "Network Error,Please try later";

                    } else if (volleyError instanceof ParseError) {
                        error_msg = "Error Occured,Please try later";
                    }

                    Verification.getInstance().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Snackbar snackbar = Snackbar
                                    .make(Verification.getInstance().linearlayout, error_msg, Snackbar.LENGTH_LONG);
                            snackbar.show();

                        }
                    });


                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    // TODO Auto-generated method stub
                    Map<String, String> params = new HashMap<String, String>();

                    params.put(getString(R.string.Name), AppController.getInstance().sharedPreferences.getString("firstname", ""));
                    params.put(getString(R.string.phone_number), AppController.getInstance().sharedPreferences.getString("phone", ""));
                    params.put(getString(R.string.token), AppController.getInstance().token_value);


                    Log.e("params", "" + params.toString());


                    return params;

                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    // TODO Auto-generated method stub
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    return params;

                }

            };

            Log.e("request", "sent");

            verify_or_add_user_request.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 10000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 0;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {
                    Log.e("error", error.toString());
                }
            });


            AppController.getInstance().getRequestQueue().add(verify_or_add_user_request);

        }
    }

    public void show_popup_alert(String msg, Context activity) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(msg).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                dialog.dismiss();
            }
        });
        final AlertDialog alert = builder.create();
        alert.setTitle("Message");
        alert.show();
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
    }

    public void show_popup_alert_without_title(String msg, Context activity) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(msg).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                dialog.dismiss();
            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
    }


    public int getGST() {

        double GST;

        Log.e("servicetax", "" + AppController.getInstance().servicetax);
        Log.e("total_cost", "" + AppController.getInstance().net_order);
        Log.e("*", "" + ((float) (AppController.getInstance().servicetax * AppController.getInstance().net_order)));
        Log.e("/", "" + ((float) (AppController.getInstance().servicetax * AppController.getInstance().net_order)) / 100);

        GST = Math.ceil(((float) (AppController.getInstance().servicetax * AppController.getInstance().net_order)) / 100);

        Log.e("GST", "" + GST);

        taxWithoutVoucher = GST+"";

        return (int) GST;
    }


    public void set_task_bg(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            ActivityManager.TaskDescription taskDesc = new ActivityManager.TaskDescription(getString(R.string.app_name), bm, getResources().getColor(R.color.white));
            activity.setTaskDescription(taskDesc);
        }

    }
}
