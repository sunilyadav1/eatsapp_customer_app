package app.eatsapp;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.Picasso;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * Created by developer.nithin@gmail.com
 */
public class Splash extends Activity {

    TextView name;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        AppController.getInstance().set_task_bg(this);

        imageView = (ImageView) findViewById(R.id.imageView);
        Picasso.with(Splash.this).load(R.drawable.eatsapp_icon)
                .resize(getResources().getDisplayMetrics().widthPixels,0)
                .into(imageView);

        name = (TextView) findViewById(R.id.name);
        Typeface fontRobo = Typeface.createFromAsset(getAssets(), "fonts/GOTHIC.TTF");
        name.setTypeface(fontRobo);

        name.setText(Html.fromHtml("<p style='letter-spacing:20px;'><font color='#d32f2f'>eats</font><font  color='#4285f4'>app</font></p>"));


        Thread t = new Thread(
                    new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(3000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            if (AppController.getInstance().sharedPreferences.getBoolean("login", false) )
                            {
                                ask_location_permission();

                            }else
                            {
                                startActivity(new Intent(Splash.this,MainActivity.class));
                                finish();
                            }


                        }
                    }
            );

            t.start();

        getKeyHash();

    }


    private void ask_location_permission() {

        Dexter.withActivity(Splash.this)
                .withPermissions(
                        android.Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                ).withListener(new MultiplePermissionsListener() {

            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {

                if (report.areAllPermissionsGranted()) {

                    Log.e("all_permission", "areAllPermissionsGranted");

                    startActivity(new Intent(Splash.this,MainActivity.class));
                    finish();

                } else if (report.isAnyPermissionPermanentlyDenied()) {

                    Log.e("all_permission", "isAnyPermissionPermanentlyDenied");


                    AlertDialog.Builder builder = new AlertDialog.Builder(Splash.this);
                    builder.setTitle("Permission");
                    builder.setMessage("Eatsapp needs LOCATION permissions to place/track you orders.Please grant it to continue using the app");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();

                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, 1);
                            Toast.makeText(getBaseContext(), "Go to Permissions to Grant LOCATION", Toast.LENGTH_LONG).show();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();

                } else {
                    Log.e("all_permission", "isAnyPermissionPermanentlyDenied else");

                    AlertDialog.Builder builder = new AlertDialog.Builder(Splash.this);
                    builder.setTitle("Permission");
                    builder.setMessage("Eatsapp needs LOCATION permissions to place/track you orders.Please grant it to continue using the app");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            ask_location_permission();

                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
//                            finish();
                        }
                    });
                    builder.show();

                }

            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                Log.e("all_permission", "onPermissionRationaleShouldBeShown");

                token.continuePermissionRequest();

            }


        }).check();
    }


    private void getKeyHash() {

        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
//                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
