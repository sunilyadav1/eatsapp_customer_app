package app.eatsapp;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;


import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;

import java.util.Map;

import java.util.ArrayList;

public class PreviousOrders extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    String rs;

    RelativeLayout email_all,parent_layout;

    OrdersAdapter ordersAdapter;
    ListView listView;

    ArrayList<String> Seq= new ArrayList<String>();
    ArrayList<String>  Names= new ArrayList<String>();
    ArrayList<String>  Item_Names= new ArrayList<String>();

    ArrayList<String> Date= new ArrayList<String>();

    View fragment_view;

    Dialog loading;

    StringRequest get_orders,send_email;

    PreviousOrders  mContext ;

    Boolean single_bill = false;


    public PreviousOrders() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PreviousOrders.
     */
    // TODO: Rename and change types and number of parameters
    public static PreviousOrders newInstance(String param1, String param2) {
        PreviousOrders fragment = new PreviousOrders();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragment_view = inflater.inflate(R.layout.fragment_previous_orders, container, false);

        Log.e("Previous order", "v3");

        rs=getString(R.string.Rs);

        loading = new Dialog(getActivity());
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading.setContentView(R.layout.loading);

        ordersAdapter= new OrdersAdapter(getActivity(),Seq,Names,Item_Names,Date,mContext);

        listView=(ListView)fragment_view.findViewById(R.id.listView);
        email_all=(RelativeLayout) fragment_view.findViewById(R.id.email_all);
        parent_layout=(RelativeLayout) fragment_view.findViewById(R.id.parent_layout);
        listView.setAdapter(ordersAdapter);

        ordersAdapter.notifyDataSetChanged();

        get_orders();

        email_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Incomplete
                if(Seq.size()==0)
                {
//                    Toast.makeText(getActivity(),"No orders to Email",Toast.LENGTH_LONG).show();
                    Toast toast = Toast.makeText(getActivity()," No orders to Email ",Toast.LENGTH_LONG);
                    toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                    toast.show();

                }else {

                    single_bill=false;
                    send_email(Seq.toString());

                }
            }
        });

        // Inflate the layout for this fragment
        return fragment_view;
    }

    private void get_orders() {

        Log.e("get_orders_url", "" +getString(R.string.base_url)+getString(R.string.orders)+AppController.getInstance().sharedPreferences.getString("id",""));

        loading.show();

        get_orders   = new StringRequest(Request.Method.GET,getString(R.string.base_url)+getString(R.string.orders)+AppController.getInstance().sharedPreferences.getString("id","")  ,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("response", "" +response);

                        try {

                            JSONArray jsonArray = new JSONArray(response);
                            JSONArray itemsJsonArray;
                            String items = "";
                            int cost=0;

                            for (int i =0 ; i<jsonArray.length();i++)
                            {
                                if(jsonArray.getJSONObject(i).has("items"))
                                {
                                    Seq.add(jsonArray.getJSONObject(i).getString("order_number"));
                                    itemsJsonArray = new JSONArray (jsonArray.getJSONObject(i).getString("items"));
                                    items="";
//                                    cost=0;

                                    for (int j =0 ; j<itemsJsonArray.length();j++)
                                    {
                                        items=items+" "+itemsJsonArray.getJSONObject(j).getString("menu")+",";
//                                        cost = cost+Integer.parseInt(itemsJsonArray.getJSONObject(j).getString("cost"));

                                    }

                                    Item_Names.add(items);
                                    Names.add(jsonArray.getJSONObject(i).getString("restaurant_name")+" - "+rs+" "+jsonArray.getJSONObject(i).getString("total_cost"));
                                    Date.add(itemsJsonArray.getJSONObject(itemsJsonArray.length()-1).getString("ordered_on"));

                                }

                            }




                        } catch (JSONException e) {
                            Log.e("JSONException", "" +e.toString());

                            if(e.toString().contains("order list  could not be found for the customer"))
                            {
                                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage("You have no previous orders !").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                                        dialog.cancel();
                                    }
                                });
                                final AlertDialog alert = builder.create();
                                alert.show();
                                alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));


                            }
                        }

                        loading.dismiss();

                        ordersAdapter.notifyDataSetChanged();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String error_msg="";

                Log.e("error", "" + volleyError.toString());

               loading.dismiss();

                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured, Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error, Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error, Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured, Please try later";
                }

//                Toast.makeText(getActivity(), error_msg, Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(getActivity()," "+error_msg+" ",Toast.LENGTH_SHORT);
                toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                toast.show();
            }
        }) ;

        AppController.getInstance().getRequestQueue().add(get_orders);

    }

    public void build_no_internet_msg() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Your Internet seems to be turned OFF, do you want to turn it ON?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();

            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));


    }


    public void send_email(final String string) {

        Log.e("email_details","email_details");
        Log.e("email_details",AppController.getInstance().sharedPreferences.getString("email",""));

        if(AppController.getInstance().sharedPreferences.getString("email","").equals(""))
        {
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Please update your Email ID in profile to receive bills").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                    dialog.cancel();

                }
            });
            final AlertDialog alert = builder.create();
            alert.show();
            alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        }else
        {
            loading.show();

            Log.e("_url", "" +getString(R.string.base_url)+getString(R.string.ordermail));

            send_email  = new StringRequest(Request.Method.POST,getString(R.string.base_url)+getString(R.string.ordermail),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        loading.dismiss();

                        Log.e("response", "" +response);

                        if(response.contains("Success"))
                        {

//                            Toast.makeText(getActivity(),"Your bills have been emailed",Toast.LENGTH_LONG).show();

                            String msg = "";

                           if(single_bill)
                           {
                               msg = " Your Bill has been emailed ";

                           }else
                           {
                               msg = " Your Bills have been emailed ";
                           }

                            Toast toast = Toast.makeText(getActivity(),msg,Toast.LENGTH_LONG);
                            toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                            toast.show();

                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String error_msg="";

                Log.e("error", "" + volleyError.toString());

                loading.dismiss();

                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured, Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error, Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error, Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured, Please try later";
                }

                Snackbar snackbar = Snackbar
                        .make(parent_layout, error_msg, Snackbar.LENGTH_LONG) ;
                snackbar.show();

            }
        }){
            @Override
            protected Map<String, String> getParams()
            {
                // TODO Auto-generated method stub

                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id",""+AppController.getInstance().sharedPreferences.getString("id",""));
                params.put("order_ids",string);

                Log.e("params", "" + params.toString());

                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

            AppController.getInstance().getRequestQueue().add(send_email);
        }

    }
}
