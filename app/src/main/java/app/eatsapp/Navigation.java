package app.eatsapp;

import android.*;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.NetworkImageView;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;


import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.HashMap;

import java.util.List;
import java.util.Map;

public class Navigation extends AppCompatActivity {

    Dialog loading;

    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;

    private Toolbar toolbar;

    int toolbar_height;

    Fragment fragment;

    Boolean traffic = false;

    // index to identify current nav menu item
    public static int navItemIndex = 0;

    // tags used to attach the fragments
    private static final String TAG_HOME = "home";
    private static final String TAG_PROFILE = "profile";
    private static final String TAG_ADDRESS = "address";


    private static final String TAG_previous_orders = "previous_orders";
    private static final String TAG_notification = "notification";
    private static final String TAG_invite = "invite";

    private static final String TAG_SuggestionRestaurants = "suggest_restaurants";
    private static final String TAG_suggest_pitstops = "suggest_pitstops";


    private static final String TAG_feedback = "feedback";
    private static final String TAG_terms = "terms";
    private static final String TAG_logout = "logout";


    public static String CURRENT_TAG = TAG_HOME;

    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;

    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;

    NetworkImageView user_pic;
    ImageView user_pic_img;
    TextView user_name;

    StringRequest display_user_photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppController.getInstance().set_task_bg(this);

        Log.e("version", "change5");
        setContentView(R.layout.activity_navigation);


        loading = new Dialog(Navigation.this);
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading.setContentView(R.layout.loading);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        ViewTreeObserver vto = toolbar.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                toolbar_height = toolbar.getHeight();

            }
        });

        mHandler = new Handler();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);


        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        user_pic = (NetworkImageView) navHeader.findViewById(R.id.user_pic);
        user_pic_img = (ImageView) navHeader.findViewById(R.id.user_pic_img);
        user_name = (TextView) navHeader.findViewById(R.id.user_name);
        user_pic.setDefaultImageResId(R.drawable.user);

        user_name.setText(AppController.getInstance().sharedPreferences.getString("firstname", ""));

        user_pic_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(Navigation.this, PhotoUpload.class).putExtra("type", "update"), 1);
            }
        });

        user_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(Navigation.this, PhotoUpload.class).putExtra("type", "update"), 1);
            }
        });

        // load toolbar titles from string resources
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);

        // initializing navigation menu
        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();
        }

        display_user_photo();
    }

    private void display_user_photo() {

        Log.e("display_user_photo_url", "" + getString(R.string.base_url) + getString(R.string.user_pic));


        display_user_photo = new StringRequest(Request.Method.POST, getString(R.string.base_url) + getString(R.string.user_pic),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("response", "" + response);

                        if (response.contains("Success")) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                AppController.getInstance().getRequestQueue().getCache().clear();
                                user_pic.setImageUrl(jsonObject.getString("url"), AppController.getInstance().getImageLoader());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }


                        try {
                            JSONObject jsonObject = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {


                Log.e("error", "" + volleyError.toString());

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // TODO Auto-generated method stub

                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", AppController.getInstance().sharedPreferences.getString("id", "") + "");

                Log.e("params", "" + params.toString());

                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        display_user_photo.setShouldCache(false);
        AppController.getInstance().getRequestQueue().add(display_user_photo);
    }

    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    private void loadHomeFragment() {
        // selecting appropriate nav menu item

        Log.e("loadHomeFragment", "1");

        if (navItemIndex != 4 && navItemIndex != 5 && navItemIndex != 7 && navItemIndex != 9) {

            selectNavMenu();

            // set toolbar title
            setToolbarTitle();

        }

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();


            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments

                getHomeFragment();

                if (navItemIndex != 4 && navItemIndex != 5 && navItemIndex != 7 && navItemIndex != 9 && navItemIndex != 10) {

                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                    fragmentTransaction.commitAllowingStateLoss();
                }
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);

        }


        //Closing drawer on item click
        if (navItemIndex != 4 && navItemIndex != 5 && navItemIndex != 7 && navItemIndex != 9 && navItemIndex != 10) {

            drawer.closeDrawers();


            // refresh toolbar menu

            invalidateOptionsMenu();
        }


    }

    private void getHomeFragment() {

        switch (navItemIndex) {
            case 0:
                // home
                Home home = new Home();
                fragment = home;
                break;

            case 1:
                Profile profile = new Profile();
                fragment = profile;
                break;

            case 2:
                Address address = new Address();
                fragment = address;
                break;

            case 3:
                PreviousOrders previous_orders = new PreviousOrders();
                fragment = previous_orders;
                break;


            case 5:
//                Toast.makeText(Navigation.this,"Please wait",Toast.LENGTH_LONG).show();

                getInviteMessage();

                break;

            case 4:
                startActivity(new Intent(Navigation.this, Notifications.class));
                onBackPressed();
                break;

            case 6:
                suggest_restaurant suggest_restaurant = new suggest_restaurant();
                fragment = suggest_restaurant;
                break;

            case 7:
                startActivity(new Intent(Navigation.this, LocateOnMap.class).putExtra("from", "Delivery Point"));
                onBackPressed();
                break;

            case 8:
                Feedback feedback = new Feedback();
                fragment = feedback;
                break;


            case 9:
                startActivity(new Intent(Navigation.this, TermsList.class));
                loading.show();

                break;


            case 10:
                AppController.getInstance().verification = false;
                AppController.getInstance().sharedPreferences_editor.putBoolean("login", false);
                AppController.getInstance().sharedPreferences_editor.commit();
                startActivity(new Intent(Navigation.this, MainActivity.class));
                finish();

                break;

            default:
                fragment = new Home();
        }
    }

    private void getInviteMessage() {

        loading.show();

        Log.e("getInviteMessage", "" + getResources().getString(R.string.base_url) + getResources().getString(R.string.invitemessage));

        StringRequest getInviteMessage = new StringRequest(Request.Method.GET, getResources().getString(R.string.base_url) + getResources().getString(R.string.invitemessage), new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                // TODO Auto-generated method stub

                Log.e("response", response);

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.getString("message").length()!=0) {

                        Toast toast = Toast.makeText(Navigation.this, " Please wait ", Toast.LENGTH_LONG);
                        toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                        toast.show();

                        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                        sharingIntent.setType("text/plain");
                        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                        sharingIntent.putExtra(Intent.EXTRA_TEXT, URLDecoder.decode(jsonObject.getString("message"), "UTF-8").replaceAll("&amp;#039;","'"));
                        startActivity(Intent.createChooser(sharingIntent, "Tell a Friend"));


                    } else {

                        AppController.getInstance().show_popup_alert("Something went wrong, Please try later !", Navigation.this);
                    }

                } catch (Exception e) {

                    Log.e("e", e.toString());
                    AppController.getInstance().show_popup_alert("Something went wrong, Please try later !", Navigation.this);

                }

                loading.dismiss();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                // TODO Auto-generated method stub

                loading.dismiss();
                
                String error_msg = "";

                Log.e("error", "" + volleyError.toString());

                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured, Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error, Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error, Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured, Please try later";
                }
                
                AppController.getInstance().show_popup_alert(error_msg, Navigation.this);
            }
        });

        Log.e("request", "getInviteMessage :" + "sent");

        AppController.getInstance().getRequestQueue().add(getInviteMessage);
    }

    private void setToolbarTitle() {

        Log.e("test", navItemIndex + "");

        if (activityTitles[navItemIndex].toLowerCase().contains("eatsapp") || navItemIndex == 0) {

            changeToolbarFont(toolbar, Navigation.this);
            getSupportActionBar().setTitle(Html.fromHtml("<p style='letter-spacing:20px;'><font color='#d32f2f'>eats</font><font  color='#4285f4'>app</font></p>"));


        } else {

            resetFont(toolbar, Navigation.this);
            getSupportActionBar().setTitle(activityTitles[navItemIndex]);

        }
    }

    public static void changeToolbarFont(Toolbar toolbar, Activity context) {
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);
            if (view instanceof TextView) {
                TextView tv = (TextView) view;
                if (tv.getText().equals(toolbar.getTitle())) {
                    applyFont(tv, context);
                    break;
                }
            }
        }
    }

    public static void applyFont(TextView tv, Activity context) {
        tv.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/GOTHIC.TTF"));
    }

    public static void resetFont(Toolbar toolbar, Activity context) {

        TextView current_tv = null;

        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);
            if (view instanceof TextView) {
                TextView tv = (TextView) view;
                if (tv.getText().equals(toolbar.getTitle())) {
                    current_tv = tv;
                    break;
                }
            }
        }

        current_tv.setTypeface(Typeface.DEFAULT);
    }


    private void selectNavMenu() {

        Log.e("selectNavMenu", navItemIndex + "");

        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu

        Log.e("size", navigationView.getMenu().size() + "");

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.home:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_HOME;
                        break;
                    case R.id.profile:
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_PROFILE;
                        break;

                    case R.id.address:
                        navItemIndex = 2;
                        CURRENT_TAG = TAG_ADDRESS;
                        break;


                    case R.id.previous_orders:
                        navItemIndex = 3;
                        CURRENT_TAG = TAG_previous_orders;
                        break;

                    case R.id.notification:
                        navItemIndex = 4;
                        CURRENT_TAG = TAG_notification;
                        break;

                    case R.id.invite:
                        navItemIndex = 5;
                        CURRENT_TAG = TAG_invite;
                        break;

                    case R.id.suggest_restaurants:
                        navItemIndex = 6;
                        CURRENT_TAG = TAG_SuggestionRestaurants;
                        break;


                    case R.id.suggest_pitstops:
                        navItemIndex = 7;
                        CURRENT_TAG = TAG_suggest_pitstops;
                        break;


                    case R.id.feedback:
                        navItemIndex = 8;
                        CURRENT_TAG = TAG_feedback;
                        break;

                    case R.id.terms:
                        navItemIndex = 9;
                        CURRENT_TAG = TAG_terms;
                        break;


                    case R.id.logout:
                        navItemIndex = 10;
                        CURRENT_TAG = TAG_logout;
                        break;


//                    case R.id.nav_privacy_policy:
//                        // launch new intent instead of loading fragment
//                        startActivity(new Intent(MainActivity.this, PrivacyPolicyActivity.class));
//                        drawer.closeDrawers();
//                        return true;

                    default:
                        navItemIndex = 0;
                }

                //Checking if the item is in checked state or not, if not make it in checked state

                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {

        Log.e("onBackPressed", AppController.getInstance().sharedPreferences.getString("firstname", ""));

        user_name.setText(AppController.getInstance().sharedPreferences.getString("firstname", ""));

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than home
        if (shouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than home
            if (navItemIndex != 0) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_HOME;
                loadHomeFragment();
                return;
            }
        }

        super.onBackPressed();
    }

    public void open_home_screen() {
        Log.e("open_home_screen", "flag 1");
        navItemIndex = 0;
        CURRENT_TAG = TAG_HOME;

        // selecting appropriate nav menu item

        Log.e("loadHomeFragment", "1");

        selectNavMenu();
        setToolbarTitle();

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments

                getHomeFragment();


                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();

            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);

        }

        drawer.closeDrawers();
        invalidateOptionsMenu();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (navItemIndex == 0) {
            getMenuInflater().inflate(R.menu.toolbar_menus, menu);
            return true;

        } else {

            return false;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.delivery) {

            if (navItemIndex == 0) {
                ask_location_permission();

            }

            return true;

        } else if (id == R.id.traffic) {

            if (navItemIndex == 0) {
                if (traffic) {
                    traffic = false;
                } else {
                    traffic = true;
                }

                Fragment home_fragment = getSupportFragmentManager().findFragmentByTag(TAG_HOME);
                ((Home) home_fragment).toggle_traffic(traffic);

            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (data != null && resultCode == Activity.RESULT_OK && requestCode == 1 && data.getExtras().getString("upload").equals("success")) {
            Log.e("extras", data.getExtras().getString("upload"));
            Log.e("yourBitmap", AppController.getInstance().yourBitmap.toString());
            user_pic.setVisibility(View.GONE);
            user_pic_img.setVisibility(View.VISIBLE);
            user_pic_img.setImageBitmap(AppController.getInstance().yourBitmap);

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        loading.dismiss();

        if (activityTitles[navItemIndex].toLowerCase().contains("eatsapp") || navItemIndex == 0) {

            changeToolbarFont(toolbar, Navigation.this);
            getSupportActionBar().setTitle(Html.fromHtml("<p style='letter-spacing:20px;'><font color='#d32f2f'>eats</font><font  color='#4285f4'>app</font></p>"));


        } else {

            resetFont(toolbar, Navigation.this);
            getSupportActionBar().setTitle(activityTitles[navItemIndex]);

        }

        if (navItemIndex != 0 && navItemIndex != 2 && navItemIndex != 6 && navItemIndex != 8) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();
            return;
        }
    }

    private void ask_location_permission() {

        Dexter.withActivity(Navigation.this)
                .withPermissions(
                        android.Manifest.permission.ACCESS_COARSE_LOCATION,
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                ).withListener(new MultiplePermissionsListener() {

            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {

                if (report.areAllPermissionsGranted()) {
                    Log.e("navItemIndex", navItemIndex + "");
                    Fragment home_fragment = getSupportFragmentManager().findFragmentByTag(TAG_HOME);
                    ((Home) home_fragment).get_current_orders();

                } else if (report.isAnyPermissionPermanentlyDenied()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Navigation.this);
                    builder.setTitle("Permission");
                    builder.setMessage("Eatsapp needs LOCATION permissions to place/track you orders");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();

                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, 1);
                            Toast.makeText(getBaseContext(), "Go to Permissions to Grant LOCATION", Toast.LENGTH_LONG).show();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();

                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Navigation.this);
                    builder.setTitle("Permission");
                    builder.setMessage("Eatsapp needs LOCATION permissions to place/track you orders");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            ask_location_permission();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();

                }

                Log.e("report", "areAllPermissionsGranted" + report.areAllPermissionsGranted() + "");
                Log.e("report", "getDeniedPermissionResponses" + report.getDeniedPermissionResponses() + "");
                Log.e("report", "getGrantedPermissionResponses" + report.getGrantedPermissionResponses() + "");
                Log.e("report", "isAnyPermissionPermanentlyDenied" + report.isAnyPermissionPermanentlyDenied() + "");
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                token.continuePermissionRequest();

            }


        }).check();
    }


}
