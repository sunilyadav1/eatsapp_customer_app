package app.eatsapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.widget.BaseAdapter;

/**
 * Created by developer.nithin@gmail.com
 */
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

public class AddressAdapter extends BaseAdapter implements View.OnClickListener {
    Activity activity;

    ArrayList<String> Seq;
    ArrayList<String>  Names;
    ArrayList<String> Addresses;
    LayoutInflater inflater;
    Address mContext;
    Intent i ;
    Bundle b;
    Boolean show;

    public AddressAdapter(Activity activity,ArrayList<String> Seq, ArrayList<String> Names, ArrayList<String> Addresses,Address mContext,Boolean show)
    {
        // TODO Auto-generated constructor stub
        this.activity = activity;
        this.Seq =Seq;
        this.show =show;
        this.Names = Names;
        this.Addresses = Addresses;
        this.mContext = mContext;
        i  = new Intent(activity,AddEditAddress.class);
         b = new Bundle();

        b.putString("new","no");


    }

    @Override
    public int getCount()
    {
        // TODO Auto-generated method stub
        return Seq.size();
    }

    @Override
    public Object getItem(int position)
    {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int id)
    {
        // TODO Auto-generated method stub
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2)
    {
        // TODO Auto-generated method stub
        if(inflater == null)
        {
            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if(convertView ==null)
        {
            convertView = inflater.inflate(R.layout.address_row, null);
        }

        TextView name =(TextView)convertView.findViewById(R.id.name);
        TextView address=(TextView)convertView.findViewById(R.id.address);
        ImageButton delete=(ImageButton)convertView.findViewById(R.id.delete);

        name.setText(Names.get(position) );
        address.setText(Addresses.get(position));

        delete.setTag(position+"");
        address.setTag(position+"");
        name.setTag(position+"");

        delete.setOnClickListener(AddressAdapter.this);
        address.setOnClickListener(AddressAdapter.this);
        name.setOnClickListener(AddressAdapter.this);

        if(show)
        {
            delete.setVisibility(View.VISIBLE);
        }


        return convertView;
    }

    @Override
    public void onClick(View v) {

        final int position = Integer.parseInt(v.getTag().toString());


        switch (v.getId())
        {
            case R.id.delete:

                final AddressAdapter addressAdapter=this;

                final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setMessage("Are you sure you want to delete address of "+Names.get(position)).setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                        ((Address)mContext).delete_address(Seq.get(position));
                        Seq.remove(position);
                        Names.remove(position);
                        Addresses.remove(position);

                        addressAdapter.notifyDataSetChanged();


                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();

                    }
                });
                final AlertDialog alert = builder.create();
                alert.show();
                alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(((Address)mContext).getResources().getColor(R.color.colorPrimary));
                alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(((Address)mContext).getResources().getColor(R.color.colorPrimary));

            break;

            case R.id.name:

                try {
                    b.putString("json", ((Address)mContext).jsonArray.get(position).toString());
                    b.putString("id", Seq.get(position));

                } catch (Exception e) {

                    Log.e("JSONException",e.toString());
                }

                i.putExtras(b);
                activity.startActivity(i);
                break;

            case R.id.address:
                try {

                    b.putString("json", ((Address)mContext).jsonArray.get(position).toString());
                    b.putString("id", Seq.get(position));

                } catch (Exception e) {

                    Log.e("JSONException"   ,e.toString());
                }

                i.putExtras(b);
                activity.startActivity(i);
                break;

        }


    }
}