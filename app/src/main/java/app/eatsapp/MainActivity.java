package app.eatsapp;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.DialogOnAnyDeniedMultiplePermissionsListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText input_ph;
    private TextInputLayout input_layout_ph;
    TextView generate,agreement,terms,name;
    LinearLayout parent_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.e("main","v3");

//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        AppController.getInstance().set_task_bg(this);
//
//        Intent i = new Intent(MainActivity.this, Navigation.class);
//            startActivity(i);
//            finish();

        if (AppController.getInstance().sharedPreferences.getBoolean("login", false) ) {

            if(AppController.getInstance().sharedPreferences.getString("firstname","").toString().length()!=0)
            {
                Intent i = new Intent(MainActivity.this, Navigation.class);
                startActivity(i);
                finish();

            }else {

                startActivity(new Intent(MainActivity.this, UserName.class));
            }

        }else {
            getdid();
        }

        parent_layout=(LinearLayout)findViewById(R.id.parent_layout);

        input_layout_ph = (TextInputLayout) findViewById(R.id.input_layout_ph);

        input_ph = (EditText) findViewById(R.id.input_ph);

        generate = (TextView) findViewById(R.id.generate);
        agreement = (TextView) findViewById(R.id.agreement);

        terms = (TextView) findViewById(R.id.terms);

        name = (TextView) findViewById(R.id.name);
        Typeface fontRobo = Typeface.createFromAsset(getAssets(), "fonts/GOTHIC.TTF");
        name.setTypeface(fontRobo);

        name.setText(Html.fromHtml("<p style='letter-spacing:20px;'><font color='#d32f2f'>eats</font><font  color='#4285f4'>app</font></p>"));

        input_ph.addTextChangedListener(new MyTextWatcher(input_ph));



        terms.setText(Html.fromHtml("<p>By clicking on Proceed you agree to the <br / > <font  color='#d32f2f'><b>Terms & Conditions</b></font></p>"));

        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,TermsList.class));
            }
        });

        agreement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,TermsList.class));
            }
        });

        generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validatePh()) {

                    AppController.getInstance().sharedPreferences_editor.putString("firstname", "");
                    AppController.getInstance().sharedPreferences_editor.putString("phone", input_ph.getText().toString());
                    AppController.getInstance().sharedPreferences_editor.commit();

                    if(!ConnectivityReceiver.isConnected())
                    {
                        Snackbar snackbar = Snackbar
                                .make(parent_layout, "No internet connection!", Snackbar.LENGTH_LONG);

                        snackbar.show();

                        build_no_internet_msg();

                    }else
                    {
                        startActivity(new Intent(MainActivity.this, Verification.class));
                        finish();
                    }

//                    ask_permission();


                } else {
                    Log.e("false"," validatePh() " + validatePh());
                }

            }
        });
    }

//    private void ask_permission() {
//
//        Dexter.withActivity(MainActivity.this)
//                .withPermissions(
//                        Manifest.permission.READ_SMS,
//                        Manifest.permission.RECEIVE_SMS
//                ).withListener(new MultiplePermissionsListener() {
//
//            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
//
//                if(report.areAllPermissionsGranted())
//                {
//                    if(!ConnectivityReceiver.isConnected())
//                    {
//                        Snackbar snackbar = Snackbar
//                                .make(parent_layout, "No internet connection!", Snackbar.LENGTH_LONG);
//
//                        snackbar.show();
//
//                        build_no_internet_msg();
//
//                    }else
//                    {
//                        startActivity(new Intent(MainActivity.this, Verification.class));
//                        finish();
//                    }
//
//                }else if(report.isAnyPermissionPermanentlyDenied())
//                {
//                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
//                    builder.setTitle("Permission");
//                    builder.setMessage("Eatsapp needs SMS permissions to authenticate the user");
//                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.cancel();
//
//                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                            Uri uri = Uri.fromParts("package", getPackageName(), null);
//                            intent.setData(uri);
//                            startActivityForResult(intent,1);
//                            Toast.makeText(getBaseContext(), "Go to Permissions to Grant SMS", Toast.LENGTH_LONG).show();
//                        }
//                    });
//                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.cancel();
//                        }
//                    });
//                    builder.show();
//
//                }else
//                {
//                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
//                    builder.setTitle("Permission");
//                    builder.setMessage("Eatsapp needs SMS permissions to authenticate the user");
//                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.cancel();
//                            ask_permission();
//                        }
//                    });
//                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.cancel();
//                        }
//                    });
//                    builder.show();
//
//                }
//
//                Log.e("report","areAllPermissionsGranted"+report.areAllPermissionsGranted()+"");
//                Log.e("report","getDeniedPermissionResponses"+report.getDeniedPermissionResponses()+"");
//                Log.e("report","getGrantedPermissionResponses"+report.getGrantedPermissionResponses()+"");
//                Log.e("report","isAnyPermissionPermanentlyDenied"+report.isAnyPermissionPermanentlyDenied()+"");
//            }
//
//            @Override
//            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
//
//                token.continuePermissionRequest();
//
//            }
//
//
//        }).check();
//    }


    private boolean validatePh() {
        if (input_ph.getText().toString().length() == 0 || input_ph.getText().toString().length() < 10 ) {
            input_layout_ph.setError("Please enter valid mobile number");
            requestFocus(input_ph);
            return false;
        } else {
            input_layout_ph.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    private void getdid() {

        if (FirebaseInstanceId.getInstance().getToken() != null) {

            Log.e("old token : ", "" + FirebaseInstanceId.getInstance().getToken());
            AppController.getInstance().token=true;
            AppController.getInstance().token_value=FirebaseInstanceId.getInstance().getToken();

        } else {

            Log.e("fetching : ", "new did");
            FirebaseInstanceId.getInstance().getToken();

        }


    }


    public void build_no_internet_msg() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Your Internet seems to be turned OFF, do you want to turn it ON?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();

            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));


    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {

                case R.id.input_ph:
                    break;

            }
        }

    }

}
