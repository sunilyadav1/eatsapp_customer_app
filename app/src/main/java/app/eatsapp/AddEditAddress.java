package app.eatsapp;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;


import java.util.HashMap;

import java.util.Map;

/**
 * Created by developer.nithin@gmail.com
 */
public class AddEditAddress extends AppCompatActivity {

    private Toolbar toolbar;

    ArrayList<String> listAll=new ArrayList<String>();
    // for listing all states

    AutoCompleteTextView act,actCity,actState;

    TextInputLayout name_layout,address1_layout,address2_layout,actAll_layout,location0_layout,location1_layout,location2_layout,zip_layout,actCity_layout,actState_layout;
    TextView name ,address1 , address2 ,actAll ,location0, location1 , location2 , zip;

    Button save;

    StringRequest insert_address;

    LinearLayout parent_layout;

    Dialog loading;

    String id = "";
    JSONObject received_address;

    ArrayList<String> listState=new ArrayList<String>();

    ArrayList<String> listCity=new ArrayList<String>();

    TextView latlng_label,latlng_values;
    Double lat,lng;

    ArrayList<View> textViews = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.add_edit_address);

        AppController.getInstance().set_task_bg(this);

        AppController.getInstance().added=false;

        loading = new Dialog(AddEditAddress.this);
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading.setContentView(R.layout.loading);


        name_layout =(TextInputLayout)findViewById(R.id.name_layout);
        address1_layout =(TextInputLayout)findViewById(R.id.address1_layout);
        address2_layout=(TextInputLayout)findViewById(R.id.address2_layout);
        actAll_layout=(TextInputLayout)findViewById(R.id.actAll_layout);
        actCity_layout=(TextInputLayout)findViewById(R.id.actCity_layout);
        actState_layout=(TextInputLayout)findViewById(R.id.actState_layout);


        location0_layout =(TextInputLayout)findViewById(R.id.location0_layout);
        location1_layout =(TextInputLayout)findViewById(R.id.location1_layout);
        location2_layout =(TextInputLayout)findViewById(R.id.location2_layout);
        zip_layout=(TextInputLayout)findViewById(R.id.zip_layout);

        latlng_label= (TextView)findViewById(R.id.latlng_label);
        latlng_values= (TextView)findViewById(R.id.latlng_values);

        parent_layout=(LinearLayout) findViewById(R.id.parent_layout);

        name=(TextView)findViewById(R.id.name);
        address1=(TextView)findViewById(R.id.address1);
        address2=(TextView)findViewById(R.id.address2);
        actAll=(TextView)findViewById(R.id.actAll);
        location0=(TextView)findViewById(R.id.location0);
        location1=(TextView)findViewById(R.id.location1);
        location2=(TextView)findViewById(R.id.location2);
        zip=(TextView)findViewById(R.id.zip);

        save=(Button) findViewById(R.id.save);

        name.addTextChangedListener(new MyTextWatcher(name));
        address1.addTextChangedListener(new MyTextWatcher(address1));
        address2.addTextChangedListener(new MyTextWatcher(address2));
        actAll.addTextChangedListener(new MyTextWatcher(actAll));
        location0.addTextChangedListener(new MyTextWatcher(location0));
        location1.addTextChangedListener(new MyTextWatcher(location1));
        location2.addTextChangedListener(new MyTextWatcher(location2));
        zip.addTextChangedListener(new MyTextWatcher(zip));

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("New Address");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        callAll();
        Bundle bundle = getIntent().getExtras();

        if(getIntent().getExtras().getString("new").equals("no"))
        {
            try {
                received_address=new JSONObject(getIntent().getExtras().getString("json"));
                id=getIntent().getExtras().getString("id");
                set_values();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


        textViews.add(name);
        textViews.add(address1);
        textViews.add(address2);
        textViews.add(actAll);
        textViews.add(location0);
        textViews.add(location1);
        textViews.add(location2);
        textViews.add(actCity);
        textViews.add(actState);
        textViews.add(zip);

        for (int i = 0; i < textViews.size()-1; i++) {

            final int finalI = i;

            ((TextView)textViews.get(i)).addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {

                    String newline = System.getProperty("line.separator");

                    if(  ((TextView)textViews.get(finalI)).getText().toString().contains(newline))
                    {
                        ((TextView)textViews.get(finalI)).setText(((TextView)textViews.get(finalI)).getText().toString().replace(newline,""));
                        textViews.get(finalI+1).requestFocus();
                    }
                }
            });

        }


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validate_name() &&  validate_address1() && validate_address2() && validate_city() && validate_state() && validate_location0() && validate_latlng())//  &&  validate_zip())
                {
                    Log.e("done","done");
                    insert_address();

                }else {

                    Log.e("not done","not done");
                }

            }
        });

        latlng_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(AddEditAddress.this,LocateOnMap.class).putExtra("from",""),9);
            }
        });

        latlng_values.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(AddEditAddress.this,LocateOnMap.class).putExtra("from",""),9);
            }
        });


    }

    private void set_values() {
        String city_state ;


      try
      {
          city_state = received_address.getString("city_state");

          name.setText(received_address.getString("company"));
          address1.setText(received_address.getString("address1"));
          address2.setText(received_address.getString("address2"));
          actCity.setText(city_state.substring(0,city_state.indexOf(",")));
          actState.setText(city_state.substring(city_state.indexOf(",")+1,city_state.length()));
          location0.setText(received_address.getString("location0"));
          location1.setText(received_address.getString("location1"));
          location2.setText(received_address.getString("location2"));
          zip.setText(received_address.getString("zip"));

          lat = Double.parseDouble(received_address.getString("lat")+"");
          lng = Double.parseDouble(received_address.getString("lng")+"");

          latlng_values.setText(lat+", "+lng);

      }catch (Exception e)
      {
          Log.e("e",e.toString());
      }


    }

    private void insert_address() {

        loading.show();

        Log.e("insert_address_url", "" +getString(R.string.base_url)+getString(R.string.Addresssave));

        insert_address = new StringRequest(Request.Method.POST,getString(R.string.base_url)+getString(R.string.Addresssave),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("response", "" +response);

                        if(response.contains("success"))
                        {
                            AppController.getInstance().added=true;

                            if(getIntent().getExtras().getString("new").equals("no"))
                            {
//                                Toast.makeText(AddEditAddress.this,"Address has been updated",Toast.LENGTH_LONG).show();
                                Toast toast = Toast.makeText(AddEditAddress.this," Address has been updated ",Toast.LENGTH_LONG);
                                toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                                toast.show();
                            }else {
//                                Toast.makeText(AddEditAddress.this,"Address has been added",Toast.LENGTH_LONG).show();
                                Toast toast = Toast.makeText(AddEditAddress.this," Address has been added ",Toast.LENGTH_LONG);
                                toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                                toast.show();
                            }



                            name.setText(" ");
                            address1.setText(" ");
                            address2.setText(" ");
                            actCity.setText(" ");
                            actState.setText(" ");
                            location0.setText(" ");
                            location1.setText(" ");
                            location2.setText(" ");
                            zip.setText(" ");

                            finish();


                        }else {

//                            Toast.makeText(AddEditAddress.this,"Error occurred while adding address",Toast.LENGTH_LONG).show();
                            Toast toast = Toast.makeText(AddEditAddress.this," Error occurred while adding address ",Toast.LENGTH_LONG);
                            toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                            toast.show();

                        }

                        loading.dismiss();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String error_msg="";

                Log.e("error", "" + volleyError.toString());

                loading.dismiss();

                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured, Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error, Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error, Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured, Please try later";
                }

                Snackbar snackbar = Snackbar
                        .make(parent_layout, error_msg, Snackbar.LENGTH_LONG) ;
                snackbar.show();

            }
        }){
            @Override
            protected Map<String, String> getParams()
            {
                // TODO Auto-generated method stub

                Map<String, String> params = new HashMap<String, String>();

                params.put("id",id);
                params.put("company",""+name.getText().toString());
                params.put("address1",""+address1.getText().toString());
                params.put("address2",""+address2.getText().toString());
                params.put("city_state",""+actCity.getText().toString()+","+actState.getText().toString());
                params.put("location0",""+location0.getText().toString());
                params.put("location1",""+location1.getText().toString());
                params.put("location2",""+location2.getText().toString());
                params.put("zip",""+zip.getText().toString());
                params.put("latitude",""+lat);
                params.put("langitude",""+lng);
                params.put("customer_id",""+AppController.getInstance().sharedPreferences.getString("id",""));

                Log.e("params", "" + params.toString());


                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        AppController.getInstance().getRequestQueue().add(insert_address);
    }

    public void build_no_internet_msg() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(AddEditAddress.this);
        builder.setMessage("Your Internet seems to be turned OFF, do you want to turn it ON?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();

            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));


    }


    public void callAll()
    {
        obj_list();

        addToAll();

        addCity();

        addState();

    }

    // Get the content of cities.json from assets directory and store it as string
    public String getJson()
    {
        String json=null;
        try
        {
            // Opening cities.json file
            InputStream is = getAssets().open("cities.json");
            // is there any content in the file
            int size = is.available();
            byte[] buffer = new byte[size];
            // read values in the byte array
            is.read(buffer);
            // close the stream --- very important
            is.close();
            // convert byte to string
            json = new String(buffer, "UTF-8");
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            return json;
        }
        return json;
    }

    // This add all JSON object's data to the respective lists
    void obj_list()
    {
        // Exceptions are returned by JSONObject when the object cannot be created
        try
        {
            // Convert the string returned to a JSON object
            JSONObject jsonObject=new JSONObject(getJson());
            // Get Json array
            JSONArray array=jsonObject.getJSONArray("array");
            // Navigate through an array item one by one
            for(int i=0;i<array.length();i++)
            {
                // select the particular JSON data
                JSONObject object=array.getJSONObject(i);
                String city=object.getString("name");
                String state=object.getString("state");


                listAll.add(city+" , "+state);
                listCity.add(city);

                if (!listState.contains(state)) {

                    listState.add(state);

                }

            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }



    // The first auto complete text view
    void addToAll()
    {
        act=(AutoCompleteTextView)findViewById(R.id.actAll);
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,listAll);
        act.setAdapter(adapter);

        act.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            }
        });
    }


    void addCity()
    {
        actCity=(AutoCompleteTextView)findViewById(R.id.actCity);
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,listCity);
        actCity.setAdapter(adapter);

        actCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            }
        });
    }


    void addState()
    {
        actState=(AutoCompleteTextView)findViewById(R.id.actState);
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,listState);
        actState.setAdapter(adapter);

        actState.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            }
        });
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
           getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    private boolean validate_name() {
        if (name.getText().toString().length() == 0 || name.getText().toString().equals(" ")) {
            name_layout.setError("Please enter Name");
            requestFocus(name);
            return false;
        } else {
            name_layout.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validate_address1() {
        if (address1.getText().toString().length() == 0 || address1.getText().toString().equals(" ")) {
            address1_layout.setError("* Required");
            requestFocus(address1);
            return false;
        } else {
            address1_layout.setErrorEnabled(false);
        }

        return true;
    }


    private boolean validate_address2() {
        if (address2.getText().toString().length() == 0 || address2.getText().toString().equals(" ")) {
            address2_layout.setError("* Required");
            requestFocus(address2);
            return false;
        } else {
            address2_layout.setErrorEnabled(false);
        }

        return true;
    }


    private boolean validate_actAll() {
        if (actAll.getText().toString().length() == 0 || actAll.getText().toString().equals(" ")) {
            actAll_layout.setError("* Required");
            requestFocus(actAll);
            return false;
        } else {
            actAll_layout.setErrorEnabled(false);
        }

        return true;
    }


    private boolean validate_city() {
        if (actCity.getText().toString().length() == 0 || actCity.getText().toString().equals(" ")) {
            actCity_layout.setError("* Required");
            requestFocus(actCity);
            return false;
        } else {
            actCity_layout.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validate_state() {
        if (actState.getText().toString().length() == 0 || actState.getText().toString().equals(" ")) {
            actState_layout.setError("* Required");
            requestFocus(actAll);
            return false;
        } else {
            actState_layout.setErrorEnabled(false);
        }

        return true;
    }



    private boolean validate_location0() {
        if (location0.getText().toString().length() == 0 || location0.getText().toString().equals(" ")) {
            location0_layout.setError("* Required");
            requestFocus(location0);
            return false;
        } else {
            location0_layout.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validate_latlng() {

        Log.e("validate_latlng",(latlng_values.getText().toString().length() == 0)+"  "+latlng_values.getText().toString().equals(""));

        if (latlng_values.getText().toString().length() == 0 || latlng_values.getText().toString().equals("")) {

            Toast.makeText(AddEditAddress.this,"Please locate on Map",Toast.LENGTH_SHORT).show();

            return false;

        } else {

            return true;
        }


    }


    private boolean validate_location1() {
        if (location1.getText().toString().length() == 0  || location1.getText().toString().equals(" ")) {
            location1_layout.setError("* Required");
            requestFocus(location1);
            return false;
        } else {
            location1_layout.setErrorEnabled(false);
        }

        return true;
    }


    private boolean validate_location2() {
        if (location2.getText().toString().length() == 0  || location2.getText().toString().equals(" ")) {
            location2_layout.setError("* Required");
            requestFocus(location2);
            return false;
        } else {
            location2_layout.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validate_zip() {
        if (zip.getText().toString().length() == 0 || zip.getText().toString().equals(" ")) {
            zip_layout.setError("* Required");
            requestFocus(zip);
            return false;
        } else {
            zip_layout.setErrorEnabled(false);
        }

        return true;
    }




    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_name:

                    break;
                case R.id.input_email:

                    break;
                case R.id.input_age:

                    break;

            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 9 && data != null) {

            Log.e("returned",""+data.getExtras().getString("Address"));

            lat = Double.parseDouble(data.getExtras().getString("lat")+"");
            lng = Double.parseDouble(data.getExtras().getString("long")+"");

            latlng_values.setText(lat+", "+lng);
        }
    }
}
