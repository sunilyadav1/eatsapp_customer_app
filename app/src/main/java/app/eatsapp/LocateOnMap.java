package app.eatsapp;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.*;
import android.location.Address;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import app.eatsapp.autocomplete.PlacesAutoCompleteActivity;

/**
 * Created by developer.nithin@gmail.com
 */
public class LocateOnMap extends AppCompatActivity implements OnMapReadyCallback {

    RelativeLayout address_bar;

    private GoogleMap mMap;
    SupportMapFragment mapFragment;
    Geocoder geocoder;
    Boolean locked = false;
    TextView action_hint, address, go;
    String full_address = "", from;
    ImageView pin;
    LocationManager manager = null;
    StringRequest insert_suggest_pitstop;
    Dialog loading;
    RelativeLayout parent_layout;
    String lat, lng;
    List<Address> addresses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppController.getInstance().set_task_bg(this);

        ask_location_permission();

        setContentView(R.layout.locate_on_map);

        Log.e("LocateOnMapv", "v6");

        manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        loading = new Dialog(LocateOnMap.this);
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading.setContentView(R.layout.loading);

        from = getIntent().getExtras().getString("from");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Locate " + from + " on Map");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        pin = (ImageView) findViewById(R.id.pin);

        action_hint = (TextView) findViewById(R.id.action_hint);
        address = (TextView) findViewById(R.id.address);
        go = (TextView) findViewById(R.id.go);
        address_bar = (RelativeLayout) findViewById(R.id.address_bar);
        parent_layout = (RelativeLayout) findViewById(R.id.parent_layout);


        action_hint.setText("Select " + from + " Location");

        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!from.contains("Delivery Point")) {

                    if (!ConnectivityReceiver.isConnected()) {
//                        Toast.makeText(LocateOnMap.this,"No Internet Connection",Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(LocateOnMap.this, " No Internet Connection ", Toast.LENGTH_SHORT);
                        toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                        toast.show();

                        build_no_internet_msg();

                    } else {

                        Intent i = new Intent();
                        i.putExtra("Address", full_address);
                        i.putExtra("lat", mMap.getCameraPosition().target.latitude + "");
                        i.putExtra("long", mMap.getCameraPosition().target.longitude + "");
                        setResult(RESULT_OK, i);
                        finish();
                    }


                } else {

                    if (full_address.isEmpty()) {
//                        Toast.makeText(LocateOnMap.this,"Please select Delivery Point location",Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(LocateOnMap.this, " Please select Delivery Point location ", Toast.LENGTH_SHORT);
                        toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                        toast.show();

                    } else {
                        lat = mMap.getCameraPosition().target.latitude + "";
                        lng = mMap.getCameraPosition().target.longitude + "";
                        insert_suggest_pitstop();
                    }


                }

            }
        });

        action_hint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(LocateOnMap.this, PlacesAutoCompleteActivity.class), 9);
            }
        });

        address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(LocateOnMap.this, PlacesAutoCompleteActivity.class), 9);
            }
        });


        final ViewTreeObserver vto = pin.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                int result = 0;
                int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
                if (resourceId > 0) {
                    result = getResources().getDimensionPixelSize(resourceId);
                }

                ViewGroup.LayoutParams layoutParams = pin.getLayoutParams();
                ViewGroup.MarginLayoutParams address_bar_params = (ViewGroup.MarginLayoutParams) address_bar.getLayoutParams();

                layoutParams.height = ((getResources().getDisplayMetrics().heightPixels / 2)  - findViewById(R.id.toolbar).getHeight() - address_bar_params.topMargin - result)-convertDpToPixel(18);

                //layoutParams.height = (getResources().getDisplayMetrics().heightPixels / 2) - findViewById(R.id.toolbar).getHeight() - address_bar_params.topMargin;
                pin.setLayoutParams(layoutParams);

                if (vto.isAlive()) {
                    Log.e("ViewTreeObserver", "isAlive");
                    vto.removeOnGlobalLayoutListener(this);
                }

                address_bar.setVisibility(View.VISIBLE);
                pin.setVisibility(View.VISIBLE);


            }
        });

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }


    }

    public int convertDpToPixel(float dp) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }


    public void build_no_internet_msg() {


        final AlertDialog.Builder builder = new AlertDialog.Builder(LocateOnMap.this);
        builder.setMessage("Your Internet seems to be turned OFF, do you want to turn it ON?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();

            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));


    }

    private void insert_suggest_pitstop() {

        Log.e("_url", "" +getString(R.string.base_url) + getString(R.string.Pitstop));

        loading.show();

        insert_suggest_pitstop = new StringRequest(Request.Method.POST, getString(R.string.base_url) + getString(R.string.Pitstop),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        loading.dismiss();

                        Log.e("response", "" + response);

                        if (response.contains("Success")) {

//                                Toast.makeText(LocateOnMap.this,"Thanks for your Suggestion",Toast.LENGTH_LONG).show();
                            Toast toast = Toast.makeText(LocateOnMap.this, " Thanks for your Suggestion ", Toast.LENGTH_LONG);
                            toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                            toast.show();
                            finish();


                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String error_msg = "";

                Log.e("error", "" + volleyError.toString());

                loading.dismiss();

                if (volleyError instanceof NoConnectionError) {
                    error_msg = "No Internet Connection";
                    build_no_internet_msg();

                } else if (volleyError instanceof TimeoutError) {
                    error_msg = "Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError) {
                    error_msg = "Error Occured, Please try later";

                } else if (volleyError instanceof ServerError) {
                    error_msg = "Server Error, Please try later";

                } else if (volleyError instanceof NetworkError) {
                    error_msg = "Network Error, Please try later";

                } else if (volleyError instanceof ParseError) {
                    error_msg = "Error Occured, Please try later";
                }

                Snackbar snackbar = Snackbar
                        .make(parent_layout, error_msg, Snackbar.LENGTH_LONG);
                snackbar.show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // TODO Auto-generated method stub

                Map<String, String> params = new HashMap<String, String>();

                params.put("location", full_address);
                params.put("restaurant_latitude", lat + "");
                params.put("restaurant_langitude", lng + "");
                params.put("user_id", AppController.getInstance().sharedPreferences.getString("id", "0") + "");

                Log.e("params", params.toString() + "");
                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        insert_suggest_pitstop.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 0;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
                Log.e("error", error.toString());
            }
        });

        AppController.getInstance().getRequestQueue().add(insert_suggest_pitstop);
    }


    void get_address() {

        full_address = "";


        try {
//            Log.e("flow", "1");
            addresses = geocoder.getFromLocation(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
//            Log.e("flow", "2");

            if (addresses != null) {
                String address_value = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

//                Log.e("address", address_value+">"+city+">"+state+">"+country+">"+postalCode+">"+knownName);
//                Log.e("flow", "getMaxAddressLineIndex"+addresses.get(0).getMaxAddressLineIndex());


                for (int j = 0; j <= addresses.get(0).getMaxAddressLineIndex(); j++) {
//                    Log.e("flow", "4");
                    full_address = full_address + " " + addresses.get(0).getAddressLine(j);
                }

                address.setText(full_address);

                action_hint.setText("Select " + from + " Location");

            }

        } catch (Exception e) {
            Log.e("addresses Exception", e.toString());
        }


    }


    @Override
    public void onResume() {
        super.onResume();

        if (mMap == null) {
            mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

            mapFragment.getMapAsync(this);

        }

        geocoder = new Geocoder(LocateOnMap.this, Locale.getDefault());

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(LocateOnMap.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        } else {

            mMap.setMyLocationEnabled(true);

            mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
                @Override
                public void onCameraMove() {

                    if (!locked) {

                        if (!ConnectivityReceiver.isConnected()) {
//                            Toast.makeText(LocateOnMap.this,"No Internet Connection",Toast.LENGTH_SHORT).show();
                            Toast toast = Toast.makeText(LocateOnMap.this, " No Internet Connection ", Toast.LENGTH_SHORT);
                            toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                            toast.show();
                            build_no_internet_msg();

                        } else {
                            action_hint.setText("Getting Address...");
                            address.setText("....");
                        }

                    }

                }
            });


            mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                @Override
                public void onCameraIdle() {

                    if (!ConnectivityReceiver.isConnected()) {
//                        Toast.makeText(LocateOnMap.this, "Not Internet Connection", Toast.LENGTH_LONG);
                        Toast toast = Toast.makeText(LocateOnMap.this, " No Internet Connection ", Toast.LENGTH_LONG);
                        toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                        toast.show();
                        build_no_internet_msg();
                    } else {
                        get_address();
                    }

                }
            });


            Float lat = 0f;
            Float lng = 0f;

            if (!AppController.getInstance().sharedPreferences.getString("last_lat", "empty").equals("empty")) {
                lat = Float.parseFloat(AppController.getInstance().sharedPreferences.getString("last_lat", "0"));
                lng = Float.parseFloat(AppController.getInstance().sharedPreferences.getString("last_long", "0"));
                Log.e("last_lat", AppController.getInstance().sharedPreferences.getString("last_lat", "empty"));

                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), ((mMap.getMaxZoomLevel() + mMap.getMinZoomLevel()) / 2) + ((mMap.getMaxZoomLevel() + mMap.getMinZoomLevel()) / 4)));

            }


        }

    }


    @Override
    public void onPause() {
        super.onPause();

        mapFragment.onPause();

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 9 && data != null) {
            full_address = data.getExtras().getString("Address");
            address.setText(data.getExtras().getString("Address"));
            mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble(data.getExtras().getString("Lat") + ""), Double.parseDouble(data.getExtras().getString("Long") + ""))));
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void buildAlertMessageNoGps() {
        // TODO Auto-generated method stub

        pin.setVisibility(View.GONE);
        address_bar.setVisibility(View.GONE);

        final AlertDialog.Builder builder = new AlertDialog.Builder(LocateOnMap.this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();

            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));

    }

    private void ask_location_permission() {

        Dexter.withActivity(LocateOnMap.this)
                .withPermissions(
                        android.Manifest.permission.ACCESS_COARSE_LOCATION,
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                ).withListener(new MultiplePermissionsListener() {

            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {

                if (report.areAllPermissionsGranted()) {


                } else if (report.isAnyPermissionPermanentlyDenied()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(LocateOnMap.this);
                    builder.setTitle("Permission");
                    builder.setMessage("Eatsapp needs LOCATION permissions to obatain the suggested location");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();

                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, 1);
                            Toast.makeText(getBaseContext(), "Go to Permissions to Grant LOCATION", Toast.LENGTH_LONG).show();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            finish();
                        }
                    });
                    builder.show();

                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(LocateOnMap.this);
                    builder.setTitle("Permission");
                    builder.setMessage("Eatsapp needs LOCATION permissions to obatain the suggested location");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            ask_location_permission();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            finish();
                        }
                    });
                    builder.show();

                }

                Log.e("report", "areAllPermissionsGranted" + report.areAllPermissionsGranted() + "");
                Log.e("report", "getDeniedPermissionResponses" + report.getDeniedPermissionResponses() + "");
                Log.e("report", "getGrantedPermissionResponses" + report.getGrantedPermissionResponses() + "");
                Log.e("report", "isAnyPermissionPermanentlyDenied" + report.isAnyPermissionPermanentlyDenied() + "");
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                token.continuePermissionRequest();

            }


        }).check();
    }

}

