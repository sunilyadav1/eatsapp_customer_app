package app.eatsapp;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by developer.nithin@gmail.com
 */
public class MenuItem {

    String id,menu;
    int cost;

    MenuItem(String Id,String Menu, int Cost)
    {
        id = Id;
        menu = Menu;
        cost = Cost;
    }

}
