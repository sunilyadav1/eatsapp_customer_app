package app.eatsapp;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by developer.nithin@gmail.com
 */
public class FinalScreen extends AppCompatActivity {

    TextView code,name;
    Button navigate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("final","v3");
        setContentView(R.layout.final_screen);

        AppController.getInstance().set_task_bg(this);

        code = (TextView) findViewById(R.id.code);
        navigate = (Button) findViewById(R.id.navigate);

        if(AppController.getInstance().option_selected==0)
        {
            navigate.setVisibility(View.VISIBLE);
        }

        navigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("navigate","getLatitude"+AppController.getInstance().delivery_location.getLatitude()+"");
                Log.e("navigate","getLongitude"+AppController.getInstance().delivery_location.getLongitude()+"");

                setResult(RESULT_OK, new Intent().putExtra("order", "done"));
                finish();

                if(AppController.getInstance().delivery_location!=null)
                {
                    String location_link = "https://www.google.com/maps?daddr="+AppController.getInstance().collection_point_latitude+","+AppController.getInstance().collection_point_langitude;
                    Log.e("location_link",location_link);
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse(location_link));
                    startActivity(intent);
                }

            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Thank You");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        name = (TextView) findViewById(R.id.name);

        Typeface fontRobo = Typeface.createFromAsset(getAssets(), "fonts/GOTHIC.TTF");
        name.setTypeface(fontRobo);
        name.setText(Html.fromHtml("<p style='letter-spacing:20px;margin:0px;padding:0px;'><font color='#d32f2f'>eats</font><font  color='#4285f4'>app</font></p>"));
        code.setText("" + getIntent().getExtras().getString("Passcode"));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                setResult(RESULT_OK, new Intent().putExtra("order", "done"));
                finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onBackPressed() {

        Log.e("onBackPressed","onBackPressed");

        setResult(RESULT_OK, new Intent().putExtra("order", "done"));
        finish();



    }
}
