package app.eatsapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by HP on 08-Feb-18.
 */

public class ListAdapter extends BaseAdapter   {

    Activity activity;

    ArrayList<String> customization_items;
    LayoutInflater inflater;
//
    public ListAdapter(Activity activity,ArrayList<String> customization_items)
    {
        // TODO Auto-generated constructor stub
        this.activity = activity;
        this.customization_items =customization_items;


    }

    @Override
    public int getCount()
    {
        // TODO Auto-generated method stub
        return customization_items.size();
    }

    @Override
    public Object getItem(int position)
    {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int id)
    {
        // TODO Auto-generated method stub
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2)
    {
        // TODO Auto-generated method stub
        if(inflater == null)
        {
            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if(convertView ==null)
        {
            convertView = inflater.inflate(R.layout.menu_names_cell, null);
        }

        TextView item =(TextView)convertView.findViewById(R.id.menu_name);

        item.setText(customization_items.get(position).replace(" ,",", "));

        return convertView;
    }


}
