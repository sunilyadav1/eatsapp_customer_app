package app.eatsapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by developer.nithin@gmail.com
 */
public class Payment extends Activity {


    WebView mWebView;
    ProgressDialog progressDialog;
    String jio_order_id,Passcode,url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the layout from video_main.xml



        jio_order_id = getIntent().getExtras().getString("jio_order_id");
        Passcode = getIntent().getExtras().getString("Passcode");

        Log.e("Payment", "v4 "+jio_order_id);
        setContentView(R.layout.payment);
        mWebView = (WebView) findViewById(R.id.webview);

        AppController.getInstance().set_task_bg(this);

        progressDialog= new ProgressDialog(this);
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();


        try {

            mWebView.setWebViewClient(new WebViewClient());
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.getSettings().setAppCacheEnabled(true);
            mWebView.getSettings().setDomStorageEnabled(true);
            url=getString(R.string.jio_url)+"purchase.php?order_id="+jio_order_id;
            mWebView.loadUrl(url);
            Log.e("jio_call","tag 4");




            mWebView.setWebChromeClient(new WebChromeClient() {

                public void onProgressChanged(WebView view, int progress) {

                    Log.e("onProgressChanged", ""+progress );

                    progressDialog.show();

                    if (progress == 100) {

                    progressDialog.dismiss();
                    Log.e("onProgressChanged", "100"+mWebView.getUrl() );

                        if(mWebView.getUrl().contains(getString(R.string.jio_url)+"response.php"))
                        {
                            setResult(RESULT_OK);
                            finish();
                        }

                    }
                }




            });




        } catch (Exception e) {
           Log.e("Exception",e.toString());
        }
    }


}
