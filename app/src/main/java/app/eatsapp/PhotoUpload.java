package app.eatsapp;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;
import com.esafirm.imagepicker.features.ImagePicker;

import com.esafirm.imagepicker.features.camera.OnImageReadyListener;
import com.esafirm.imagepicker.model.Image;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.theartofdev.edmodo.cropper.CropImage;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import java.util.Map;

/**
 * Created by developer.nithin@gmail.com
 */
public class PhotoUpload extends AppCompatActivity {

    String order_json;
    TextView select_image,proceed,skip,msg;

    private ArrayList<Image> images = new ArrayList<>();
    ImageView photo_selected;
    NetworkImageView Profile_photo_selected;

    String type;
    StringRequest update_profile_pic,insert_order,check_order_statusRequest;

    Dialog loading;

    StringRequest display_user_photo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.e("PhotoUpload","v10");
        AppController.getInstance().set_task_bg(this);

        setContentView(R.layout.photo_upload);


        AppController.getInstance().yourBitmap=null;
        AppController.getInstance().yourBitmap_string="0";

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Photo");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        type = getIntent().getExtras().getString("type");

        loading = new Dialog(this);
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading.setContentView(R.layout.loading);


        select_image=(TextView)findViewById(R.id.select_image);
        proceed=(TextView)findViewById(R.id.proceed);
        skip=(TextView)findViewById(R.id.skip);
        msg=(TextView)findViewById(R.id.msg);
        photo_selected=(ImageView)findViewById(R.id.photo_selected);
        Profile_photo_selected=(NetworkImageView) findViewById(R.id.Profile_photo_selected);


        if(type.equals("update"))
        {
            skip.setVisibility(View.GONE);
            proceed.setText("Update");
            msg.setText("Update Profile Photo");
            Profile_photo_selected.setVisibility(View.VISIBLE);
            Profile_photo_selected.setDefaultImageResId(R.drawable.user);
            photo_selected.setVisibility(View.GONE);
            AppController.getInstance().jio_order_id="0";
            display_user_photo();
        }else {

            photo_selected.setVisibility(View.VISIBLE);
            Profile_photo_selected.setVisibility(View.GONE);
        }


        Profile_photo_selected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ask_permission();

            }
        });


        photo_selected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ask_permission();

            }
        });

        select_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ask_permission();

            }
        });


        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 AppController.getInstance().yourBitmap_string="0";
                try {
                    AppController.getInstance().order_json_object.put("photo", AppController.getInstance().yourBitmap_string);
                    insert_order();
                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }
        });

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(type.equals("update"))
                {
                    if(AppController.getInstance().yourBitmap!=null)
                    {
                        update_profile_pic();
                    }else
                    {
//                        Toast.makeText(PhotoUpload.this,"Please select a photo",Toast.LENGTH_LONG).show();
                        Toast toast = Toast.makeText(PhotoUpload.this," Please select a photo ",Toast.LENGTH_LONG);
                        toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                        toast.show();

                    }



                }else
                {


                    if(AppController.getInstance().yourBitmap!=null)
                    {
                        AppController.getInstance().yourBitmap_string=getStringImage(AppController.getInstance().yourBitmap);
                        try {

                            AppController.getInstance().order_json_object.put("photo", AppController.getInstance().yourBitmap_string);

                            insert_order();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }




                    }else
                    {
//                        Toast.makeText(PhotoUpload.this,"Please select a photo",Toast.LENGTH_LONG).show();
                        Toast toast = Toast.makeText(PhotoUpload.this," Please select a photo ",Toast.LENGTH_LONG);
                        toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                        toast.show();
                    }

                }

            }
        });


        select_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ask_permission();
            }
        });

    }

    private void display_user_photo() {

        Log.e("display_user_photo", "display_user_photo" );

        Log.e("display_user_photo_url", "" +getString(R.string.base_url)+getString(R.string.user_pic));


        display_user_photo  = new StringRequest(Request.Method.POST,getString(R.string.base_url)+getString(R.string.user_pic),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("response", "" +response);

                        if(response.contains("Success"))
                        {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.e("setImageUrl", "" +jsonObject.getString("url"));
                                AppController.getInstance().getRequestQueue().getCache().clear();
                                Profile_photo_selected.setImageUrl(jsonObject.getString("url"),AppController.getInstance().getImageLoader());

                            } catch (JSONException e) {
                                Log.e("JSONException", "" +e.toString());

                            }
                        }




                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {


                Log.e("error", "" + volleyError.toString());

            }
        }){
            @Override
            protected Map<String, String> getParams()
            {
                // TODO Auto-generated method stub

                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id",AppController.getInstance().sharedPreferences.getString("id","")+"");

                Log.e("params", "" + params.toString());

                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };
        display_user_photo.setShouldCache(false);
        AppController.getInstance().getRequestQueue().add(display_user_photo);
    }


    private void insert_order() {

        Log.e("insert_order_url", "" +getString(R.string.base_url)+getString(R.string.ordersinsert));



        loading.show();

        insert_order   = new StringRequest(Request.Method.POST,getString(R.string.base_url)+getString(R.string.ordersinsert),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        loading.dismiss();

                        Log.e("response", "" +response);


                        if(response.contains("Success"))
                        {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                AppController.getInstance().jio_order_id = jsonObject.getString("order_id");
                                AppController.getInstance().Passcode = jsonObject.getString("Passcode");

                                if(AppController.getInstance().paymentMode==1)
                                {
                                    startActivityForResult(new Intent(PhotoUpload.this, FinalScreen.class).putExtra("Passcode",AppController.getInstance().Passcode),11);

                                }else
                                {
                                    String url = getString(R.string.jio_url)+"purchase.php?order_id="+AppController.getInstance().jio_order_id+"&amount="+AppController.getInstance().decimalFormat.format(((AppController.getInstance().net_order + AppController.getInstance().delivery_charge+Double.parseDouble(AppController.getInstance().taxWithoutVoucher))))+".00&ph="+AppController.getInstance().sharedPreferences.getString("phone","");
                                    Log.e("url",url);
                                    Log.e("jio_call","tag 3");

                                    CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                                    CustomTabsIntent customTabsIntent = builder.build();

                                    builder.setToolbarColor(ContextCompat.getColor(PhotoUpload.this,R.color.white));
                                    builder.setShowTitle(false);

                                    customTabsIntent.intent.setPackage("com.android.chrome");

                                    customTabsIntent.launchUrl(PhotoUpload.this, Uri.parse(url));
                                }

//                                old code before jio integration
//                                startActivityForResult(new Intent(PhotoUpload.this,Payment.class).putExtra("Passcode",jsonObject.getString("Passcode")),11);

                            } catch (JSONException e) {
                                Log.e("response", "" +e.toString());
                            }

                        }else
                        {
//                            Toast.makeText(PhotoUpload.this,"Your order couldnt be procesed at this time",Toast.LENGTH_LONG);
                            Toast toast = Toast.makeText(PhotoUpload.this," Your order couldnt be procesed at this time ",Toast.LENGTH_LONG);
                            toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                            toast.show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String error_msg="";

                Log.e("error", "" + volleyError.toString());

                loading.dismiss();

                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured, Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error, Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error, Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured, Please try later";
                }

//                Toast.makeText(PhotoUpload.this,error_msg, Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(PhotoUpload.this," "+error_msg+" ",Toast.LENGTH_LONG);
                toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                toast.show();


            }
        }){

            @Override
            public byte[] getBody() throws com.android.volley.AuthFailureError {

                Log.e("order_json", "" +AppController.getInstance().order_json_object.toString());

                return AppController.getInstance().order_json_object.toString().getBytes();
            };

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/text");
                headers.put("charset", "TYPE_UTF8_CHARSET");

                Log.e("headers", "" +headers.toString());
                return headers;
            }
        };

        AppController.getInstance().getRequestQueue().add(insert_order);
    }

    public void build_no_internet_msg() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(PhotoUpload.this);
        builder.setMessage("Your Internet seems to be turned OFF, do you want to turn it ON?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();

            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));


    }

    private void update_profile_pic() {

        loading.show();

        Log.e("update_profile_pic", "url" +getString(R.string.base_url)+getString(R.string.updatepicture));

        update_profile_pic  = new StringRequest(Request.Method.POST,getString(R.string.base_url)+getString(R.string.updatepicture),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("response", "" +response);

                            loading.dismiss();

                            if(response.contains("Success"))
                            {
//                                Toast.makeText(PhotoUpload.this,"Photo has been Updated",Toast.LENGTH_LONG).show();
                                Toast toast = Toast.makeText(PhotoUpload.this," Photo has been Updated ",Toast.LENGTH_LONG);
                                toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                                toast.show();
                                setResult(Activity.RESULT_OK, new Intent().putExtra("upload","success"));

                            }else
                            {
//                                Toast.makeText(PhotoUpload.this,"Error Occured,Unable to Update Photo",Toast.LENGTH_LONG).show();
                                Toast toast = Toast.makeText(PhotoUpload.this," Error Occured,Unable to Update Photo ",Toast.LENGTH_LONG);
                                toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                                toast.show();

                            }


                            finish();

//                        try {
//                            JSONObject jsonObject = new JSONObject(response);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String error_msg="";

                Log.e("error", "" + volleyError.toString());

                loading.dismiss();

                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured, Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error, Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error, Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured, Please try later";
                }

//                Toast.makeText(PhotoUpload.this,error_msg,Toast.LENGTH_LONG).show();
                Toast toast = Toast.makeText(PhotoUpload.this," "+error_msg+" ",Toast.LENGTH_LONG);
                toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                toast.show();

            }
        }){
            @Override
            protected Map<String, String> getParams()
            {
                // TODO Auto-generated method stub

                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id",AppController.getInstance().sharedPreferences.getString("id", ""));
                params.put("profile_image",getStringImage(AppController.getInstance().yourBitmap));

                Log.e("params", "" + params.toString());

//                Log.e("profile_image",getStringImage(AppController.getInstance().yourBitmap));
                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        AppController.getInstance().getRequestQueue().add(update_profile_pic);

    }

    private void ask_permission() {

        Dexter.withActivity(PhotoUpload.this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA
                ).withListener(new MultiplePermissionsListener() {

            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {

                if(report.areAllPermissionsGranted())
                {
                    captureImage();

                }else if(report.isAnyPermissionPermanentlyDenied())
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(PhotoUpload.this);
                    builder.setTitle("Permission");
                    builder.setMessage("Eatsapp needs Storage permissions to continue");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();

                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent,1);
                            Toast.makeText(getBaseContext(), "Go to Permissions to Grant SMS", Toast.LENGTH_LONG).show();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();

                }else
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(PhotoUpload.this);
                    builder.setTitle("Permission");
                    builder.setMessage("Eatsapp needs Storage permissions to continue");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            ask_permission();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();

                }

                Log.e("report","areAllPermissionsGranted"+report.areAllPermissionsGranted()+"");
                Log.e("report","getDeniedPermissionResponses"+report.getDeniedPermissionResponses()+"");
                Log.e("report","getGrantedPermissionResponses"+report.getGrantedPermissionResponses()+"");
                Log.e("report","isAnyPermissionPermanentlyDenied"+report.isAnyPermissionPermanentlyDenied()+"");
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                token.continuePermissionRequest();

            }


        }).check();
    }



    private void captureImage() {

//        Toast.makeText(PhotoUpload.this,"Please wait",Toast.LENGTH_LONG).show();
        Toast toast = Toast.makeText(PhotoUpload.this," Please wait ",Toast.LENGTH_LONG);
        toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
        toast.show();

//
//        startActivityForResult(getCameraModule().getCameraIntent(PhotoUpload.this), 2);

        if(type.equals("update"))
        {
            ImagePicker.create(PhotoUpload.this)
                    .imageTitle("Tap to select") // image selection title
                    .multi() // multi mode (default mode)
                    .limit(1) // max images can be selected (99 by default)
                    .showCamera(true) // show camera or not (true by default)
                    .start(12); // start image picker activity with request code
        }else {
            ImagePicker.cameraOnly().start(PhotoUpload.this,13);
        }

    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }



    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {

        Log.e("requestCode",requestCode+"");
        Log.e("resultCode",resultCode+"");

        if(data!=null)
        {
            Log.e("data",data.toString()+"");
        }else {
            Log.e("data","was null");
        }

        if (requestCode == 2 && resultCode == RESULT_OK) {

            List<Image> images = ImagePicker.getImages(data);


                    if(images.size()>0)
                    {
                        String image_url = images.get(0).getPath().toString();
                        image_url=  image_url.substring(5,image_url.length());



                        Log.e("image",image_url);

                        File imgFile = new  File(image_url);


                        try
                        {
                            AppController.getInstance().yourBitmap= BitmapFactory.decodeFile(image_url);
                            AppController.getInstance().yourBitmap=Bitmap.createScaledBitmap(AppController.getInstance().yourBitmap, 250, 250, true);

                            photo_selected.setImageBitmap(AppController.getInstance().yourBitmap);

                        }catch (Exception e)
                        {
                            Log.e("Exception",e.toString());
                        }

                    }



        }else if(requestCode==11 && resultCode==RESULT_OK && data!=null )
        {
            if(data.getExtras().getString("order").equals("done"))
            {
                setResult(RESULT_OK,new Intent().putExtra("order","done"));
                finish();
            }
        }else if(requestCode==12 && resultCode==RESULT_OK && data!=null )
        {
            ArrayList<Image> images = (ArrayList<Image>) ImagePicker.getImages(data);


            if(images.size()>0) {
                String image_url = images.get(0).getPath().toString();

                Log.e("uri before", Uri.parse(images.get(0).getPath()) + "");

                Intent i = new Intent(PhotoUpload.this, Crop.class);
                i.putExtra("uri", Uri.parse(images.get(0).getPath()).toString());
                i.putExtra("rotate", false);

                startActivityForResult(i, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE);


            }
        }else if(requestCode==13 && resultCode==RESULT_OK )
            {
                List<Image> images = ImagePicker.getImages(data);

                        Log.e("images", images.size() + "");

                        if(images.size()>0) {


                            String image_url = images.get(0).getPath().toString();

                            Log.e("uri before", Uri.parse(image_url) + "");
//                            image_url=  image_url.substring(5,image_url.length());


                            Log.e("uri after", Uri.parse(image_url) + "");

                            Intent i = new Intent(PhotoUpload.this, Crop.class);
                            i.putExtra("uri", Uri.parse(images.get(0).getPath()).toString());
                            i.putExtra("rotate", true);
                            startActivityForResult(i, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE);

                        }

        }else if(requestCode== CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE )
        {
            if (resultCode == RESULT_OK) {

                try
                {

                    photo_selected.setVisibility(View.VISIBLE);
                    Profile_photo_selected.setVisibility(View.GONE);

                    AppController.getInstance().yourBitmap=Bitmap.createScaledBitmap(AppController.getInstance().yourBitmap, 250, 250, true);

                    photo_selected.setImageBitmap(AppController.getInstance().yourBitmap);

                }catch (Exception e)
                {
                    Log.e("Exception",e.toString());
                }

            } else {

                Log.e("crop error","error");
            }

        }
    }

    private void check_order_status() {

        Log.e("check_order_status_url", "" +getResources().getString(R.string.base_url)+"GetOrderStatus/"+AppController.getInstance().jio_order_id);

        loading.show();

        check_order_statusRequest  = new StringRequest(Request.Method.GET, getResources().getString(R.string.base_url)+"GetOrderStatus/"+AppController.getInstance().jio_order_id,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("response from server", response);
                        loading.dismiss();

                        if(response.contains("Payment pending"))
                        {

                            AppController.getInstance().show_popup_alert_without_title("Your payment was not successful.",PhotoUpload.this);


                        }else if(response.contains("Order Placed"))
                        {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(PhotoUpload.this);
                            builder.setMessage("Your order was sucessfuly placed.").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                                    dialog.dismiss();
                                    startActivityForResult(new Intent(PhotoUpload.this, FinalScreen.class).putExtra("Passcode",AppController.getInstance().Passcode),11);


                                }
                            });
                            final AlertDialog alert = builder.create();
//                            alert.setTitle("Message");
                            alert.setCancelable(false);
                            alert.show();

                        }



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                loading.dismiss();

                String error_msg="";

                Log.e("error", "" + volleyError.toString());


                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured, Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error, Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error, Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured, Please try later";
                }
//
//                Snackbar snackbar = Snackbar
//                        .make(parent_layout, error_msg, Snackbar.LENGTH_LONG) ;
//                snackbar.show();

            }
        });

        AppController.getInstance().getRequestQueue().add(check_order_statusRequest);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("onresume","place order");
        if(!AppController.getInstance().jio_order_id.equals("0")) {
            check_order_status();
        }
    }


}
