
/**
 * Created by developer.nithin@gmail.com
 */

package app.eatsapp;

        import android.content.Intent;
        import android.os.Bundle;
        import android.widget.BaseAdapter;
      import java.util.ArrayList;

        import android.app.Activity;
        import android.content.Context;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ImageButton;
        import android.widget.TextView;

public class AddressListAdapter extends BaseAdapter   {
    Activity activity;

    ArrayList<String> Seq;
    ArrayList<String>  Names;
    ArrayList<String> Addresses;
    LayoutInflater inflater;

    Intent i ;
    Bundle b;
    Boolean show;

    public AddressListAdapter(Activity activity,ArrayList<String> Seq, ArrayList<String> Names, ArrayList<String> Addresses,Boolean show)
    {
        // TODO Auto-generated constructor stub
        this.activity = activity;
        this.Seq =Seq;
        this.show =show;
        this.Names = Names;
        this.Addresses = Addresses;

        i  = new Intent(activity,AddEditAddress.class);
        b = new Bundle();

        b.putString("new","no");


    }

    @Override
    public int getCount()
    {
        // TODO Auto-generated method stub
        return Seq.size();
    }

    @Override
    public Object getItem(int position)
    {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int id)
    {
        // TODO Auto-generated method stub
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2)
    {
        // TODO Auto-generated method stub
        if(inflater == null)
        {
            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if(convertView ==null)
        {
            convertView = inflater.inflate(R.layout.address_row, null);
        }

        TextView name =(TextView)convertView.findViewById(R.id.name);
        TextView address=(TextView)convertView.findViewById(R.id.address);
        ImageButton delete=(ImageButton)convertView.findViewById(R.id.delete);

        name.setText(Names.get(position) );
        address.setText(Addresses.get(position));

        delete.setTag(position+"");
        address.setTag(position+"");
        name.setTag(position+"");



        if(show)
        {
            delete.setVisibility(View.GONE);
        }


        return convertView;
    }


}
