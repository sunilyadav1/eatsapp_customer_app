package app.eatsapp;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by developer.nithin@gmail.com
 */
public class OrdersAdapter extends BaseAdapter implements View.OnClickListener {
    Activity activity;

    ArrayList<String> Seq;
    ArrayList<String>  Names;
    ArrayList<String>  Item_Names;

    ArrayList<String> Date;

    LayoutInflater inflater;
    PreviousOrders mContext;

    public OrdersAdapter(Activity activity,ArrayList<String> Seq, ArrayList<String> Names, ArrayList<String> Item_Names, ArrayList<String> Date,PreviousOrders mContext)
    {
        // TODO Auto-generated constructor stub
        this.activity = activity;
        this.Seq =Seq;
        this.Names = Names;
        this.Item_Names = Item_Names;
        this.mContext = mContext;

        this.Date = Date;


    }

    @Override
    public int getCount()
    {
        // TODO Auto-generated method stub
        return Seq.size();
    }

    @Override
    public Object getItem(int position)
    {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int id)
    {
        // TODO Auto-generated method stub
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2)
    {
        // TODO Auto-generated method stub
        if(inflater == null)
        {
            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if(convertView ==null)
        {
            convertView = inflater.inflate(R.layout.previous_orders_cell, null);
        }

        TextView name =(TextView)convertView.findViewById(R.id.name);
        TextView  item_names =(TextView)convertView.findViewById(R.id.item_names);
        TextView date=(TextView)convertView.findViewById(R.id.date);
        ImageView email=(ImageView)convertView.findViewById(R.id.email);

        name.setText(Names.get(position));
        item_names.setText(Item_Names.get(position));
        date.setText(Date.get(position));

        email.setTag( position );

        email.setOnClickListener(this);
        return convertView;
    }

    @Override
    public void onClick(View v) {

        int position = Integer.parseInt(v.getTag().toString());

        ((PreviousOrders)mContext).send_email("[\""+Seq.get(position)+"\"]");
        ((PreviousOrders)mContext).single_bill=true;

        Log.e("order id","[\""+Seq.get(position)+"\"]");

    }
}