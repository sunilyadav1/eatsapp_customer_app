package app.eatsapp;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Menu.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Menu#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Menu extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ArrayList<String> l_o;
    ArrayList<String> categories;


    ArrayList<String> id;
    ArrayList<String> name;
    ArrayList<String> price;
    ArrayList<String> veg;
    ArrayList<String> description;

    String title="";
    View fragment_view;

    ListView listView;

    MenuAdapter menuAdapter;

    LayoutInflater inflater ;

    SwitchCompat compatSwitch;

     Dialog description_popup;

    TextView word,meaning;


    public Menu() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Menu.
     */
    // TODO: Rename and change types and number of parameters
    public static Menu newInstance(String param1, String param2) {
        Menu fragment = new Menu();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            id = getArguments().getStringArrayList("id");
            name = getArguments().getStringArrayList("name");
            price = getArguments().getStringArrayList("price");
            veg = getArguments().getStringArrayList("veg");
            description = getArguments().getStringArrayList("description");

            title = getArguments().getString("title");

            //            Log.e("getArguments onCreate",getArguments().toString());

//            if(title.equals("Recommended"))
//            {
//                categories= getArguments().getStringArrayList("categories");
//                l_o= getArguments().getStringArrayList("l_o");
//                alter_array_list();
//            }

        }

//        Log.e("onCreate",title);
    }

    private void alter_array_list() {

        int from;

        for(int i=0;i<categories.size();i++)
        {
            from = Integer.parseInt(l_o.get(i).substring(0,l_o.get(i).indexOf(",")));
            from=from+i;

            id.add(from,"0");
            name.add(from,""+categories.get(i));
            description.add(from,"0");
            price.add(from,"0");
            veg.add(from,"0");



        }

//        Log.e("----------------test 14","---------------------------");
//
//        Log.e("name",name.toString());
//
//        Log.e("----------------test 14","---------------------------");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        fragment_view= inflater.inflate(R.layout.fragment_menu, container, false);

        description_popup = new Dialog(getActivity());
        description_popup.requestWindowFeature(Window.FEATURE_NO_TITLE);
        description_popup.setContentView(R.layout.description);
        description_popup.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        word= (TextView)description_popup.findViewById(R.id.word);
        meaning= (TextView)description_popup.findViewById(R.id.meaning);

        listView=(ListView)fragment_view.findViewById(R.id.listView);

        menuAdapter = new MenuAdapter(getActivity(),id,name,price,veg,this,description);
        listView.setAdapter(menuAdapter);
        menuAdapter.notifyDataSetChanged();

        inflater= getActivity().getLayoutInflater();

        RelativeLayout listHeaderView = (RelativeLayout)inflater.inflate(
                R.layout.veg_nonveg_header, null);

        listView.addHeaderView(listHeaderView);

        compatSwitch=(SwitchCompat)listHeaderView.findViewById(R.id.compatSwitch);

        compatSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.e("isChecked",""+isChecked);
                AppController.getInstance().veg=isChecked;
                menuAdapter.notifyDataSetChanged();
            }
        });

        compatSwitch.setChecked(AppController.getInstance().veg);

//        Log.e("onCreateView",title);

        return fragment_view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

//        Log.e("SetUserVisibleHint-:)",title +" : "+isVisibleToUser);

        if(isVisibleToUser)
        {
            if(menuAdapter!=null)
            {
//                Log.e("menuAdapter "+title,"was not null");
                compatSwitch.setChecked(AppController.getInstance().veg);
                menuAdapter.notifyDataSetChanged();
            }else
            {
//                Log.e("menuAdapter "+title,"was null");
            }


        }
    }


    public void update_values() {

        menuAdapter.notifyDataSetChanged();
    }

    public void show_description(int position) {
        word.setText(name.get(position));
        meaning.setText(description.get(position));
        description_popup.show();
    }
}
