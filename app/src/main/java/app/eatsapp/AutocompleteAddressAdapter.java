package app.eatsapp;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by developer.nithin@gmail.com
 */import app.eatsapp.autocomplete.PlacesAutoCompleteActivity;

import static android.app.Activity.RESULT_OK;

public class AutocompleteAddressAdapter extends BaseAdapter implements View.OnClickListener {

    Activity activity;

    ArrayList<String> Seq;
    ArrayList<String>  Names;
    ArrayList<String> Addresses;
    LayoutInflater inflater;
    PlacesAutoCompleteActivity mContext;
    Intent i ;
    Bundle b;
    Boolean show;

    public AutocompleteAddressAdapter(Activity activity, ArrayList<String> Seq, ArrayList<String> Names, ArrayList<String> Addresses, PlacesAutoCompleteActivity mContext, Boolean show)
    {
        // TODO Auto-generated constructor stub
        this.activity = activity;
        this.Seq =Seq;
        this.show =show;
        this.Names = Names;
        this.Addresses = Addresses;
        this.mContext = mContext;
        i  = new Intent(activity,AddEditAddress.class);
        b = new Bundle();

        b.putString("new","no");


    }

    @Override
    public int getCount()
    {
        // TODO Auto-generated method stub
        return Seq.size();
    }

    @Override
    public Object getItem(int position)
    {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int id)
    {
        // TODO Auto-generated method stub
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2)
    {
        // TODO Auto-generated method stub
        if(inflater == null)
        {
            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if(convertView ==null)
        {
            convertView = inflater.inflate(R.layout.autocomplete_address_row, null);
        }

        TextView name =(TextView)convertView.findViewById(R.id.name);
        TextView address=(TextView)convertView.findViewById(R.id.address);

        name.setText(Names.get(position) );
        address.setText(Addresses.get(position));


        address.setTag(position+"");
        name.setTag(position+"");


        address.setOnClickListener(AutocompleteAddressAdapter.this);
        name.setOnClickListener(AutocompleteAddressAdapter.this);



        return convertView;
    }

    @Override
    public void onClick(View v) {

        final int position = Integer.parseInt(v.getTag().toString());


        switch (v.getId())
        {

            case R.id.name:

                try {

                    Intent goback = new Intent();
                    goback.putExtra("Address",Addresses.get(position));
                    goback.putExtra("Lat", ((PlacesAutoCompleteActivity)mContext).jsonArray.getJSONObject(position).getString("lat") +"");
                    goback.putExtra("Long",((PlacesAutoCompleteActivity)mContext).jsonArray.getJSONObject(position).getString("lng")+"");
                    goback.putExtra("name",Names.get(position));


                    activity.setResult(RESULT_OK, goback);
                    activity.finish();

                } catch (Exception e) {

                    Log.e("Exception"   ,e.toString());
                }

                break;

            case R.id.address:

                try {

                    Intent goback = new Intent();
                    goback.putExtra("Address",Addresses.get(position));
                    goback.putExtra("Lat", ((PlacesAutoCompleteActivity)mContext).jsonArray.getJSONObject(position).getString("lat") +"");
                    goback.putExtra("Long",((PlacesAutoCompleteActivity)mContext).jsonArray.getJSONObject(position).getString("lng")+"");
                    goback.putExtra("name",Names.get(position));


                    activity.setResult(RESULT_OK, goback);
                    activity.finish();

                } catch (Exception e) {

                    Log.e("Exception"   ,e.toString());
                }

                break;

        }


    }
}