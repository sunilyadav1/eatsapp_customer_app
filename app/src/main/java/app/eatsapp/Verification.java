package app.eatsapp;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Verification extends AppCompatActivity {

    ImageButton edit_number;
    ProgressBar timer;
    TextView time;
    EditText code;
    int time_left = 0, sec = 0;
    SimpleDateFormat format;
    CountDownTimer countDownTimer;
    Boolean cancel = false;
    StringRequest sms_request;
    String Ph, verification_code, url;
    Random random = new Random();
    StringRequest verify_or_add_user_request;
    Dialog d;
    LinearLayout linearlayout;
    TextView ph_number;
    Boolean changed=false;
    static Verification mInstance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppController.getInstance().set_task_bg(this);

        Log.e("verification","v6");

        setContentView(R.layout.verification);

        mInstance=this;

        Ph=AppController.getInstance().sharedPreferences.getString("phone","");

        linearlayout=(LinearLayout)findViewById(R.id.linearlayout);

        d= new Dialog(Verification.this);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.loading);

        AppController.getInstance().check_sms = true;

        ph_number=(TextView)findViewById(R.id.ph_number);
        timer = (ProgressBar) findViewById(R.id.timer);
        time = (TextView) findViewById(R.id.time);
        code = (EditText) findViewById(R.id.code);

        edit_number=(ImageButton)findViewById(R.id.edit_number);

        ph_number.setText(Ph);
 
        edit_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Verification.this,Home.class));
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Verification");
        setSupportActionBar(toolbar);

        if(!ConnectivityReceiver.isConnected())
        {
            Snackbar snackbar = Snackbar
                    .make(linearlayout, "No internet connection!", Snackbar.LENGTH_LONG);

            snackbar.show();
            build_no_internet_msg();

        }else
        { dispatch_sms();
        }



        format = new SimpleDateFormat("mm:ss");

        countDownTimer = new CountDownTimer(300000, 1000)
        {

            @Override
            public void onTick(long tick)
            {
                // TODO Auto-generated method stub
                if (!cancel)
                {
                    time_left++;
                    timer.setProgress(time_left);
                    sec = (int) ((tick / 1000) % 60);

                    if (sec < 10)
                    {
                        time.setText((int) (((tick / 1000) / 60) % 60) + ":0" + sec);

                    } else
                    {
                        time.setText((int) (((tick / 1000) / 60) % 60) + ":" + sec);
                    }

                    if (AppController.getInstance().verification)
                    {
                        Log.e("Ticking", "Ticking");
                        code.setText(AppController.getInstance().verification_code);
                        stop_ticking();

                    }
                }
            }

            @Override
            public void onFinish()
            {
                // TODO Auto-generated method stub

                //incomplete when code is not received even after 5 mins of wait


            }
        }.start();


        code.addTextChangedListener(new TextWatcher()
        {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3)
            {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3)
            {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                // TODO Auto-generated method stub

                if(code.getText().toString().equals(AppController.getInstance().verification_code))
                {
                    AppController.getInstance().verification = true;

                    stop_ticking();

                }

            }
        });


    }

    private void stop_ticking() {

        Snackbar.make(linearlayout, "Verification Successful", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();

        d.show();

        cancel = true;
        countDownTimer.cancel();
        countDownTimer.onFinish();


        if(!ConnectivityReceiver.isConnected())
        {
            Snackbar snackbar = Snackbar
                    .make(linearlayout, "No internet connection!", Snackbar.LENGTH_LONG);

            snackbar.show();
            build_no_internet_msg();

        }else
        {
            if(!changed)
            {
                changed=true;
                AppController.getInstance().valid=true;
                AppController.getInstance().register_user();
            }
        }



    }

    private void register_user() {

        Log.e("verify_or_add_user_url", "" +getResources().getString(R.string.base_url)+"customercheck");

        verify_or_add_user_request = new StringRequest(Request.Method.POST, getResources().getString(R.string.base_url)+"customercheck", new Response.Listener<String>()
        {

            @Override
            public void onResponse(String response)
            {
                // TODO Auto-generated method stub

                Log.e("response", response);

                try {

                     JSONObject jsonObject = new JSONObject(response);

                     AppController.getInstance().sharedPreferences_editor.putString("id",jsonObject.getString("id"));
                     if(jsonObject.has("firstname"))
                     {
                         AppController.getInstance().sharedPreferences_editor.putString("firstname",jsonObject.getString("firstname"));
                     }

                    AppController.getInstance().sharedPreferences_editor.commit();

                    AppController.getInstance().set_login(true);



                }catch (Exception e)
                {
                    Log.e("e", e.toString());
                }

//              d.dismiss();

            }
        }, new Response.ErrorListener()
        {

            @Override
            public void onErrorResponse(VolleyError volleyError)
            {
                // TODO Auto-generated method stub

                String error_msg="";

                Log.e("error", "" + volleyError.toString());

                d.dismiss();

                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured,Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error,Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error,Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured,Please try later";
                }

                Snackbar snackbar = Snackbar
                        .make(linearlayout, error_msg, Snackbar.LENGTH_LONG) ;
                snackbar.show();
            }
        })
        {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                // TODO Auto-generated method stub
                Map<String, String> params = new HashMap<String, String>();

                params.put(getString(R.string.Name),AppController.getInstance().sharedPreferences.getString("firstname",""));
                params.put(getString(R.string.phone_number),AppController.getInstance().sharedPreferences.getString("phone",""));

                Log.e("params", "" + params.toString());

                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                // TODO Auto-generated method stub
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;

            }

        };

        Log.e("request", "sent");


        AppController.getInstance().getRequestQueue().add(verify_or_add_user_request);
    }

    public void build_no_internet_msg() {



        final AlertDialog.Builder builder = new AlertDialog.Builder(Verification.this);
        builder.setMessage("Your Internet seems to be turned OFF, do you want to turn it ON?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();

            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));


    }

    private void dispatch_sms() {

        // TODO Auto-generated method stub

        verification_code=""+random.nextInt(9)+random.nextInt(9)+random.nextInt(9)+random.nextInt(9);
//        verification_code="1234";
        AppController.getInstance().verification_code=verification_code;
//        url = getString(R.string.sms)+getString(R.string.user_name)+"&"+getString(R.string.password)+"&sender=BIGPER&to="+Ph+"&message=Verification+Code+is+"+verification_code+"&route_id=7";
//        url =getString(R.string.sms_new)+getString(R.string.user_name_new)+"&"+getString(R.string.password_new)+"&senderid=GRAZZY&dest_mobileno="+Ph+"&tempid=44246&F1="+verification_code+"&response=Y";
//        url =getString(R.string.sms_new)+getString(R.string.user_name_new)+"&"+getString(R.string.password_new)+"&senderid=EATSAP&dest_mobileno="+Ph+"&tempid=52492&F1="+verification_code+"&response=Y";
        url ="https://eatsapp.in/login/index.php/api/Curl?otp="+verification_code+"&ph="+Ph;
        Log.e("url", ""+url);

        sms_request = new StringRequest(Request.Method.GET, url,

                new Response.Listener<String>()
                {

                    @Override
                    public void onResponse(String response)
                    {
                        // TODO Auto-generated method stub
                        //incomplete
                        Log.e("response", response);
                    }

                }, new Response.ErrorListener()
        {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                // TODO Auto-generated method stub
                //incomplete
                Log.e("onErrorResponse", error.toString());

            }
        });

        AppController.getInstance().getRequestQueue().add(sms_request);
        Log.e("sms", "sent");
    }


    public static synchronized Verification getInstance() {
        return mInstance;
    }

    public void finish_activity()
    {
        startActivity(new Intent(Verification.this, UserName.class));
        finish();
    }

}
