package app.eatsapp;

/**
 * Created by developer.nithin@gmail.com
 */

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.model.LatLng;

public class DirectionsJSONParser {

    int total_time=0;
    int total_timeA=0;

    /** Receives a JSONObject and returns a list of lists containing latitude and longitude */
    public List<List<HashMap<String,String>>> parse(JSONObject jObject){

        List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String,String>>>() ;
        JSONArray jRoutes = null;
        JSONArray jLegs = null;
        JSONArray jSteps = null;

        String temp_name;

        Log.e("routes",jObject.toString());

        try {

            jRoutes = jObject.getJSONArray("routes");
            total_time=0;
            /** Traversing all routes */
            for(int i=0;i<jRoutes.length();i++){
                jLegs = ( (JSONObject)jRoutes.get(i)).getJSONArray("legs");
                List path = new ArrayList<HashMap<String, String>>();

                /** Traversing all legs */
                for(int j=0;j<jLegs.length();j++){

                    total_timeA = total_timeA + Integer.parseInt( ((JSONObject)jLegs.get(j)).getJSONObject("duration").get("value").toString());

                    jSteps = ((JSONObject)jLegs.get(j)).getJSONArray("steps");

                    /** Traversing all steps */
                    for(int k=0;k<jSteps.length();k++){
                        String polyline = "";
                        polyline = (String)((JSONObject)((JSONObject)jSteps.get(k)).get("polyline")).get("points");
//                        total_time = total_time+;
                        temp_name=(String)((JSONObject)((JSONObject)jSteps.get(k)).get("duration")).get("text");

                        Log.e("temp_name",temp_name);

                        if(temp_name.contains("hours"))
                        {
                            total_time =total_time+Integer.parseInt(temp_name.substring(0,temp_name.indexOf("h")-1))*60+Integer.parseInt(temp_name.substring(temp_name.indexOf("s")+2,temp_name.indexOf("m")-1));


                        }else if(temp_name.contains("hour"))
                        {
                            total_time =total_time+Integer.parseInt(temp_name.substring(0,temp_name.indexOf("h")-1))*60+Integer.parseInt(temp_name.substring(temp_name.indexOf("r")+2,temp_name.indexOf("m")-1));

                        }else
                        {
                            total_time=total_time+Integer.parseInt(temp_name.replaceAll("[^0-9]", ""));
                        }



                        List<LatLng> list = decodePoly(polyline);

                        /** Traversing all points */
                        for(int l=0;l<list.size();l++){
                            HashMap<String, String> hm = new HashMap<String, String>();
                            hm.put("lat", Double.toString(((LatLng)list.get(l)).latitude) );
                            hm.put("lng", Double.toString(((LatLng)list.get(l)).longitude) );
                            path.add(hm);
                        }
                    }
                    routes.add(path);
                }

//                total_timeA = total_timeA / 60;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){
        }
        return routes;
    }


    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }

    public String get_total_time() {

//        AppController.getInstance().travel_time_mins = total_time+"";

        float formated_time;
        float formated_timeA;
        int temp_total_timeA;

        temp_total_timeA = total_timeA/60;

        AppController.getInstance().travel_time_mins = temp_total_timeA+"";

        Log.e("timeValueA",temp_total_timeA+"");

        if(temp_total_timeA>60)
        {
            formated_timeA = temp_total_timeA / 60;
//            Log.e("formated_timeA",(int)formated_timeA+" hr "+ (total_timeA % 60)+" mins");
            return ((int)formated_timeA)+" hr "+ (temp_total_timeA % 60)+" mins";

        }else
        {
//            Log.e("total_timeA",total_timeA+" mins");
            return temp_total_timeA+" mins";
        }


//        if(total_time>60)
//        {
//            formated_time = total_time / 60;
//            return ((int)formated_time)+" hr "+ (total_time % 60)+" mins";
//
//        }else
//        {
//            return total_time+" mins";
//        }



    }
}