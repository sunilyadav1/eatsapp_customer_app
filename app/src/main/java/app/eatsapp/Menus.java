package app.eatsapp;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by developer.nithin@gmail.com
 */
public class Menus extends AppCompatActivity  {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    StringRequest get_menu;

    ViewPagerAdapter viewPagerAdapter;

    Dialog loading;

    ArrayList<String> categories = new ArrayList<String>();
    ArrayList<String> id = new ArrayList<String>();
    ArrayList<String> name = new ArrayList<String>();
    ArrayList<String> description = new ArrayList<String>();
    ArrayList<String> price = new ArrayList<String>();
    ArrayList<String> veg = new ArrayList<String>();

    ArrayList<String> l_o = new ArrayList<String>();

    TextView total_items,total_cost,proceed;


    String restaurant_id;
    String menu_from_server;

    Dialog customization,value_selection;
    TextView customization_title,value_title,ok_customization;
    ListView customization_listView,value_listView;
    LinearLayout ok_customization_ll;

    ListAdapter adapter;
    ValuesAdapter valuesAdapter;
    ArrayList<String> customization_names = new ArrayList<String>();
    ArrayList<String> customization_values = new ArrayList<String>();
    ArrayList<Boolean> customization_values_selected = new ArrayList<Boolean>();
    JSONArray customizations;

    Button cancel,add_to_cart;

    EditText test_id;

    TextView min;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.e(" Menus version  ","Nithin 12 ");

        Log.e("timeValueB",AppController.getInstance().travel_time_mins);

        AppController.getInstance().set_task_bg(this);

//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//        WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.menu);

        customization = new Dialog(Menus.this);
        customization.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customization.setContentView(R.layout.customization_names);

        customization_title = (TextView) customization.findViewById(R.id.title);
        customization_listView = (ListView) customization.findViewById(R.id.listView);

        test_id = (EditText) findViewById(R.id.test_id);
        cancel = (Button) customization.findViewById(R.id.cancel);
        add_to_cart = (Button) customization.findViewById(R.id.add_to_cart);

        adapter = new ListAdapter(Menus.this,customization_names);
        customization_listView.setAdapter(adapter);

        value_selection = new Dialog(Menus.this);
        value_selection.requestWindowFeature(Window.FEATURE_NO_TITLE);
        value_selection.setContentView(R.layout.customization_values_layout);

        value_title = (TextView) value_selection.findViewById(R.id.title);
        ok_customization_ll = (LinearLayout) value_selection.findViewById(R.id.ok_customization_ll);
        ok_customization = (TextView) value_selection.findViewById(R.id.ok_customization);

        customization_title = (TextView) value_selection.findViewById(R.id.title);
        value_listView = (ListView) value_selection.findViewById(R.id.listView);

        valuesAdapter = new ValuesAdapter(Menus.this,customization_values,customization_values_selected);
        value_listView.setAdapter(valuesAdapter);

        restaurant_id = getIntent().getStringExtra("restaurant_id");

        loading = new Dialog(this);
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading.setContentView(R.layout.loading);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getIntent().getExtras().getString("restaurant_name"));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        total_items=(TextView)findViewById(R.id.total_items);
        total_cost=(TextView)findViewById(R.id.total_cost);
        proceed=(TextView)findViewById(R.id.proceed);
        min=(TextView)findViewById(R.id.min);

        AppController.getInstance().n_items=0;
        AppController.getInstance().total_cost=0;

        total_items.setText(AppController.getInstance().n_items+" Items");
        total_cost.setText("Total : "+getResources().getString(R.string.Rs)+" "+AppController.getInstance().total_cost);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customization.dismiss();
            }
        });

        add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customization.dismiss();

                String item_name= AppController.getInstance().current_item_name;
                String temp_customization="";

                int cost=0;

                try {

                    JSONArray customizations = new JSONArray(AppController.getInstance().customization.get(AppController.getInstance().current_item).toString());

                    for(int i=0;i<customizations.length();i++)
                    {
                        temp_customization = AppController.getInstance().selected_customization.get(customizations.getJSONObject(i).getString("name"));
                        temp_customization=temp_customization.substring(0,temp_customization.indexOf("Rs")-1);
                        item_name = item_name +" "+temp_customization+" | ";

                        temp_customization = AppController.getInstance().selected_customization.get(customizations.getJSONObject(i).getString("name"));

                        temp_customization=temp_customization.substring(temp_customization.indexOf("Rs.")+3,temp_customization.length());
                        cost=cost+Integer.parseInt(temp_customization);

                        Log.e("cost",Integer.parseInt(temp_customization)+"");
                    }


                    app.eatsapp.MenuItem item = new app.eatsapp.MenuItem(AppController.getInstance().current_item,item_name,cost);

                    AppController.getInstance().cart.add(item);

                    AppController.getInstance().current_value++;
                    AppController.getInstance().n_items++;
                    AppController.getInstance().total_cost = AppController.getInstance().total_cost + cost;

                    total_items.setText(AppController.getInstance().n_items + " Items");
                    total_cost.setText("Total : " + getResources().getString(R.string.Rs) + " " + AppController.getInstance().total_cost + "");

                    AppController.getInstance().quantity.put(AppController.getInstance().current_item, AppController.getInstance().current_value);

                    show_cart();

                } catch (Exception e) {

                    Log.e("Exception",e.toString());

                }

            }
        });

        ok_customization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                update_custimizations(AppController.getInstance().customization_category);
            }
        });

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("n_items value",AppController.getInstance().n_items+"");
                Log.e("condition",(AppController.getInstance().n_items!=0)+"");

                if( AppController.getInstance().n_items!=0)
                {
                    if(AppController.getInstance().total_cost<AppController.getInstance().minordervalue)
                    {
                        AppController.getInstance().show_popup_alert_without_title("Cart total ("+getResources().getString(R.string.Rs)+" "+AppController.getInstance().total_cost+") is lesser than Minimum Order Value ("+getResources().getString(R.string.Rs)+" "+AppController.getInstance().minordervalue+")",Menus.this);

                    }else
                    {
                        HashMap<String,Integer>  quantities ;
                        ArrayList<String> final_ids = new ArrayList<String>();
                        ArrayList<String> final_name= new ArrayList<String>();
                        ArrayList<String> final_price = new ArrayList<String>();

                        AppController.getInstance().final_quantity=new HashMap<String, Integer>(AppController.getInstance().quantity);
                        quantities=AppController.getInstance().final_quantity;

                        Log.e("orgi quantities before",AppController.getInstance().final_quantity.toString());

                        String id,menu;
                        int cost;

                        for(int i=0;i< AppController.getInstance().cart.size();i++)
                        {
                            id = AppController.getInstance().cart.get(i).id;
                            menu = AppController.getInstance().cart.get(i).menu;
                            cost = (AppController.getInstance().cart.get(i)).cost;

                            if(quantities.get(id)==0)
                            {
                                Log.e("removed"+name.get(i),"was"+quantities.get(id));
                                quantities.remove(id);

                            }else
                            {
                                final_ids.add(id);
                                final_name.add(menu);
                                final_price.add(cost+"");
                            }
                        }

                        Log.e("new quantities",quantities.toString());
                        Log.e("original quantity",AppController.getInstance().final_quantity.toString());

                        Bundle b = new Bundle();
                        b.clear();

                        b.putStringArrayList("final_ids",final_ids);
                        b.putStringArrayList("final_name",final_name);
                        b.putStringArrayList("final_price",final_price);
                        b.putString("final_quantity",quantities.toString());

                        Log.e("names ",name.toString());

                        startActivityForResult(new Intent(Menus.this,PlaceOrder.class).putExtras(b),11);
                    }


                }else {

//                    Toast.makeText(Menus.this,"Please Select an Item",Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(Menus.this," Please Select an Item ",Toast.LENGTH_SHORT);
                    toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                    toast.show();
                }

            }
        });


        customization_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Log.e(" customization ",position+"");

                try {

                    show_sub_menu(customizations.getJSONObject(position).getString("values"),customization_names.get(position).substring(0,customization_names.get(position).indexOf(":")-1));

                } catch (Exception e) {

                    Log.e("Exception 1",e.toString());

                }
            }
        });

        test_id.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                restaurant_id= test_id.getText().toString();

                return false;
            }
        });

        test_id.setVisibility(View.GONE);

        get_menu();


    }

    public void show_cart() {

        String items="";
        for(int i = 0 ;i<AppController.getInstance().cart.size();i++)
        {
            items=items +" "+AppController.getInstance().cart.get(i).menu+" \n ";

        }

        Log.e(AppController.getInstance().cart.size() + " items",items);


        AppController.getInstance().menu.update_values();

    }

    public void update_custimizations(String customization_category)
    {
        Log.e("sel_cus 2", AppController.getInstance().selected_customization.toString());

        value_selection.dismiss();

        String current_item="";

        for(int i=0;i<customization_names.size();i++)
        {
            current_item = customization_names.get(i);
            current_item = current_item.substring(0,current_item.indexOf(":")-1);

            Log.e("i "+i, "current_item "+current_item+" "+AppController.getInstance().selected_customization.get(current_item));

            if(!customization_category.equalsIgnoreCase("size")){

                customization_names.set(i,current_item+" : "+AppController.getInstance().selected_customization.get(current_item));

            }else
            {
                if(current_item.equalsIgnoreCase("size"))
                {
                    customization_names.set(i,current_item+" : "+AppController.getInstance().selected_customization.get(current_item));

                }else {

                    try {

                        customization_names.set(i,current_item+" : "+get_default_selection(customizations.getJSONObject(i).getString("values"),current_item));
                        AppController.getInstance().selected_customization.put(current_item,get_default_selection(customizations.getJSONObject(i).getString("values"),current_item));
                        Log.e("get_default_selection",get_default_selection(customizations.getJSONObject(i).getString("values"),current_item));

                    } catch (JSONException e) {

                        Log.e("JSONException",e.toString());

                    }
                }
            }

        }

        adapter.notifyDataSetChanged();
    }


    private String get_default_selection(String valuesJSONArray, String customization_title) {

        String temp_value="";// Large Rs.300
        String selected_size="";//Large , Small
        String selected_customization_value = AppController.getInstance().selected_customization.get(customization_title);// Large Rs.300 or Onion Rs.10
        AppController.getInstance().customization_category=customization_title;// Size or Topings

        customization_values.clear();
        customization_values_selected.clear();
        AppController.getInstance().current_sizes.clear();

        Iterator it = AppController.getInstance().selected_customization.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();

            if(pair.getKey().toString().equalsIgnoreCase("size"))
            {
                selected_size = pair.getValue().toString();
                Log.e("selected_size","Iterator"+selected_size);
                break;
            }

        }

        selected_size = selected_size.substring(0,selected_size.indexOf("Rs.")-1);

        try {

            JSONArray values = new JSONArray(valuesJSONArray);

            for(int i=0;i<values.length();i++)
            {
                if(!customization_title.equalsIgnoreCase("size"))
                {
                    Log.e(values.getJSONObject(i).getString("weight"),selected_size);

                    if(values.getJSONObject(i).getString("weight").equalsIgnoreCase(AppController.getInstance().current_selected_size) || values.getJSONObject(i).getString("weight").isEmpty())
                    {
                        temp_value = values.getJSONObject(i).getString("name")+" Rs."+values.getJSONObject(i).getString("price");

                        customization_values_selected.add(false);

                        customization_values.add(values.getJSONObject(i).getString("name")+" Rs."+values.getJSONObject(i).getString("price"));

                        return (values.getJSONObject(i).getString("name")+" Rs."+values.getJSONObject(i).getString("price"));
                    }


                }

            }

            valuesAdapter.notifyDataSetChanged();

        } catch (Exception e) {

            Log.e("Exception 2",e.toString());

        }

         return "";
    }


    public void show_customization_popup(String jsonarray) {

        JSONArray values;
        AppController.getInstance().selected_customization.clear();
        customization_names.clear();
        customization_title.setText("Customization");

        try {

            customizations = new JSONArray(jsonarray);

            for (int i=0;i<customizations.length();i++)
            {
                values = new JSONArray(customizations.getJSONObject(i).getString("values"));

                customization_names.add(customizations.getJSONObject(i).getString("name")+" : "+values.getJSONObject(0).getString("name")+" Rs."+values.getJSONObject(0).getString("price"));

                AppController.getInstance().selected_customization.put(customizations.getJSONObject(i).getString("name"),values.getJSONObject(0).getString("name")+" Rs."+values.getJSONObject(0).getString("price"));

                if(customizations.getJSONObject(i).getString("name").equalsIgnoreCase("size"))
                {
                    AppController.getInstance().current_selected_size = values.getJSONObject(0).getString("weight");
                }

            }



            Log.e("current_selected_size",  AppController.getInstance().current_selected_size);
            Log.e("sel_cus 0", AppController.getInstance().selected_customization.toString());

            adapter.notifyDataSetChanged();

            customization.show();

        } catch (Exception e) {

            Log.e("Exception",e.toString());
        }

    }


    private void show_sub_menu(String valuesJSONArray, String customization_title) {

        String temp_value="";// Large Rs.300
        String selected_size="";//Large , Small
        String selected_customization_value = AppController.getInstance().selected_customization.get(customization_title);// Large Rs.300 or Onion Rs.10
        AppController.getInstance().customization_category=customization_title;// Size or Topings
        value_title.setText(customization_title); // Size

        customization_values.clear();
        customization_values_selected.clear();
        AppController.getInstance().current_sizes.clear();

        Iterator it = AppController.getInstance().selected_customization.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();

            if(pair.getKey().toString().equalsIgnoreCase("size"))
            {
                selected_size = pair.getValue().toString();
                Log.e("selected_size","Iterator"+selected_size);
                break;
            }

        }

        selected_size = selected_size.substring(0,selected_size.indexOf("Rs.")-1);

        Log.e("selected_size",selected_size);

        try {

            JSONArray values = new JSONArray(valuesJSONArray);

            for(int i=0;i<values.length();i++)
            {
                if(!customization_title.equalsIgnoreCase("size"))
                {
                    Log.e(values.getJSONObject(i).getString("weight"),selected_size);

                    if(values.getJSONObject(i).getString("weight").equalsIgnoreCase(AppController.getInstance().current_selected_size) || values.getJSONObject(i).getString("weight").isEmpty())
                    {
                        temp_value = values.getJSONObject(i).getString("name")+" Rs."+values.getJSONObject(i).getString("price");

                        if(temp_value.equals(selected_customization_value))
                        {
                            customization_values_selected.add(true);
                        }else
                        {
                            customization_values_selected.add(false);
                        }

                        customization_values.add(values.getJSONObject(i).getString("name")+" Rs."+values.getJSONObject(i).getString("price"));
                    }


                }else
                {
                    temp_value = values.getJSONObject(i).getString("name")+" Rs."+values.getJSONObject(i).getString("price");

                    if(temp_value.equals(selected_customization_value))
                    {
                        customization_values_selected.add(true);
                        AppController.getInstance().current_selected_size = values.getJSONObject(i).getString("weight");

                    }else
                    {
                        customization_values_selected.add(false);
                    }

                    customization_values.add(values.getJSONObject(i).getString("name")+" Rs."+values.getJSONObject(i).getString("price"));
                    AppController.getInstance().current_sizes.add(values.getJSONObject(i).getString("weight"));
                }

            }


            Log.e("current_selected_size",AppController.getInstance().current_selected_size);


            valuesAdapter.notifyDataSetChanged();
            if(AppController.getInstance().customization_category.contains("Toppings") || AppController.getInstance().customization_category.contains("Add-ons"))
            {
                ok_customization.setVisibility(View.VISIBLE);
                ok_customization_ll.setVisibility(View.VISIBLE);

            }else {

                ok_customization.setVisibility(View.GONE);
                ok_customization_ll.setVisibility(View.GONE);
            }
            value_selection.show();

        } catch (Exception e) {

            Log.e("Exception 2",e.toString());

        }

    }

    private void get_menu() {

        loading.show();

        Log.e("get_menu","url"+getString(R.string.base_url)+getString(R.string.menus)+restaurant_id+"/"+AppController.getInstance().option_selected);

        get_menu  = new StringRequest(Request.Method.GET,getString(R.string.base_url)+getString(R.string.menus)+restaurant_id+"/"+AppController.getInstance().option_selected,

//        Log.e("get_menu","url"+getString(R.string.base_url)+getString(R.string.menus)+restaurant_id+"/336");
//
//        get_menu  = new StringRequest(Request.Method.GET,getString(R.string.base_url)+getString(R.string.menus)+restaurant_id+"/336",

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

//                        response="[{\n" +
//                                "\t\"category_id\": \"2\",\n" +
//                                "\t\"category\": \"Dosa\",\n" +
//                                "\t\"servicetax\": \"14\",\n" +
//                                "\t\"menus\": [{\n" +
//                                "\t\t\"menu_id\": \"186\",\n" +
//                                "\t\t\"menu\": \"Masal Dosa\",\n" +
//                                "\t\t\"price\": \"50\",\n" +
//                                "\t\t\"size\": \"\",\n" +
//                                "\t\t\"image\": \"uploads\\/images\\/thumbnails\\/\",\n" +
//                                "\t\t\"type\": \"veg\",\n" +
//                                "\t\t\"itemPreparation_time\": \"10\",\n" +
//                                "\t\t\"customisation\": false\n" +
//                                "\t},\n" +
//                                "\t{\n" +
//                                "\t\t\"menu_id\": \"185\",\n" +
//                                "\t\t\"menu\": \"Plain Dosa\",\n" +
//                                "\t\t\"price\": \"30\",\n" +
//                                "\t\t\"size\": \"\",\n" +
//                                "\t\t\"image\": \"uploads\\/images\\/thumbnails\\/\",\n" +
//                                "\t\t\"type\": \"veg\",\n" +
//                                "\t\t\"itemPreparation_time\": \"10\",\n" +
//                                "\t\t\"customisation\": false\n" +
//                                "\t}]\n" +
//                                "},\n" +
//                                "{\n" +
//                                "\t\"category_id\": \"3\",\n" +
//                                "\t\"category\": \"Chinese\",\n" +
//                                "\t\"servicetax\": \"14\",\n" +
//                                "\t\"menus\": [{\n" +
//                                "\t\t\"menu_id\": \"184\",\n" +
//                                "\t\t\"menu\": \"Gobi\",\n" +
//                                "\t\t\"price\": \"40\",\n" +
//                                "\t\t\"size\": \"\",\n" +
//                                "\t\t\"image\": \"uploads\\/images\\/thumbnails\\/\",\n" +
//                                "\t\t\"type\": \"veg\",\n" +
//                                "\t\t\"itemPreparation_time\": \"20\",\n" +
//                                "\t\t\"customisation\": false\n" +
//                                "\t},\n" +
//                                "\t{\n" +
//                                "\t\t\"menu_id\": \"183\",\n" +
//                                "\t\t\"menu\": \"Noodles\",\n" +
//                                "\t\t\"price\": \"45\",\n" +
//                                "\t\t\"size\": \"\",\n" +
//                                "\t\t\"image\": \"uploads\\/images\\/thumbnails\\/\",\n" +
//                                "\t\t\"type\": \"veg\",\n" +
//                                "\t\t\"itemPreparation_time\": \"20\",\n" +
//                                "\t\t\"customisation\": false\n" +
//                                "\t}]\n" +
//                                "},\n" +
//                                "{\n" +
//                                "\t\"category_id\": \"15\",\n" +
//                                "\t\"category\": \"Pizza\",\n" +
//                                "\t\"servicetax\": \"14\",\n" +
//                                "\t\"menus\": [{\n" +
//                                "\t\t\"menu_id\": \"182\",\n" +
//                                "\t\t\"menu\": \"Veg Pizza\",\n" +
//                                "\t\t\"price\": \"128\",\n" +
//                                "\t\t\"size\": \"\",\n" +
//                                "\t\t\"image\": \"uploads\\/images\\/thumbnails\\/\",\n" +
//                                "\t\t\"type\": \"veg\",\n" +
//                                "\t\t\"itemPreparation_time\": \"20\",\n" +
//                                "\t\t\"customisation\": [{\n" +
//                                "\t\t\t\"type\": \"checklist\",\n" +
//                                "\t\t\t\"name\": \"Size\",\n" +
//                                "\t\t\t\"values\": [{\n" +
//                                "\t\t\t\t\"name\": \"small\",\n" +
//                                "\t\t\t\t\"weight\": \"Small\",\n" +
//                                "\t\t\t\t\"price\": \"100\"\n" +
//                                "\t\t\t},\n" +
//                                "\t\t\t{\n" +
//                                "\t\t\t\t\"name\": \"Medium\",\n" +
//                                "\t\t\t\t\"weight\": \"Medium\",\n" +
//                                "\t\t\t\t\"price\": \"200\"\n" +
//                                "\t\t\t},\n" +
//                                "\t\t\t{\n" +
//                                "\t\t\t\t\"name\": \"Large\",\n" +
//                                "\t\t\t\t\"weight\": \"Large\",\n" +
//                                "\t\t\t\t\"price\": \"300\"\n" +
//                                "\t\t\t}]\n" +
//                                "\t\t},\n" +
//                                "\t\t{\n" +
//                                "\t\t\t\"type\": \"checklist\",\n" +
//                                "\t\t\t\"name\": \"Toppings\",\n" +
//                                "\t\t\t\"values\": [{\n" +
//                                "\t\t\t\t\"name\": \"Onion\",\n" +
//                                "\t\t\t\t\"weight\": \"Small\",\n" +
//                                "\t\t\t\t\"price\": \"10\"\n" +
//                                "\t\t\t},\n" +
//                                "\t\t\t{\n" +
//                                "\t\t\t\t\"name\": \"Onion\",\n" +
//                                "\t\t\t\t\"weight\": \"Medium\",\n" +
//                                "\t\t\t\t\"price\": \"20\"\n" +
//                                "\t\t\t},\n" +
//                                "\t\t\t{\n" +
//                                "\t\t\t\t\"name\": \"Onion\",\n" +
//                                "\t\t\t\t\"weight\": \"Large\",\n" +
//                                "\t\t\t\t\"price\": \"30\"\n" +
//                                "\t\t\t},\n" +
//                                "\t\t\t{\n" +
//                                "\t\t\t\t\"name\": \"Mushroom\",\n" +
//                                "\t\t\t\t\"weight\": \"Small\",\n" +
//                                "\t\t\t\t\"price\": \"15\"\n" +
//                                "\t\t\t},\n" +
//                                "\t\t\t{\n" +
//                                "\t\t\t\t\"name\": \"Mushroom\",\n" +
//                                "\t\t\t\t\"weight\": \"Medium\",\n" +
//                                "\t\t\t\t\"price\": \"25\"\n" +
//                                "\t\t\t},\n" +
//                                "\t\t\t{\n" +
//                                "\t\t\t\t\"name\": \"Mushroom\",\n" +
//                                "\t\t\t\t\"weight\": \"Large\",\n" +
//                                "\t\t\t\t\"price\": \"35\"\n" +
//                                "\t\t\t}]\n" +
//                                "\t\t},\n" +
//                                "\t\t{\n" +
//                                "\t\t\t\"type\": \"checklist\",\n" +
//                                "\t\t\t\"name\": \"Crust\",\n" +
//                                "\t\t\t\"values\": [{\n" +
//                                "\t\t\t\t\"name\": \"Thin Crust\",\n" +
//                                "\t\t\t\t\"weight\": \"\",\n" +
//                                "\t\t\t\t\"price\": \"18\"\n" +
//                                "\t\t\t},\n" +
//                                "\t\t\t{\n" +
//                                "\t\t\t\t\"name\": \"Thick Crust\",\n" +
//                                "\t\t\t\t\"weight\": \"\",\n" +
//                                "\t\t\t\t\"price\": \"28\"\n" +
//                                "\t\t\t},\n" +
//                                "\t\t\t{\n" +
//                                "\t\t\t\t\"name\": \"Ginger\",\n" +
//                                "\t\t\t\t\"weight\": \"\",\n" +
//                                "\t\t\t\t\"price\": \"38\"\n" +
//                                "\t\t\t}]\n" +
//                                "\t\t}]\n" +
//                                "\t},\n" +
//                                "\t{\n" +
//                                "\t\t\"menu_id\": \"181\",\n" +
//                                "\t\t\"menu\": \"Chicken Pizza\",\n" +
//                                "\t\t\"price\": \"248\",\n" +
//                                "\t\t\"size\": \"\",\n" +
//                                "\t\t\"image\": \"uploads\\/images\\/thumbnails\\/\",\n" +
//                                "\t\t\"type\": \"non veg\",\n" +
//                                "\t\t\"itemPreparation_time\": \"20\",\n" +
//                                "\t\t\"customisation\": [{\n" +
//                                "\t\t\t\"type\": \"checklist\",\n" +
//                                "\t\t\t\"name\": \"Size\",\n" +
//                                "\t\t\t\"values\": [{\n" +
//                                "\t\t\t\t\"name\": \"small\",\n" +
//                                "\t\t\t\t\"weight\": \"Small\",\n" +
//                                "\t\t\t\t\"price\": \"200\"\n" +
//                                "\t\t\t},\n" +
//                                "\t\t\t{\n" +
//                                "\t\t\t\t\"name\": \"Medium\",\n" +
//                                "\t\t\t\t\"weight\": \"Medium\",\n" +
//                                "\t\t\t\t\"price\": \"300\"\n" +
//                                "\t\t\t},\n" +
//                                "\t\t\t{\n" +
//                                "\t\t\t\t\"name\": \"Large\",\n" +
//                                "\t\t\t\t\"weight\": \"Large\",\n" +
//                                "\t\t\t\t\"price\": \"400\"\n" +
//                                "\t\t\t}]\n" +
//                                "\t\t},\n" +
//                                "\t\t{\n" +
//                                "\t\t\t\"type\": \"checklist\",\n" +
//                                "\t\t\t\"name\": \"Toppings\",\n" +
//                                "\t\t\t\"values\": [{\n" +
//                                "\t\t\t\t\"name\": \"Onion\",\n" +
//                                "\t\t\t\t\"weight\": \"Small\",\n" +
//                                "\t\t\t\t\"price\": \"20\"\n" +
//                                "\t\t\t},\n" +
//                                "\t\t\t{\n" +
//                                "\t\t\t\t\"name\": \"Onion\",\n" +
//                                "\t\t\t\t\"weight\": \"Medium\",\n" +
//                                "\t\t\t\t\"price\": \"30\"\n" +
//                                "\t\t\t},\n" +
//                                "\t\t\t{\n" +
//                                "\t\t\t\t\"name\": \"Onion\",\n" +
//                                "\t\t\t\t\"weight\": \"Large\",\n" +
//                                "\t\t\t\t\"price\": \"40\"\n" +
//                                "\t\t\t},\n" +
//                                "\t\t\t{\n" +
//                                "\t\t\t\t\"name\": \"Mushroom\",\n" +
//                                "\t\t\t\t\"weight\": \"Small\",\n" +
//                                "\t\t\t\t\"price\": \"25\"\n" +
//                                "\t\t\t},\n" +
//                                "\t\t\t{\n" +
//                                "\t\t\t\t\"name\": \"Mushroom\",\n" +
//                                "\t\t\t\t\"weight\": \"Medium\",\n" +
//                                "\t\t\t\t\"price\": \"35\"\n" +
//                                "\t\t\t},\n" +
//                                "\t\t\t{\n" +
//                                "\t\t\t\t\"name\": \"Mushroom\",\n" +
//                                "\t\t\t\t\"weight\": \"Large\",\n" +
//                                "\t\t\t\t\"price\": \"45\"\n" +
//                                "\t\t\t}]\n" +
//                                "\t\t},\n" +
//                                "\t\t{\n" +
//                                "\t\t\t\"type\": \"checklist\",\n" +
//                                "\t\t\t\"name\": \"Crust\",\n" +
//                                "\t\t\t\"values\": [{\n" +
//                                "\t\t\t\t\"name\": \"Thin Crust\",\n" +
//                                "\t\t\t\t\"weight\": \"\",\n" +
//                                "\t\t\t\t\"price\": \"28\"\n" +
//                                "\t\t\t},\n" +
//                                "\t\t\t{\n" +
//                                "\t\t\t\t\"name\": \"Thick Crust\",\n" +
//                                "\t\t\t\t\"weight\": \"\",\n" +
//                                "\t\t\t\t\"price\": \"38\"\n" +
//                                "\t\t\t},\n" +
//                                "\t\t\t{\n" +
//                                "\t\t\t\t\"name\": \"Ginger\",\n" +
//                                "\t\t\t\t\"weight\": \"\",\n" +
//                                "\t\t\t\t\"price\": \"48\"\n" +
//                                "\t\t\t}]\n" +
//                                "\t\t}]\n" +
//                                "\t}]\n" +
//                                "}]";


                        Log.e("get_menu","response"+ response);

                        if(!response.contains("menus  could not be found for the restaurant"))
                        {
                            menu_from_server=response;
                            setup_ui();


                        }else
                        {
                            loading.dismiss();
//                            Toast.makeText(Menus.this,"No Menus found for the Restaurant",Toast.LENGTH_LONG).show();
                            Toast toast = Toast.makeText(Menus.this," No Menus found for the Store ",Toast.LENGTH_LONG);
                            toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                            toast.show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String error_msg="";

                Log.e("error", "" + volleyError.toString());

                loading.dismiss();

                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured, Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error, Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error, Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured, Please try later";
                }

//                    Toast.makeText(Menus.this,error_msg,Toast.LENGTH_LONG).show();
                Toast toast = Toast.makeText(Menus.this," "+error_msg+" ",Toast.LENGTH_LONG);
                toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                toast.show();

            }
        }) ;

        AppController.getInstance().getRequestQueue().add(get_menu);


    }

    public void build_no_internet_msg() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(Menus.this);
        builder.setMessage("Your Internet seems to be turned OFF, do you want to turn it ON?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();

            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));


    }
    private void setup_ui() {

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Log.e("hasFocus",""+hasFocus);
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {

        Log.e("get_menu","flag1");

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

//        viewPagerAdapter.addFragment(new Menu(), "Recommended");


        try {
            Log.e("get_menu","2");
            JSONArray response_array = new JSONArray(menu_from_server);
            JSONArray menus;

            id.clear();
            name.clear();
            description.clear();
            price.clear();
            veg.clear();
            AppController.getInstance().quantity.clear();

//            Log.e("size",response_array.length()+"");

            AppController.getInstance().itemPreparation_time.clear();
            AppController.getInstance().customization.clear();
            AppController.getInstance().cart.clear();
            AppController.getInstance().selected_customization.clear();

            String temp_totime;


            for(int i =0;i<response_array.length();i++)
            {
                categories.add(response_array.getJSONObject(i).getString("category"));
                AppController.getInstance().delivery_charge = 0;
                AppController.getInstance().servicetax = 0;

                try {

                    if(AppController.getInstance().option_selected!=2)
                    {
                        AppController.getInstance().delivery_charge = Integer.parseInt(response_array.getJSONObject(i).getString("delivery_charge"));
                    }

                    AppController.getInstance().servicetax = Integer.parseInt(response_array.getJSONObject(i).getString("servicetax"));

                    AppController.getInstance().minordervalue = Integer.parseInt(response_array.getJSONObject(i).getString("minordervalue"));

                    AppController.getInstance().payment_mode = response_array.getJSONObject(i).getString("payment_mode");

                    temp_totime = response_array.getJSONObject(i).getString("totime");

                    SimpleDateFormat parser=new SimpleDateFormat("HH:mm:ss");
                    Date date = parser.parse(temp_totime);
                    Calendar closingTime = Calendar.getInstance();
                    closingTime.setTimeInMillis(date.getTime()-(10*60*1000));

                    Log.e("totime","temp_totime"+temp_totime);

                    AppController.getInstance().totime_hr = closingTime.get(Calendar.HOUR_OF_DAY);
                    AppController.getInstance().totime_min = closingTime.get(Calendar.MINUTE);

                    Log.e("totime","temp_totime"+temp_totime+"totime_hr"+AppController.getInstance().totime_hr+"totime_min"+AppController.getInstance().totime_min);

                    AppController.getInstance().discount1 = Integer.parseInt(response_array.getJSONObject(i).getString("discount1"));
                    AppController.getInstance().discount2 = Integer.parseInt(response_array.getJSONObject(i).getString("discount2"));
                    AppController.getInstance().reimb = Integer.parseInt(response_array.getJSONObject(i).getString("reimb"));
                    AppController.getInstance().commission = Integer.parseInt(response_array.getJSONObject(i).getString("commission"));
                    AppController.getInstance().penalty = Integer.parseInt(response_array.getJSONObject(i).getString("penalty"));
                    AppController.getInstance().del_partner_penalty = Integer.parseInt(response_array.getJSONObject(i).getString("del_partner penalty"));



                }catch (Exception e)
                {
                    Log.e("Menus Exception",e.toString());
                }


                Log.e("current_json_array",response_array.getJSONObject(i).toString());

                menus = new JSONArray(response_array.getJSONObject(i).getString("menus"));

                l_o.add(id.size()+"");

                for(int j =0;j<menus.length();j++)
                {

                    id.add(menus.getJSONObject(j).getString("menu_id"));
                    name.add(menus.getJSONObject(j).getString("menu"));
                    description.add(menus.getJSONObject(j).getString("description"));
                    price.add(menus.getJSONObject(j).getString("price"));
                    if(menus.getJSONObject(j).getString("type").equals("veg"))
                    {
                        veg.add("1");
                    }else
                    {
                        veg.add("0");

                    }

//                    Log.e(name.get(j),menus.getJSONObject(j).getString("customisation"));

                    AppController.getInstance().itemPreparation_time.put(menus.getJSONObject(j).getString("menu_id"),Integer.parseInt(menus.getJSONObject(j).getString("itemPreparation_time")));
                    AppController.getInstance().customization.put(menus.getJSONObject(j).getString("menu_id"),menus.getJSONObject(j).getString("customisation"));

                    Log.e("customization",""+AppController.getInstance().customization.toString());

                }


//                Log.e("test 9","---------------------------");

                l_o.set(i,l_o.get(i).toString()+","+(id.size()-1));


//                Log.e("cropped from",l_o.get(i).substring(0,l_o.get(i).indexOf(",")));
//                Log.e("cropped till",l_o.get(i).substring(l_o.get(i).indexOf(",")+1,l_o.get(i).length())+1);
//
//                Log.e("name subset",name.subList(Integer.parseInt(l_o.get(i).substring(0,l_o.get(i).indexOf(","))),Integer.parseInt(l_o.get(i).substring(l_o.get(i).indexOf(",")+1,l_o.get(i).length()))+1).toString());
//
//
//                Log.e("test 9","---------------------------");


                viewPagerAdapter.addFragment(new Menu(), categories.get(i));

            }

            Log.e("after adding", AppController.getInstance().itemPreparation_time.toString());


            for(int i=0;i<id.size();i++)
            {
                AppController.getInstance().quantity.put(id.get(i),0);
            }

            Log.e("quantity",AppController.getInstance().quantity.toString());

//            Log.e("names",name.toString());

        } catch (JSONException e) {
            e.printStackTrace();

            Log.e("get_menu","Exception"+e.toString());

        }

        viewPager.setAdapter(viewPagerAdapter);
        min.setText(getResources().getString(R.string.min)+AppController.getInstance().minordervalue);
        loading.dismiss();
    }



    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();


        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {

            Bundle b=new Bundle();

//            if(position!=0)
//            {
//                position--;
                int from,to;

//                Log.e("all names",""+name.toString());


                from = Integer.parseInt(l_o.get(position).substring(0,l_o.get(position).indexOf(",")));
                to = Integer.parseInt(l_o.get(position).substring(l_o.get(position).indexOf(",")+1,l_o.get(position).length()))+1;

//                Log.e("from"+from,"to"+to);
//
//                Log.e("croped names ",new ArrayList<String>(name.subList(from,to)).toString());

                b.putStringArrayList("id",new ArrayList<String>(id.subList(from,to)));
                b.putStringArrayList("name",new ArrayList<String>(name.subList(from,to)));
                b.putStringArrayList("description",new ArrayList<String>(description.subList(from,to)));
                b.putStringArrayList("price",new ArrayList<String>(price.subList(from,to)));
                b.putStringArrayList("veg",new ArrayList<String>(veg.subList(from,to)));

//                position++;

                b.putString("title",getPageTitle(position)+"");

//            }
//            else
//            {
//                b.putStringArrayList("id",new ArrayList<String>(id.subList(0,id.size())));
//                b.putStringArrayList("name",new ArrayList<String>(name.subList(0,name.size())));
//                b.putStringArrayList("price",new ArrayList<String>(price.subList(0,price.size())));
//                b.putStringArrayList("veg",new ArrayList<String>(veg.subList(0,veg.size())));
//                b.putStringArrayList("categories",categories);
//                b.putStringArrayList("l_o",l_o);
//
//                b.putString("title",getPageTitle(position)+"");
//
//            }

            Fragment f;
            f=mFragmentList.get(position);
            f.setArguments(b);

            return f;

        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

            public void addFragment(Fragment fragment, String title) {

            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);


        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==11 && resultCode==RESULT_OK && data!=null )
        {
            if(data.getExtras().getString("order").equals("done"))
            {
                finish();
            }
        }
    }
}
