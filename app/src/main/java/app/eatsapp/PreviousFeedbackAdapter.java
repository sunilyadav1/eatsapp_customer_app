package app.eatsapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by developer.nithin@gmail.com
 */
public class PreviousFeedbackAdapter  extends BaseAdapter implements View.OnClickListener {
    Activity activity;

    ArrayList<String> Seq;
    ArrayList<String>  Feedbacks;
    ArrayList<String>  Time;

    LayoutInflater inflater;
    PreviousFeedbacks mContext;
    Intent i ;
    Bundle b;


    public PreviousFeedbackAdapter(Activity activity, ArrayList<String> Seq, ArrayList<String> Feedbacks, PreviousFeedbacks mContext, ArrayList<String> Time)
    {
        // TODO Auto-generated constructor stub
        this.activity = activity;
        this.Seq =Seq;
        this.Feedbacks = Feedbacks;
        this.Time = Time;

        this.mContext = mContext;
        i  = new Intent(activity,AddEditAddress.class);
        b = new Bundle();

    }

    @Override
    public int getCount()
    {
        // TODO Auto-generated method stub
        return Seq.size();
    }

    @Override
    public Object getItem(int position)
    {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int id)
    {
        // TODO Auto-generated method stub
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2)
    {
        // TODO Auto-generated method stub
        if(inflater == null)
        {
            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if(convertView ==null)
        {
            convertView = inflater.inflate(R.layout.feedback_row, null);
        }

        TextView feedback =(TextView)convertView.findViewById(R.id.feedback);
        TextView time =(TextView)convertView.findViewById(R.id.time);

        ImageButton delete=(ImageButton)convertView.findViewById(R.id.delete);

        feedback.setText(Feedbacks.get(position) );
        time.setText(Time.get(position) );


        feedback.setTag(position+"");
        delete.setTag(position+"");

        feedback.setOnClickListener(PreviousFeedbackAdapter.this);
        delete.setOnClickListener(PreviousFeedbackAdapter.this);


        return convertView;
    }

    @Override
    public void onClick(View v) {

        int position = Integer.parseInt(v.getTag().toString());

        switch (v.getId())
        {
            case R.id.feedback:
                ((PreviousFeedbacks)mContext).show_popup(Seq.get(position),Feedbacks.get(position));
                break;

            case R.id.delete:((PreviousFeedbacks)mContext).delete_feedback(position);
                break;

             default:
                break;

        }




    }
}