package app.eatsapp;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by developer.nithin@gmail.com
 */


public class MenuAdapter extends BaseAdapter implements View.OnClickListener{

    Activity activity;

    ArrayList<String> Ids;
    ArrayList<String>  Names;
    ArrayList<String> Prices;
    ArrayList<String> Vegs;
    ArrayList<String> description;
    LayoutInflater inflater;
    Menu menu;
    View.OnClickListener descriptionClickListener;

    public MenuAdapter(Activity activity, ArrayList<String> Ids, ArrayList<String> Names, ArrayList<String> Prices, ArrayList<String> Vegs, final Menu menu, ArrayList<String> description)
    {
        // TODO Auto-generated constructor stub
        this.activity = activity;
        this.Ids =Ids;
        this.Names =Names;
        this.Prices =Prices;
        this.Vegs =Vegs;
        this.description =description;
        this.menu = menu;

        descriptionClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e("descriptionClick",Integer.parseInt(view.getTag().toString())+"");
                menu.show_description(Integer.parseInt(view.getTag().toString()));
            }
        };

    }

    @Override
    public int getCount()
    {
        // TODO Auto-generated method stub
        return Ids.size();
    }

    @Override
    public Object getItem(int position)
    {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int id)
    {
        // TODO Auto-generated method stub
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2)
    {
        // TODO Auto-generated method stub
        if(inflater == null)
        {
            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


       if(!Ids.get(position).equals("0"))
       {
           if(convertView ==null || !convertView.getTag().toString().equals("menu_cell"))
           {
               convertView = inflater.inflate(R.layout.new_menu_cell, null);
               convertView.setTag("menu_cell");
           }


           TextView name =(TextView)convertView.findViewById(R.id.name);
           TextView cost  =(TextView)convertView.findViewById(R.id.cost);
           TextView view_details  =(TextView)convertView.findViewById(R.id.view_details);
           ImageView photo  =(ImageView)convertView.findViewById(R.id.photo);
           ImageButton add  =(ImageButton)convertView.findViewById(R.id.add);
           ImageButton minus  =(ImageButton)convertView.findViewById(R.id.minus);
           EditText total =(EditText)convertView.findViewById(R.id.total);

           add.setTag(position);
           view_details.setTag(position);
           minus.setTag(position);

           try
           {
               total.setText(AppController.getInstance().quantity.get(Ids.get(position)).toString());

           }catch (Exception e)
           {
               Log.e("Exception",e.toString());
               total.setText("0");
           }

           add.setOnClickListener(MenuAdapter.this);
           minus.setOnClickListener(MenuAdapter.this);

           if(!description.get(position).isEmpty())
           {
               view_details.setOnClickListener(descriptionClickListener);
               view_details.setVisibility(View.VISIBLE);

           }else
           {
               view_details.setVisibility(View.GONE);
           }

           name.setText(Names.get(position));
           cost.setText(activity.getString(R.string.Rs)+" "+ Prices.get(position));

           if(Vegs.get(position).equals("1"))
           {
               photo.setImageResource(R.drawable.veg);
           }else
           {
               photo.setImageResource(R.drawable.non_veg);
           }

           if(AppController.getInstance().veg==true && Vegs.get(position).equals("0"))
           {

               Log.e(Names.get(position)+" was ","non veg");
               convertView = inflater.inflate(R.layout.empty_row, null);
               convertView.setTag("blank");
           }

       }else
       {
           if(convertView ==null || !convertView.getTag().toString().equals("category"))
           {
               convertView = inflater.inflate(R.layout.category, null);
               convertView.setTag("category");
           }

           TextView label =(TextView)convertView.findViewById(R.id.label);


           label.setText(Names.get(position));
       }



        return convertView;
    }

    @Override
    public void onClick(View v) {

        AppController.getInstance().menu = menu;

        int position;

        position = Integer.parseInt(v.getTag().toString());

        int current_value=0;
        int current_cost=0;

        String update_item,current_cost_string;

        current_cost_string=Prices.get(position);
        current_cost_string.substring(current_cost_string.lastIndexOf(" ")+1,current_cost_string.length());
        current_cost=Integer.parseInt(current_cost_string);

        update_item=Ids.get(position);

        current_value=AppController.getInstance().quantity.get(update_item);

        AppController.getInstance().current_value = current_value;
        AppController.getInstance().current_item = update_item;
        AppController.getInstance().current_item_name = Names.get(position);

        Log.e("cur costis",""+current_cost);


       switch (v.getId())
       {
           case R.id.add:

               if (AppController.getInstance().customization.get(update_item).equalsIgnoreCase("false")) {

                   current_value++;
                   AppController.getInstance().n_items++;
                   AppController.getInstance().total_cost = AppController.getInstance().total_cost + current_cost;

                   ((TextView) activity.findViewById(R.id.total_items)).setText(AppController.getInstance().n_items + " Items");
                   ((TextView) activity.findViewById(R.id.total_cost)).setText("Total : " + activity.getResources().getString(R.string.Rs) + " " + AppController.getInstance().total_cost + "");

                   AppController.getInstance().quantity.put(update_item, current_value);

                   app.eatsapp.MenuItem item = new app.eatsapp.MenuItem(AppController.getInstance().current_item, AppController.getInstance().current_item_name, current_cost);
                   AppController.getInstance().cart.add(item);

                   ((Menus) activity).show_cart();

               } else {

                   ((Menus) activity).show_customization_popup(AppController.getInstance().customization.get(update_item));

               }

               break;

           case R.id.minus:
                           if(current_value==0)
                           {
                               current_value=0;
                               AppController.getInstance().quantity.put(update_item, current_value);

                           }else
                           {
                               current_value--;
                               AppController.getInstance().n_items--;
                               ((TextView)activity.findViewById(R.id.total_items)).setText(AppController.getInstance().n_items+" Items");

                               AppController.getInstance().quantity.put(update_item, current_value);

                               for(int i=AppController.getInstance().cart.size()-1;i>= 0 ;i--)
                               {
                                   if (AppController.getInstance().cart.get(i).id.equals(update_item))
                                   {
                                       AppController.getInstance().total_cost=AppController.getInstance().total_cost-AppController.getInstance().cart.get(i).cost;
                                       ((TextView)activity.findViewById(R.id.total_cost)).setText("Total : "+activity.getResources().getString(R.string.Rs)+" "+AppController.getInstance().total_cost+"");

                                       AppController.getInstance().cart.remove(i);
                                       break;
                                   }
                               }

                               ((Menus) activity).show_cart();
                           }
               break;

       }

        this.notifyDataSetChanged();

    }
}

