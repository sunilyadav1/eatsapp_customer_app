package app.eatsapp;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.StringRequest;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.HashMap;

import java.util.List;
import java.util.Map;


/**
 * Created by developer.nithin@gmail.com
 */
public class UserName extends AppCompatActivity {

    Button update_firstname;
    TextView title;
    String user_name="";
    StringRequest update_firstname_StringRequest;
    TextInputLayout input_layout_name;
    TextView input_name;
    Dialog loading;
    LinearLayout parent_layout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppController.getInstance().set_task_bg(this);

        setContentView(R.layout.username_layout);

        loading = new Dialog(UserName.this);
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading.setContentView(R.layout.loading);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Welcome");
        setSupportActionBar(toolbar);


        input_name = (TextView)findViewById(R.id.input_name);
        update_firstname=(Button)findViewById(R.id.update_firstname);
        title=(TextView)findViewById(R.id.title);
        input_layout_name = (TextInputLayout)findViewById(R.id.input_layout_name);
        parent_layout = (LinearLayout) findViewById(R.id.parent_layout);

        user_name=AppController.getInstance().sharedPreferences.getString("firstname","");
        input_name.setText(user_name);

        if(user_name.length()!=0)
        {
            title.setText("Your current User Name");
        }

        input_name.addTextChangedListener(new MyTextWatcher(input_name));


        update_firstname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validateName())
                {
                    ask_location_permission();
                }

            }
        });
    }

    private void update() {

        Log.e("update_firstname_url", "" +getString(R.string.base_url)+getString(R.string.updateusers));

        loading.show();

        update_firstname_StringRequest  = new StringRequest(Request.Method.POST,getString(R.string.base_url)+getString(R.string.updateusers),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("response", "" +response);

                       if(response.contains("Success"))
                       {
                           AppController.getInstance().sharedPreferences_editor.putString("firstname",input_name.getText().toString());
                           AppController.getInstance().sharedPreferences_editor.commit();
                           finish();
                           startActivity(new Intent(UserName.this, Navigation.class));
                           loading.dismiss();

                       }else
                       {
                           loading.dismiss();
                           Snackbar snackbar = Snackbar
                                   .make(parent_layout,"Error Occured, Please try later", Snackbar.LENGTH_LONG) ;
                           snackbar.show();

                       }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String error_msg="";

                Log.e("error", "" + volleyError.toString());

                loading.dismiss();

                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured, Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error, Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error, Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured, Please try later";
                }

                Snackbar snackbar = Snackbar
                        .make(parent_layout, error_msg, Snackbar.LENGTH_LONG) ;
                snackbar.show();

            }
        }){
            @Override
            protected Map<String, String> getParams()
            {
                // TODO Auto-generated method stub

                Map<String, String> params = new HashMap<String, String>();

                params.put("firstname",""+input_name.getText().toString());
                params.put("id",""+AppController.getInstance().sharedPreferences.getString("id",""));

                Log.e("params",params.toString());

                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        AppController.getInstance().getRequestQueue().add(update_firstname_StringRequest);

    }

    public void build_no_internet_msg() {



        final AlertDialog.Builder builder = new AlertDialog.Builder(UserName.this);
        builder.setMessage("Your Internet seems to be turned OFF, do you want to turn it ON?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();

            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));


    }

    private boolean validateName() {
        if (input_name.getText().toString().length() == 0) {
            input_layout_name.setError("Please enter your name to continue");
            requestFocus(input_name);
            return false;
        } else {
            input_layout_name.setErrorEnabled(false);
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
           getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_name:
                    validateName();
                    break;
            }
        }

    }

    private void ask_location_permission() {

        Dexter.withActivity(UserName.this)
                .withPermissions(
                        android.Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                ).withListener(new MultiplePermissionsListener() {

            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {

                if (report.areAllPermissionsGranted()) {

                    Log.e("all_permission", "areAllPermissionsGranted");

                    update();

                } else if (report.isAnyPermissionPermanentlyDenied()) {

                    Log.e("all_permission", "isAnyPermissionPermanentlyDenied");


                    AlertDialog.Builder builder = new AlertDialog.Builder(UserName.this);
                    builder.setTitle("Permission");
                    builder.setMessage("Eatsapp needs LOCATION permissions to place/track you orders.Please grant it to continue using the app");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();

                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, 1);
                            Toast.makeText(getBaseContext(), "Go to Permissions to Grant LOCATION", Toast.LENGTH_LONG).show();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();

                } else {
                    Log.e("all_permission", "isAnyPermissionPermanentlyDenied else");

                    AlertDialog.Builder builder = new AlertDialog.Builder(UserName.this);
                    builder.setTitle("Permission");
                    builder.setMessage("Eatsapp needs LOCATION permissions to place/track you orders.Please grant it to continue using the app");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            ask_location_permission();

                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
//                            finish();
                        }
                    });
                    builder.show();

                }

            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                Log.e("all_permission", "onPermissionRationaleShouldBeShown");

                token.continuePermissionRequest();

            }


        }).check();
    }

}
