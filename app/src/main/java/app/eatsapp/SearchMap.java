package app.eatsapp;

/**
 * Created by developer.nithin@gmail.com
 */
import android.content.Intent;
import android.os.Bundle;


import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;

import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import android.widget.ProgressBar;

public class SearchMap extends AppCompatActivity
{
    private Integer THRESHOLD = 2;
    private DelayAutoCompleteTextView geo_autocomplete;

    Intent goback;

    ProgressBar progressBar;


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_map);

        AppController.getInstance().set_task_bg(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Search");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        goback = new Intent();

        progressBar=(ProgressBar)findViewById(R.id.progressBar);

        geo_autocomplete = (DelayAutoCompleteTextView) findViewById(R.id.geo_autocomplete);
        geo_autocomplete.setThreshold(THRESHOLD);
        geo_autocomplete.setLoadingIndicator(progressBar);
        geo_autocomplete.setAdapter(new GeoAutoCompleteAdapter(SearchMap.this)); // 'this' is Activity instance

        geo_autocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                GeoSearchResult result = (GeoSearchResult) adapterView.getItemAtPosition(position);
                geo_autocomplete.setText(result.getAddress());

                goback.putExtra("Address",result.getAddress() );
                goback.putExtra("Lat",result.getLat() );
                goback.putExtra("Long",result.getLong() );
                goback.putExtra("name",result.get_place_name() );
                setResult(RESULT_OK, goback);
                finish();

            }
        });


        geo_autocomplete.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}