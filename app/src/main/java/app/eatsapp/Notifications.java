package app.eatsapp;

import android.app.Dialog;
import android.app.NativeActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.android.volley.toolbox.StringRequest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;

import java.util.Map;

/**
 * Created by developer.nithin@gmail.com
 */
public class Notifications extends AppCompatActivity {

    ArrayList<String> Seq =new ArrayList<>();
    ArrayList<String> Details=new ArrayList<>();
    ArrayList<String> Time=new ArrayList<>();
    NotificationAdapter notificationAdapter;
    ListView listView;
    StringRequest get_notifications;
    LinearLayout parent_layout;

    Dialog loading;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat sdf_new = new SimpleDateFormat("dd-MM-yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        

        setContentView(R.layout.notification);

        AppController.getInstance().set_task_bg(this);

        loading = new Dialog(Notifications.this);
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading.setContentView(R.layout.loading);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Notifications");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView=(ListView)findViewById(R.id.listView);
        parent_layout=(LinearLayout) findViewById(R.id.parent_layout);

        notificationAdapter = new NotificationAdapter(Notifications.this,Seq,Details,Time);

        listView.setAdapter(notificationAdapter);



    }

    private void get_notifications() {

        Log.e("get_notifications_url", "" +getString(R.string.base_url)+getString(R.string.notificaions)+"/"+AppController.getInstance().sharedPreferences.getString("id",""));

        loading.show();

        get_notifications  = new StringRequest(Request.Method.GET,getString(R.string.base_url)+getString(R.string.notificaions)+"/"+AppController.getInstance().sharedPreferences.getString("id",""),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("response", "" +response);

                        loading.dismiss();

                        Seq.clear();
                        Details.clear();
                        Time.clear();

                        try {

                            JSONArray jsonArray = new JSONArray(response);

                            for (int i = 0;i<jsonArray.length();i++)
                            {
                                Seq.add(jsonArray.getJSONObject(i).getString("id"));
                                Details.add(jsonArray.getJSONObject(i).getString("message"));
                                try {
                                        Time.add(sdf_new.format((Date)sdf.parse(jsonArray.getJSONObject(i).getString("date"))));

                                }catch (Exception e) {
                                    Log.e("Exception", "" +e.toString());
                                }


                            }

                            notificationAdapter.notifyDataSetChanged();

                        } catch (Exception e) {
                            Log.e("Exception", "" +e.toString());
                        }

                        loading.dismiss();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String error_msg="";

                Log.e("error", "" + volleyError.toString());

                loading.dismiss();

                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured, Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error, Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error, Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured, Please try later";
                }

                Snackbar snackbar = Snackbar
                        .make(parent_layout, error_msg, Snackbar.LENGTH_LONG) ;
                snackbar.show();

            }
        }) ;

        AppController.getInstance().getRequestQueue().add(get_notifications);

    }

    public void build_no_internet_msg() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(Notifications.this);
        builder.setMessage("Your Internet seems to be turned OFF, do you want to turn it ON?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();

            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(Notifications.this, Navigation.class));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        get_notifications();
        AppController.getInstance().no_of_notifications=0;
        AppController.getInstance().nm.cancelAll();
    }
}
