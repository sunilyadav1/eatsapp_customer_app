package app.eatsapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * Created by developer.nithin@gmail.com
 */
public class Terms extends AppCompatActivity{

    private Toolbar toolbar;
    TextView details;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms);

        AppController.getInstance().set_task_bg(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Terms & Condition");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        details=(TextView)findViewById(R.id.details);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        int position = Integer.parseInt(getIntent().getStringExtra("position"));

        switch (position)
        {
            case 0:details.setText(getResources().getString(R.string.about));
                toolbar.setTitle("About Us");
                break;

            case 1:details.setText(getResources().getString(R.string.terms));
                toolbar.setTitle("Terms of Service");
                break;

            case 2:details.setText(getResources().getString(R.string.privacy));
                toolbar.setTitle("Privacy Policy");
                break;

            case 3:details.setText(getResources().getString(R.string.cancellations));
                toolbar.setTitle("Cancellations and Refunds Policy");
                break;

            case 4:details.setText(getResources().getString(R.string.disclaimers));
                toolbar.setTitle("Disclaimers and Indemnification");
                break;

            case 5:details.setText(getResources().getString(R.string.contact));
                toolbar.setTitle("Contact Us");
                break;

            default:break;
        }

    }
}
