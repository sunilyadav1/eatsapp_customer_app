package app.eatsapp;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;


import java.util.HashMap;

import java.util.Map;


public class suggest_restaurant extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    Dialog loading;

    TextInputLayout input_layout_name;
    TextInputLayout input_layout_location;
    TextInputLayout input_layout_ph;
    TextInputLayout input_layout_email;


    TextView input_name;
    TextView input_location;
    TextView input_ph;
    TextView input_email;

    View fragment_view;

    Button locate,submit;

    StringRequest insert_suggest_restaurant;

    LinearLayout parent_layout;


    public suggest_restaurant() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment suggest_restaurant.
     */
    // TODO: Rename and change types and number of parameters
    public static suggest_restaurant newInstance(String param1, String param2) {
        suggest_restaurant fragment = new suggest_restaurant();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragment_view = inflater.inflate(R.layout.fragment_suggest_restaurant, container, false);

        Log.e("suggest","v3");

        loading = new Dialog(getActivity());
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading.setContentView(R.layout.loading);


        input_layout_name = (TextInputLayout) fragment_view.findViewById(R.id.input_layout_name);
        input_layout_location = (TextInputLayout) fragment_view.findViewById(R.id.input_layout_location);
        input_layout_ph = (TextInputLayout) fragment_view.findViewById(R.id.input_layout_ph);
        input_layout_email = (TextInputLayout) fragment_view.findViewById(R.id.input_layout_email);

        parent_layout = (LinearLayout) fragment_view.findViewById(R.id.parent_layout);


        input_name = (TextView) fragment_view.findViewById(R.id.input_name);
        input_location = (TextView) fragment_view.findViewById(R.id.input_location);
        input_ph = (TextView) fragment_view.findViewById(R.id.input_ph);
        input_email = (TextView) fragment_view.findViewById(R.id.input_email);
        locate = (Button) fragment_view.findViewById(R.id.locate);
        submit = (Button) fragment_view.findViewById(R.id.submit);


        input_name.addTextChangedListener(new MyTextWatcher(input_name));
        input_location.addTextChangedListener(new MyTextWatcher(input_location));


        locate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(),LocateOnMap.class).putExtra("from","Restaurant"),9);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
               if(validateName() && validateLocation())
               {
                   if(!ConnectivityReceiver.isConnected())
                   {
//                       Toast.makeText(getActivity(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                       Toast toast = Toast.makeText(getActivity()," Not Internet Connection ",Toast.LENGTH_LONG);
                       toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                       toast.show();

                       build_no_internet_msg();

                   }else
                   {
                       insert_suggest_restaurant();
                   }

               }
            }
        });


        return fragment_view;

    }

    private void insert_suggest_restaurant() {

        Log.e("insert_suggest_rest_url", "" +getString(R.string.base_url)+getString(R.string.Restaurant));

        loading.show();

        insert_suggest_restaurant = new StringRequest(Request.Method.POST,getString(R.string.base_url)+getString(R.string.Restaurant),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        loading.dismiss();

                        Log.e("response", "" +response);

                        if(response.contains("Success"))
                        {
                            input_name.setText(" ");
                            input_ph.setText(" ");
                            input_location.setText(" ");
                            input_email.setText(" ");
//                            Toast.makeText(getActivity(),"Thanks for your Suggestion",Toast.LENGTH_LONG).show();
                            Toast toast = Toast.makeText(getActivity()," Thanks for your Suggestion ",Toast.LENGTH_LONG);
                            toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                            toast.show();


                            input_name.clearFocus();
                            input_ph.clearFocus();
                            input_location.clearFocus();
                            input_email.clearFocus();

                            getActivity().onBackPressed();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String error_msg="";

                Log.e("error", "" + volleyError.toString());

                loading.dismiss();

                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured, Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error, Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error, Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured, Please try later";
                }

                Snackbar snackbar = Snackbar
                        .make(parent_layout, error_msg, Snackbar.LENGTH_LONG) ;
                snackbar.show();

            }
        }){
            @Override
            protected Map<String, String> getParams()
            {
                // TODO Auto-generated method stub

                Map<String, String> params = new HashMap<String, String>();

                params.put("restaurant_name",""+input_name.getText().toString());
                params.put("location",input_location.getText().toString());
                params.put("phone_number",input_ph.getText().toString());
                params.put("email",input_email.getText().toString());

                params.put("user_id",AppController.getInstance().sharedPreferences.getString("id", "")+"");

                Log.e("params", "" + params.toString());

                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        insert_suggest_restaurant.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 0;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
                Log.e("error", error.toString());
            }
        });


        AppController.getInstance().getRequestQueue().add(insert_suggest_restaurant);
    }

    public void build_no_internet_msg() {



        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Your Internet seems to be turned OFF, do you want to turn it ON?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();

            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 9 && data != null) {
            Log.e("returned",""+data.getExtras().getString("Address"));

            input_location.setText(data.getExtras().getString("Address"));
        }
    }

    private boolean validateName() {
        if (input_name.getText().toString().length() == 0) {
            input_layout_name.setError("Enter Outlet name");
            requestFocus(input_name);
            return false;
        } else {
            input_layout_name.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateLocation() {
        if (input_location.getText().toString().length() == 0) {
            input_layout_location.setError("Enter Outlet location");
            requestFocus(input_location);
            return false;
        } else {
            input_layout_location.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_name:
                    String newline = System.getProperty("line.separator");

                    if(input_name.getText().toString().contains(newline))
                    {
                        input_name.setText(input_name.getText().toString().replace(newline,""));
                        input_layout_location.requestFocus();
                    }

                    validateName();
                    break;
                case R.id.input_location:
                    validateLocation();
                    break;

            }
        }

    }


}
