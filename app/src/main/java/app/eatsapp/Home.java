package app.eatsapp;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
//import com.google.android.gms.location.places.Place;
//import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.iid.FirebaseInstanceId;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
//import com.thefinestartist.finestwebview.FinestWebView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import app.eatsapp.autocomplete.PlacesAutoCompleteActivity;


public class Home extends Fragment implements View.OnClickListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener {
    // TODO: Rename parameter arguments, choose names that match

    Boolean refresh = false;

    int flow = 0;

    String marker_tag = "";

    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 11;

    AddressListAdapter addressAdapter;

    JSONArray jsonArray;

    String rs;
    String full_address = "", google_full_address = "";

    Handler handler = null;
    Runnable t;

    LocationManager manager = null;

    RelativeLayout parent_layout;

    StringRequest get_Addresses, get_pitstop, get_restaurants_related_to_pitstop, current_orders_request, get_delivery_boy_location, get_restaurants_within_3km, adduserslocation, get_here_locations_list, get_here_selected_coordinates, search_restaurant_for_pickup;

    ProgressBar list_popup_progressBar, current_orders_progressBar, address_orders_progressBar;

    RestaurantAdapter restaurantAdapter;
    CurrentOrderAdapter currentOrderAdapter;
    ArrayAdapter<String> here_adapter;

    ImageButton call, my_location;
    Button add_address;

    LatLngBounds bounds;

    ListView listView, current_orders_listView, address_orders_listView, here_listView;

    ArrayList<String> Seq = new ArrayList<String>();
    ArrayList<String> Names = new ArrayList<String>();
    ArrayList<String> Times = new ArrayList<String>();
    ArrayList<String> Photo_Url = new ArrayList<String>();
    ArrayList<String> rest_lat = new ArrayList<String>();
    ArrayList<String> rest__long = new ArrayList<String>();


    ArrayList<String> current_orders_Seq = new ArrayList<String>();
    ArrayList<String> current_orders_Names = new ArrayList<String>();
    ArrayList<String> current_orders_Items = new ArrayList<String>();
    ArrayList<String> current_orders_Cost = new ArrayList<String>();

    ArrayList<String> current_orders_Passcode = new ArrayList<String>();
    ArrayList<String> current_orders_Order_type = new ArrayList<String>();
    ArrayList<String> current_orders_Delivered_by = new ArrayList<String>();
    ArrayList<String> current_orders_Status = new ArrayList<String>();
    ArrayList<String> rms = new ArrayList<String>();
    ArrayList<String> current_orders_name = new ArrayList<String>();
    ArrayList<String> current_orders_ph = new ArrayList<String>();
    ArrayList<String> current_orders_shipping_lat = new ArrayList<String>();
    ArrayList<String> current_orders_shipping_long = new ArrayList<String>();


    ArrayList<String> address_Seq = new ArrayList<String>();
    ArrayList<String> address_Names = new ArrayList<String>();
    ArrayList<String> address_Addresses = new ArrayList<String>();


    ArrayList<String> here_id = new ArrayList<String>();
    ArrayList<String> here_locations = new ArrayList<String>();
    ArrayList<LatLng> here_latlong = new ArrayList<LatLng>();

    ArrayList<LatLng> polyline = new ArrayList<LatLng>();

    ImageView pin;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    LinearLayout linearLayout;
    RelativeLayout track_order_bar;

    int markers = 0, option_selected = 0, selected_pitstop;

    String selected_pitstop_name = "";

    Boolean locked = false, show_current_location = false, searchable = true;

    DirectionsJSONParser parser;

    Geocoder geocoder;
    List<Address> addresses;

    TextView title, address_title, pitstop_title, here_title;

    LayoutInflater layoutInflater;


    View fragment_view;
    private GoogleMap mMap;
    SupportMapFragment mapFragment;
    LinearLayout in_car_bg, here_bg, pickup_bg, home_bg;
    ImageView in_car_img, here_img, pickup_img, home_img;
    TextView in_car_text, here_text, pickup_text, home_text, user_msg;

    LatLng co_ordinates;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private Location mLastLocation;

    private GoogleApiClient mGoogleApiClient;
    private boolean mRequestingLocationUpdates = true;
    private LocationRequest mLocationRequest;
    private com.google.android.gms.location.LocationListener locationListener;

    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 3000; // 3 sec
    private static int FATEST_INTERVAL = 3000; // 3 sec
    private static int DISPLACEMENT = 10; // 10 meters

    Boolean first = true, drawn = false, user_drawn = false;

    String last_lat, last_long, zoom_value;

    TextView action_hint, address, go, total_cost_current_order, order_no, delivered_by, passcode;
    RelativeLayout address_bar;

    Dialog loading, restaurants, current_orders, address_list, here_Dialog;

    String current_ph = "";

    int current_position = 0;

    Location delivery_boy_location = new Location("delivery_boy_location");
    Location destination_location = new Location("destination_location");
    Location delivery_location = new Location("delivery_location");


    String user_destination_time, delivery_boy_destination_time;
    String user_destination_distance, delivery_boy_destination_distance;

    Marker destination_marker, delivery_boy_marker;

    Bundle restaurant_b;
    Intent restaurant_i;

    Home mContext;

    Dialog search_restaurant_popup;
    EditText search_restaurant_name, search_locality;
    TextView search, search_alert;

    String input_restaurant_name = "", input_locality = "";

    String current_city = "";

    String query_url = "";

    String deviceDetails = "";

    Boolean tracking = false;

    Boolean hideTime = false;

    HashMap<String,LatLng> collectionPoints = new HashMap<>();

    public Home() {
        // Required empty public constructor
    }

    public static Home newInstance(String param1, String param2) {
        Home fragment = new Home();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

//        deviceDetails = "m: " + Build.MODEL + " b: " + Build.BRAND + " r: " + Build.VERSION.RELEASE;

        ask_location_permission();

        Log.e("home", "version_32");

        Log.e("getToken",FirebaseInstanceId.getInstance().getToken()+"");

        mContext = this;

        restaurant_b = new Bundle();
        restaurant_i = new Intent(getActivity(), Menus.class);

        fragment_view = inflater.inflate(R.layout.fragment_home, container, false);

        search_restaurant_popup = new Dialog(getActivity());
        search_restaurant_popup.requestWindowFeature(Window.FEATURE_NO_TITLE);
        search_restaurant_popup.setContentView(R.layout.search_popup);

        search_restaurant_name = (EditText) search_restaurant_popup.findViewById(R.id.search_restaurant_name);
        search_locality = (EditText) search_restaurant_popup.findViewById(R.id.search_locality);
        search = (TextView) search_restaurant_popup.findViewById(R.id.search);
        search_alert = (TextView) search_restaurant_popup.findViewById(R.id.search_alert);


        layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        rs = getActivity().getResources().getString(R.string.Rs) + " ";

        parent_layout = (RelativeLayout) fragment_view.findViewById(R.id.parent_layout);

        restaurantAdapter = new RestaurantAdapter(getActivity(), Seq, Names, Times, Photo_Url);
        currentOrderAdapter = new CurrentOrderAdapter(getActivity(), current_orders_Seq, current_orders_Names, current_orders_Items, current_orders_Cost, current_orders_Passcode, current_orders_Order_type);
        addressAdapter = new AddressListAdapter(getActivity(), address_Seq, address_Names, address_Addresses, false);

        restaurants = new Dialog(getActivity());
        restaurants.requestWindowFeature(Window.FEATURE_NO_TITLE);
        restaurants.setContentView(R.layout.list_popup);

        current_orders = new Dialog(getActivity());
        current_orders.requestWindowFeature(Window.FEATURE_NO_TITLE);
        current_orders.setContentView(R.layout.list_popup);

        address_list = new Dialog(getActivity());
        address_list.requestWindowFeature(Window.FEATURE_NO_TITLE);
        address_list.setContentView(R.layout.list_popup);


        list_popup_progressBar = (ProgressBar) restaurants.findViewById(R.id.progressBar);
        current_orders_progressBar = (ProgressBar) current_orders.findViewById(R.id.progressBar);
        address_orders_progressBar = (ProgressBar) address_list.findViewById(R.id.progressBar);

        pitstop_title = (TextView) restaurants.findViewById(R.id.title);
        pitstop_title.setText("");

        title = (TextView) current_orders.findViewById(R.id.title);
        title.setText("Select an order to track it");

        address_title = (TextView) address_list.findViewById(R.id.title);
        add_address = (Button) address_list.findViewById(R.id.add_address);
        address_title.setText("Select Delivery Address");

        listView = (ListView) restaurants.findViewById(R.id.listView);
        current_orders_listView = (ListView) current_orders.findViewById(R.id.listView);
        address_orders_listView = (ListView) address_list.findViewById(R.id.listView);

        listView.setAdapter(restaurantAdapter);
        current_orders_listView.setAdapter(currentOrderAdapter);
        address_orders_listView.setAdapter(addressAdapter);

        restaurantAdapter.notifyDataSetChanged();
        currentOrderAdapter.notifyDataSetChanged();
        addressAdapter.notifyDataSetChanged();

        loading = new Dialog(getActivity());
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading.setContentView(R.layout.loading);

        here_Dialog = new Dialog(getActivity());
        here_Dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        here_Dialog.setContentView(R.layout.list_popup);

        here_listView = (ListView) here_Dialog.findViewById(R.id.listView);
        here_title = (TextView) here_Dialog.findViewById(R.id.title);
        here_title.setText("Select a location");

        here_adapter = new ArrayAdapter<String>(getActivity(), R.layout.here_cell, R.id.city, here_locations);
        here_listView.setAdapter(here_adapter);


        linearLayout = (LinearLayout) fragment_view.findViewById(R.id.linearLayout);
        track_order_bar = (RelativeLayout) fragment_view.findViewById(R.id.track_order_bar);

        last_lat = AppController.getInstance().sharedPreferences.getString("last_lat", "empty");
        last_long = AppController.getInstance().sharedPreferences.getString("last_long", "empty");

        pin = (ImageView) fragment_view.findViewById(R.id.pin);

        in_car_bg = (LinearLayout) fragment_view.findViewById(R.id.in_car_bg);
        here_bg = (LinearLayout) fragment_view.findViewById(R.id.here_bg);
        pickup_bg = (LinearLayout) fragment_view.findViewById(R.id.pickup_bg);
        home_bg = (LinearLayout) fragment_view.findViewById(R.id.home_bg);

        in_car_img = (ImageView) fragment_view.findViewById(R.id.in_car_img);
        here_img = (ImageView) fragment_view.findViewById(R.id.here_img);
        pickup_img = (ImageView) fragment_view.findViewById(R.id.pickup_img);
        home_img = (ImageView) fragment_view.findViewById(R.id.home_img);


        in_car_text = (TextView) fragment_view.findViewById(R.id.in_car_text);
        here_text = (TextView) fragment_view.findViewById(R.id.here_text);
        pickup_text = (TextView) fragment_view.findViewById(R.id.pickup_text);
        home_text = (TextView) fragment_view.findViewById(R.id.home_text);
        user_msg = (TextView) fragment_view.findViewById(R.id.user_msg);


        action_hint = (TextView) fragment_view.findViewById(R.id.action_hint);
        address = (TextView) fragment_view.findViewById(R.id.address);
        go = (TextView) fragment_view.findViewById(R.id.go);


        total_cost_current_order = (TextView) fragment_view.findViewById(R.id.total_cost_current_order);
        order_no = (TextView) fragment_view.findViewById(R.id.order_no);
        delivered_by = (TextView) fragment_view.findViewById(R.id.delivered_by);
        passcode = (TextView) fragment_view.findViewById(R.id.passcode);

        call = (ImageButton) fragment_view.findViewById(R.id.call);
        my_location = (ImageButton) fragment_view.findViewById(R.id.my_location);
        my_location.setRotation(180);
//        my_location.setVisibility(View.GONE);

        address_bar = (RelativeLayout) fragment_view.findViewById(R.id.address_bar);

//        build_no_internet_msg();

        search_restaurant_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    search_locality.setText("");
                    Log.e("search_locality", "click");
                }
            }
        });

        search_restaurant_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                String newline = System.getProperty("line.separator");

                if(search_restaurant_name.getText().toString().contains(newline))
                {
                    search_restaurant_name.setText(search_restaurant_name.getText().toString().replace(newline,""));
                    search.performClick();
                }
            }
        });

        search_locality.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                String newline = System.getProperty("line.separator");

                if(search_locality.getText().toString().contains(newline))
                {
                    search_locality.setText(search_locality.getText().toString().replace(newline,""));
                    search.performClick();
                }
            }
        });

        search_locality.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    search_restaurant_name.setText("");
                    Log.e("search_locality", "click");
                }

            }
        });


        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (search_restaurant_name.getText().toString().length() == 0 && search_locality.getText().toString().length() == 0) {
                    search_alert.setVisibility(View.VISIBLE);
                    search_alert.setText("Please enter the Outlet Name or Location");

                } else {
                    input_restaurant_name = "";
                    input_locality = "";
                    search_alert.setVisibility(View.GONE);
                    search_restaurant_popup.dismiss();
                    loading.show();
                    input_restaurant_name = search_restaurant_name.getText().toString();
                    input_locality = search_locality.getText().toString();
                    search_restaurant_for_pickup();
                }

            }
        });

        add_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                address_list.dismiss();

                Bundle b = new Bundle();
                b.putString("new", "yes");
                startActivity(new Intent(getActivity(), AddEditAddress.class).putExtras(b));
            }
        });


        my_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                flow = 1;
                ask_location_permission();


            }
        });


        in_car_bg.setOnClickListener(this);
        here_bg.setOnClickListener(this);
        pickup_bg.setOnClickListener(this);
        home_bg.setOnClickListener(this);

        here_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Log.e("here_listView", "v1");
                here_Dialog.dismiss();

                get_here_coordinates_for_selected_id(here_id.get(position));
            }
        });


        address_orders_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                address_list.dismiss();

                loading.show();

                try {

                    google_full_address = jsonArray.getJSONObject(position).getString("location0") + "," + jsonArray.getJSONObject(position).getString("city_state");
                    full_address = address_Addresses.get(position);
                    Log.e("address to google", google_full_address);

                    if (!ConnectivityReceiver.isConnected()) {

                        Toast toast = Toast.makeText(getActivity(), " No Internet Connection ", Toast.LENGTH_LONG);
                        toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                        toast.show();

                        build_no_internet_msg();


                    } else {

                        AppController.getInstance().full_address = full_address;
//                        new AddressToLatLng().execute();

                        delivery_location.setLatitude(Double.parseDouble(jsonArray.getJSONObject(position).getString("lat") + ""));
                        delivery_location.setLongitude(Double.parseDouble(jsonArray.getJSONObject(position).getString("lng") + ""));

                        Seq.clear();
                        Names.clear();
                        Times.clear();
                        Photo_Url.clear();
                        rest_lat.clear();
                        rest__long.clear();

                        restaurantAdapter.notifyDataSetChanged();
                        Log.e("popup","A");
                        pitstop_title.setText("Select a Store");
                        restaurants.show();
                        loading.dismiss();
                        list_popup_progressBar.setVisibility(View.VISIBLE);

                        get_restaurants_within_3km();


                    }

                } catch (Exception e) {

                    Log.e("address_orders_listView", e.toString());
                    loading.dismiss();
                    Toast toast = Toast.makeText(getActivity(), " Error parsing address ", Toast.LENGTH_LONG);
                    toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                    toast.show();

                }

            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String url;

                restaurant_b.putString("restaurant_id", Seq.get(position));
                restaurant_b.putString("restaurant_name", Names.get(position));
                AppController.getInstance().option_selected = option_selected;
                AppController.getInstance().delivery_location = delivery_location;
                AppController.getInstance().restaurant_id = Seq.get(position);

                if (option_selected == 0) {

                    AppController.getInstance().full_address = "Delivery Point " + selected_pitstop_name;
                    AppController.getInstance().pitstop_id = "" + selected_pitstop;

                    url = getDirectionsUrl(new LatLng(Double.parseDouble(rest_lat.get(position)), Double.parseDouble(rest__long.get(position))), new LatLng(delivery_location.getLatitude(), delivery_location.getLongitude()));

                    DownloadTask downloadTask = new DownloadTask();
                    downloadTask.execute(url, "just_time");

                } else {

                    AppController.getInstance().full_address = full_address;
//                    AppController.getInstance().show_popup_alert(deviceDetails + "\nfull_address\n" + full_address, getActivity());

                }

                if (option_selected == 0) {

                    Log.e("opened from", "f2 618");
                    loading.dismiss();
                    restaurants.dismiss();
                    restaurant_i.putExtras(restaurant_b);
                    startActivity(restaurant_i);

                    Log.e("travel_time_mins", "B" + AppController.getInstance().travel_time_mins);

                } else {

                    url = getDirectionsUrl(new LatLng(Double.parseDouble(rest_lat.get(position)), Double.parseDouble(rest__long.get(position))), new LatLng(delivery_location.getLatitude(), delivery_location.getLongitude()));

                    DownloadTask downloadTask = new DownloadTask();
                    downloadTask.execute(url, "just_time");
                    loading.show();

                }

            }
        });


        current_orders_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                hideTime = true;

                if (handler != null) {
                    handler.removeCallbacks(t);
                }

                mMap.clear();
                drawn = false;
                user_drawn = false;
                reset_all_buttons();

                current_ph = current_orders_ph.get(position);

                // if its being picked up OR if the order type is pick up(cuz delivery boy id will be always 0 for order_type 3)
                if ((!current_orders_Delivered_by.get(position).equals("0")) || Integer.parseInt(current_orders_Order_type.get(position)) == 3) {

                    if (current_orders_Status.get(position).equals("Rejected") || current_orders_Status.get(position).equals("order cancelled")) {

                        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Your order has been cancelled.").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                dialog.cancel();
                            }
                        });
                        final AlertDialog alert = builder.create();
                        alert.show();
                        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));


                    } else if (Integer.parseInt(current_orders_Order_type.get(position)) == 3 && rms.get(position).toString().equals("0")) {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Your order is not yet accepted.").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                dialog.cancel();
                            }
                        });
                        final AlertDialog alert = builder.create();
                        alert.show();
                        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));

                    } else {

                        current_position = position;

                        current_orders.dismiss();
//                    address_bar.setVisibility(View.GONE);
//                    pin.setVisibility(View.GONE);
                        track_order_bar.setVisibility(View.VISIBLE);

                        order_no.setText("# " + current_orders_Seq.get(position));
                        total_cost_current_order.setText(rs + " " + current_orders_Cost.get(position));
                        delivered_by.setText("Delivered By : " + current_orders_name.get(position));

                        if (!current_orders_Order_type.get(position).equals("3")) {
                            passcode.setVisibility(View.VISIBLE);
                            passcode.setText("Passcode : " + current_orders_Passcode.get(position));

                        } else {
                            delivered_by.setText("Pickup From : " + current_orders_Names.get(position));
                            passcode.setVisibility(View.INVISIBLE);
                        }

                        if (ConnectivityReceiver.isConnected()) {
                            switch (Integer.parseInt(current_orders_Order_type.get(position))) {

                                // 1 : IN CAR : check if gps is enabled
                                // get delivery boys latest location and start tracking ,
                                // add destination(pitstop) to map ,
                                // draw path between delivery boy and destination ,
                                // draw path between user and pitstop

                                case 1:
                                    if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                                        option_selected = 4;

                                        destination_location.setLatitude(Double.parseDouble(current_orders_shipping_lat.get(current_position)));
                                        destination_location.setLongitude(Double.parseDouble(current_orders_shipping_long.get(current_position)));

                                        String url = getDirectionsUrl(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), new LatLng(destination_location.getLatitude(), destination_location.getLongitude()));
                                        Log.e("DownloadTask", "called from 690");
                                        DownloadTask downloadTask = new DownloadTask();
                                        downloadTask.execute(url, "user_destination");

                                        start_tracking();


                                    } else {
                                        buildAlertMessageNoGps();
                                    }
                                    break;

                                case 2:
                                    // 2 : HERE :get delivery boys latest location and start tracking ,
                                    // add destination (delivery location) to map,
                                    // draw path between delivery boy and destination .

                                    if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                                        option_selected = 4;

                                        destination_location.setLatitude(Double.parseDouble(current_orders_shipping_lat.get(current_position)));
                                        destination_location.setLongitude(Double.parseDouble(current_orders_shipping_long.get(current_position)));

                                        mMap.addMarker(new MarkerOptions()
                                                .position(new LatLng(destination_location.getLatitude(), destination_location.getLongitude()))
                                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.selected_pin))).setTag("destination");

                                        start_tracking();


                                    } else {

                                        buildAlertMessageNoGps();
                                    }

                                    break;

                                case 3:
                                    // 3 : ILL PICKUP : add destination(restaurant) to map,
                                    // draw path between user and destination(restaurant) .

                                    if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                                        option_selected = 4;

                                        destination_location.setLatitude(Double.parseDouble(current_orders_shipping_lat.get(current_position)));
                                        destination_location.setLongitude(Double.parseDouble(current_orders_shipping_long.get(current_position)));

                                        String url = getDirectionsUrl(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), new LatLng(destination_location.getLatitude(), destination_location.getLongitude()));
                                        Log.e("DownloadTask", "called from 739");
                                        DownloadTask downloadTask = new DownloadTask();
                                        downloadTask.execute(url, "user_destination");

                                    } else {
                                        buildAlertMessageNoGps();
                                    }

                                    break;

                                case 4:
                                    // 2 : HERE :get delivery boys latest location and start tracking ,
                                    // add destination (delivery location) to map,
                                    // draw path between delivery boy and destination .

                                    if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                                        option_selected = 4;

                                        destination_location.setLatitude(Double.parseDouble(current_orders_shipping_lat.get(current_position)));
                                        destination_location.setLongitude(Double.parseDouble(current_orders_shipping_long.get(current_position)));

                                        mMap.addMarker(new MarkerOptions()
                                                .position(new LatLng(destination_location.getLatitude(), destination_location.getLongitude()))
                                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.selected_pin))).setTag("destination");


                                        start_tracking();

                                    } else {
                                        buildAlertMessageNoGps();
                                    }

                                    break;

                                default:
                                    break;
                            }


                        } else {
//                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_LONG);
                            Toast toast = Toast.makeText(getActivity(), " No Internet Connection ", Toast.LENGTH_LONG);
                            toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                            toast.show();
                            build_no_internet_msg();
                        }

                    }


                } else {

                    String msg = "";

                    if (current_orders_Status.get(position).equals("Rejected") || current_orders_Status.get(position).equals("order cancelled")) {
                        msg = "Your order has been cancelled.";

                    } else {

                        msg = "Your order is not yet dispatched.";
                    }

                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(msg).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            dialog.cancel();
                        }
                    });
                    final AlertDialog alert = builder.create();
                    alert.show();
                    alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));


                }

            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Log.e("center", mMap.getCameraPosition().target + "");

//                update_delivery_boy_location();

//                update_user_location();

                ask_call_permission();


            }
        });


        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppController.getInstance().pitstopTravelTime = "0";
                flow = 4;
                ask_location_permission();


            }


        });

        user_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user_msg.getText().toString().equals("Please enable GPS")) {
                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    user_msg.setVisibility(View.GONE);
                }
            }
        });


        action_hint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivityForResult(new Intent(getActivity(), PlacesAutoCompleteActivity.class), PLACE_AUTOCOMPLETE_REQUEST_CODE);

//
////                startActivityForResult(new Intent(getActivity(), SearchMap.class), 9);
//
//                Log.e("TYPE_FILTER_CITIES", "TYPE_FILTER_CITIES");
//
//                if (searchable) {
//                    loading.show();
//                    try {
////                        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
////                                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
////                                .build();
//
//                        Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
////                                .setFilter(typeFilter)
//                                .build(getActivity());
//
//                        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
//                    } catch (GooglePlayServicesRepairableException e) {
//                        // TODO: Handle the error.
//                        Log.e(" RepairableException", e.toString());
//                    } catch (GooglePlayServicesNotAvailableException e) {
//                        // TODO: Handle the error.
//                        Log.e(" NotAvailableException", e.toString());
//                    }
//                } else {
//                    Toast toast = Toast.makeText(getActivity(), " Destination has been selected ", Toast.LENGTH_SHORT);
//                    toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
//                    toast.show();
//                }

            }
        });

        address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivityForResult(new Intent(getActivity(), PlacesAutoCompleteActivity.class), PLACE_AUTOCOMPLETE_REQUEST_CODE);

//                Log.e("TYPE_FILTER_CITIES", "TYPE_FILTER_CITIES");
//
//                if (searchable) {
//                    loading.show();
//                    try {
//
////                        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
////                                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
////                                .build();
//
//                        Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
////                                .setFilter(typeFilter)
//                                .build(getActivity());
//
//                        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
//                    } catch (GooglePlayServicesRepairableException e) {
//                        // TODO: Handle the error.
//                        Log.e(" RepairableException", e.toString());
//                    } catch (GooglePlayServicesNotAvailableException e) {
//                        // TODO: Handle the error.
//                        Log.e(" NotAvailableException", e.toString());
//                    }
//                } else {
//                    Toast toast = Toast.makeText(getActivity(), " Destination has been selected ", Toast.LENGTH_SHORT);
//                    toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
//                    toast.show();
//                }


//                startActivityForResult(new Intent(getActivity(), SearchMap.class), 9);
            }
        });


        manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }

        locationListener = new com.google.android.gms.location.LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                mLastLocation = location;

                if (mLastLocation != null) {


                    if (first) {
                        if (last_long.equals("empty")) {
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Float.parseFloat(mLastLocation.getLatitude() + ""), Float.parseFloat(mLastLocation.getLongitude() + "")), ((mMap.getMaxZoomLevel() + mMap.getMinZoomLevel()) / 2) + ((mMap.getMaxZoomLevel() + mMap.getMinZoomLevel()) / 4)));

                        } else {
                            Log.e("mLastLocation", "getLatitude" + mLastLocation.getLatitude());
                            Log.e("mLastLocation", "getLongitude" + mLastLocation.getLongitude());

                            try {
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Float.parseFloat(mLastLocation.getLatitude() + ""), Float.parseFloat(mLastLocation.getLongitude() + "")), Float.parseFloat(zoom_value)));
                            } catch (Exception e) {
                                Log.e("Exception", e.toString());
                            }
                            Log.e("new loc", "moved to");
                        }

                        first = false;
                    }


                    if (show_current_location) {
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Float.parseFloat(mLastLocation.getLatitude() + ""), Float.parseFloat(mLastLocation.getLongitude() + "")), ((mMap.getMaxZoomLevel() + mMap.getMinZoomLevel()) / 2) + ((mMap.getMaxZoomLevel() + mMap.getMinZoomLevel()) / 4)));

                        show_current_location = false;

                    }


                }

                displayLocation();

                AppController.getInstance().sharedPreferences_editor.putString("last_lat", mLastLocation.getLatitude() + "");
                AppController.getInstance().sharedPreferences_editor.putString("last_long", mLastLocation.getLongitude() + "");
                AppController.getInstance().sharedPreferences_editor.commit();
            }

        };

        if (checkPlayServices()) {
            // Building the GoogleApi client
            buildGoogleApiClient();

            createLocationRequest();
        }


        return fragment_view;
    }

    private void go_clicked() {


        searchable = false;

        loading.show();

        if (option_selected == 0) {
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (ConnectivityReceiver.isConnected()) {

                    if (mLastLocation != null) {
                        String url = getDirectionsUrl(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), new LatLng(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude));
                        Log.e("DownloadTask", "called from 833");
                        DownloadTask downloadTask = new DownloadTask();
                        downloadTask.execute(url, "in_car");

                    } else {
                        Toast toast = Toast.makeText(getActivity(), " Waiting for location..", Toast.LENGTH_LONG);
                        toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                        toast.show();
                    }


                } else {
//                            Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_LONG);
                    Toast toast = Toast.makeText(getActivity(), " No Internet Connection ", Toast.LENGTH_LONG);
                    toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                    toast.show();
                    build_no_internet_msg();
                }

            } else {
                buildAlertMessageNoGps();
            }
        } else if (option_selected == 1) {

            if (com.google.maps.android.PolyUtil.containsLocation(new LatLng(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude), here_latlong, false)) {

                action_hint.setText("Selected Delivery Location");
                go.setVisibility(View.INVISIBLE);

                View customMarkerView;
                TextView time;
                Bitmap returnedBitmap;
                Canvas canvas;
                Drawable drawable;

                customMarkerView = layoutInflater.inflate(R.layout.custom_marker, null);
                time = (TextView) customMarkerView.findViewById(R.id.time);
                time.setText("Selected Delivery Location");

                customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
                customMarkerView.buildDrawingCache();
                returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                        Bitmap.Config.ARGB_8888);
                canvas = new Canvas(returnedBitmap);
                canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
                drawable = customMarkerView.getBackground();
                if (drawable != null)
                    drawable.draw(canvas);
                customMarkerView.draw(canvas);

                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude))
                        .icon(BitmapDescriptorFactory.fromBitmap(returnedBitmap))).setTag("selected_pin");

                delivery_location.setLatitude(mMap.getCameraPosition().target.latitude);
                delivery_location.setLongitude(mMap.getCameraPosition().target.longitude);


//                    address_bar.setVisibility(View.GONE);

                pin.setVisibility(View.GONE);
                locked = true;


                Seq.clear();
                Names.clear();
                Times.clear();
                Photo_Url.clear();
                rest_lat.clear();
                rest__long.clear();


                restaurantAdapter.notifyDataSetChanged();
                Log.e("popup","B");

                restaurants.show();
                list_popup_progressBar.setVisibility(View.VISIBLE);
                if (!ConnectivityReceiver.isConnected()) {
//                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_LONG);
                    Toast toast = Toast.makeText(getActivity(), " No Internet Connection ", Toast.LENGTH_LONG);
                    toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                    toast.show();
                    build_no_internet_msg();
                } else {
                    loading.dismiss();

                    if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        pitstop_title.setText("Select a Store");
                        get_restaurants_within_3km();
                    } else {
                        buildAlertMessageNoGps();
                    }

                }
            } else {
                loading.dismiss();
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Please select a location inside the red boundary").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.dismiss();


                    }
                });
                final AlertDialog alert = builder.create();
                alert.show();
                alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            }


        }
    }

    private void ask_call_permission() {

        Dexter.withActivity(getActivity())
                .withPermissions(
                        Manifest.permission.CALL_PHONE
                ).withListener(new MultiplePermissionsListener() {

            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {

                if (report.areAllPermissionsGranted()) {


                    Log.e("current_ph", current_ph + "");

                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + current_ph));
                    startActivity(callIntent);

                } else if (report.isAnyPermissionPermanentlyDenied()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Permission");
                    builder.setMessage("Eatsapp needs PHONE CALL permission to place the call");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();

                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, 1);
                            Toast.makeText(getActivity().getBaseContext(), "Go to Permissions to Grant PHONE", Toast.LENGTH_LONG).show();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();

                } else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Permission");
                    builder.setMessage("Eatsapp needs PHONE CALL permission to place the call");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            ask_call_permission();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();

                }


            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                token.continuePermissionRequest();

            }


        }).check();
    }


    private void ask_location_permission() {

        Dexter.withActivity(getActivity())
                .withPermissions(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                ).withListener(new MultiplePermissionsListener() {

            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {

                if (report.areAllPermissionsGranted()) {

                    Log.e("all_permission", "areAllPermissionsGranted");
                    Log.e("all_permission", "refresh" + refresh);

                    if (refresh) {
                        ((Navigation) getActivity()).open_home_screen();
                    }

                    if (flow == 1) {

                        my_location.setImageResource(R.drawable.ic_my_location_black_48dp);

                        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                            if (mLastLocation != null) {
                                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Float.parseFloat(mLastLocation.getLatitude() + ""), Float.parseFloat(mLastLocation.getLongitude() + "")), ((mMap.getMaxZoomLevel() + mMap.getMinZoomLevel()) / 2) + ((mMap.getMaxZoomLevel() + mMap.getMinZoomLevel()) / 4)));

                                show_current_location = false;
                            } else {

                                Toast toast = Toast.makeText(getActivity(), " Fetching Location... ", Toast.LENGTH_LONG);
                                toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                                toast.show();

                                show_current_location = true;
                            }

                        } else {
                            buildAlertMessageNoGps();
                        }

                    } else if (flow == 2) {
                        loading.show();

                        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                            option_selected = 0;
                            marker_tag = "";
                            in_car_img.setImageResource(R.drawable.ic_drive_eta_black_36dp_red);
//                            in_car_bg.setBackgroundResource(R.color.bg);
                            in_car_text.setTypeface(null, Typeface.BOLD);
                            in_car_text.setTextColor(getResources().getColor(R.color.colorPrimary));
                            action_hint.setText("Enter your trip destination...");
                            locked = false;
                            if (!ConnectivityReceiver.isConnected()) {
//                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_LONG);
                                Toast toast = Toast.makeText(getActivity(), " No Internet Connection ", Toast.LENGTH_LONG);
                                toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                                toast.show();

                                build_no_internet_msg();
                            } else {
                                get_address();
                            }

                        } else {
                            buildAlertMessageNoGps();
                        }

                        loading.dismiss();

                    } else if (flow == 3) {
                        loading.show();
                        option_selected = 2;
                        pickup_img.setImageResource(R.drawable.ic_shopping_basket_black_36dp_red);

                        pickup_bg.setBackgroundResource(R.color.bg);
                        pickup_text.setTypeface(null, Typeface.BOLD);
                        pickup_text.setTextColor(getResources().getColor(R.color.colorPrimary));
                        address_bar.setVisibility(View.GONE);
                        pin.setVisibility(View.GONE);

                        mMap.clear();

                        address_bar.setVisibility(View.GONE);
                        pin.setVisibility(View.GONE);

                        Seq.clear();
                        Names.clear();
                        Times.clear();
                        Photo_Url.clear();
                        rest_lat.clear();
                        rest__long.clear();

                        restaurantAdapter.notifyDataSetChanged();


                        if (!ConnectivityReceiver.isConnected()) {

//                    Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_LONG);
                            Toast toast = Toast.makeText(getActivity(), " No Internet Connection ", Toast.LENGTH_LONG);
                            toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                            toast.show();

                            build_no_internet_msg();

                        } else {

                            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                                search_restaurant_popup.show();

                            } else {
                                loading.dismiss();
                                buildAlertMessageNoGps();
                            }

                        }
                    } else if (flow == 4) {
                        go_clicked();

                    } else if (flow == 5) {

                        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                            loading.show();

                            if (!ConnectivityReceiver.isConnected()) {
                                Toast toast = Toast.makeText(getActivity(), " No Internet Connection ", Toast.LENGTH_LONG);
                                toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                                toast.show();

                                build_no_internet_msg();
                            } else {
                                get_current_city();
                            }

                            action_hint.setText("Select Delivery Location");

                        } else {
                            loading.dismiss();
                            buildAlertMessageNoGps();
                        }

                    }

                } else if (report.isAnyPermissionPermanentlyDenied()) {
                    refresh = true;

                    Log.e("all_permission", "isAnyPermissionPermanentlyDenied");
                    Log.e("all_permission", "refresh" + refresh);

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Permission");
                    builder.setMessage("Eatsapp needs LOCATION permissions to place/track you orders.Please grant it to continue using the app");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();

                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, 1);
                            Toast.makeText(getActivity().getBaseContext(), "Go to Permissions to Grant LOCATION", Toast.LENGTH_LONG).show();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
//                            getActivity().finish();
                        }
                    });
                    builder.show();

                } else {
                    refresh = true;

                    Log.e("all_permission", "isAnyPermissionPermanentlyDenied else");
                    Log.e("all_permission", "refresh" + refresh);

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Permission");
                    builder.setMessage("Eatsapp needs LOCATION permissions to place/track you orders.Please grant it to continue using the app");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            ask_location_permission();

                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
//                            getActivity().finish();
                        }
                    });
                    builder.show();

                }

            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                refresh = true;

                Log.e("all_permission", "onPermissionRationaleShouldBeShown");
                Log.e("all_permission", "refresh" + refresh);

                token.continuePermissionRequest();

            }


        }).check();
    }


    private void get_here_coordinates_for_selected_id(String here_selected_id) {

        loading.show();

        Log.e("selectedCoordinates_url", "" + getString(R.string.base_url) + getString(R.string.Get_coordinates) + "/" + here_selected_id);


        get_here_selected_coordinates = new StringRequest(Request.Method.GET, getString(R.string.base_url) + getString(R.string.Get_coordinates) + "/" + here_selected_id,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        ArrayList<String> lats = new ArrayList<String>();
                        ArrayList<String> longs = new ArrayList<String>();

                        loading.dismiss();

                        try {

                            JSONArray jsonArray = new JSONArray(response);
                            response = jsonArray.getJSONObject(0).get("coordinates").toString();
                            response = response.replace(" ", ",");
                            String[] arr = response.split(",");

                            for (int i = 0; i < arr.length; i++) {
                                if (i % 3 == 0) {
                                    longs.add(arr[i]);

                                } else if (i % 3 == 1) {
                                    lats.add(arr[i]);
                                }
                            }

                            here_latlong.clear();


                            for (int i = 0; i < lats.size(); i++) {
                                here_latlong.add(new LatLng(Double.parseDouble(lats.get(i)), Double.parseDouble(longs.get(i))));
                            }

                            Log.e("here_latlong", "" + here_latlong.toString());


                            draw_shape();


                        } catch (Exception e) {

                            Log.e("Exception", "" + e.toString());
                            loading.dismiss();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                loading.dismiss();

                if (isVisible()) {

                    String error_msg = "";

                    Log.e("error", "" + volleyError.toString());


                    if (volleyError instanceof NoConnectionError) {
                        error_msg = "No Internet Connection";
                        build_no_internet_msg();

                    } else if (volleyError instanceof TimeoutError) {
                        error_msg = "Server is taking too long to respond";

                    } else if (volleyError instanceof AuthFailureError) {
                        error_msg = "Error Occured, Please try later";

                    } else if (volleyError instanceof ServerError) {
                        error_msg = "Server Error, Please try later";

                    } else if (volleyError instanceof NetworkError) {
                        error_msg = "Network Error, Please try later";

                    } else if (volleyError instanceof ParseError) {
                        error_msg = "Error Occured, Please try later";
                    }
                    restaurants.dismiss();
                    if (option_selected == 2) {
                        loading.dismiss();
                    }

                    Toast toast = Toast.makeText(getActivity(), " " + error_msg + " ", Toast.LENGTH_SHORT);
                    toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                    toast.show();
                }

            }
        });

        AppController.getInstance().getRequestQueue().add(get_here_selected_coordinates);

        Log.e("getselctdcordinates", "sent");

    }

    private void draw_shape() {

        loading.show();

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        for (int i = 0; i < here_latlong.size(); i++) {
            builder.include(here_latlong.get(i));

        }

        LatLngBounds bounds = builder.build();

        mMap.addPolygon(new PolygonOptions()
                .addAll(here_latlong)
                .strokeWidth(5)
                .strokeColor(Color.RED)
                .fillColor(Color.TRANSPARENT));

        loading.dismiss();

        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 0));
    }

    private void get_pitstop() {

        Log.e("inside", "get_pitstop");
        final String url = getString(R.string.base_url) + getString(R.string.pitstopsuse);

        loading.show();

        Log.e("get_pitstop_url", "" + getString(R.string.base_url) + getString(R.string.pitstopsuse));

        get_pitstop = new StringRequest(Request.Method.POST, getString(R.string.base_url) + getString(R.string.pitstopsuse),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (isVisible()) {

                            loading.dismiss();

                            Log.e("response", "" + response);
//                            Log.e("polyline", "" + polyline.toString());


                            try {
                                JSONArray jsonArray = new JSONArray(response);

                                int filtered = 0;
                                CircleOptions circleOptions = new CircleOptions();

                                // Specifying the center of the circle


                                // Radius of the circle
                                circleOptions.radius(2000);

                                // Border color of the circle
                                circleOptions.strokeColor(Color.BLACK);

                                // Fill color of the circle
                                circleOptions.fillColor(0x30ff0000);

                                // Border width of the circle
                                circleOptions.strokeWidth(1);

                                // Adding the circle to the GoogleMap

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    if (polyline.size() != 0 && com.google.maps.android.PolyUtil.isLocationOnPath(new LatLng(Double.parseDouble(jsonArray.getJSONObject(i).getString("latitude")), Double.parseDouble(jsonArray.getJSONObject(i).getString("langitude"))), polyline, false, 4)) {
                                        Log.e("pitstop on path", "" + jsonArray.getJSONObject(i).getString("pitstop_id"));

                                        Location pitstop_location = new Location("");
                                        pitstop_location.setLatitude(Double.parseDouble(jsonArray.getJSONObject(i).getString("latitude")));
                                        pitstop_location.setLongitude(Double.parseDouble(jsonArray.getJSONObject(i).getString("langitude")));

                                        if (mLastLocation.distanceTo(pitstop_location) > 2000) {

                                            filtered++;
                                            mMap.addMarker(new MarkerOptions()
                                                    .position(new LatLng(Double.parseDouble(jsonArray.getJSONObject(i).getString("latitude")), Double.parseDouble(jsonArray.getJSONObject(i).getString("langitude"))))
                                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.pitstop_new))
                                            ).setTag("Pistiop " + jsonArray.getJSONObject(i).getString("pitstop_id") + "_" + jsonArray.getJSONObject(i).getString("pitstop_name"));

                                            try
                                            {
                                                collectionPoints.put(jsonArray.getJSONObject(i).getString("pitstop_id"),new LatLng(Double.parseDouble(jsonArray.getJSONObject(i).getString("collection_point_latitude")),Double.parseDouble(jsonArray.getJSONObject(i).getString("collection_point_langitude"))));

                                            }catch (Exception e)
                                            {
                                                Log.e("Exception",e.toString());
                                            }

                                        }else
                                        {
                                            Log.e("failed Dist",i+" "+mLastLocation.distanceTo(pitstop_location)+" "+jsonArray.getJSONObject(i).getString("pitstop_name"));
                                        }

                                    }else
                                    {
                                        Log.e("failed Path",i+" "+jsonArray.getJSONObject(i).getString("pitstop_name"));
                                    }

                                }

//                                AppController.getInstance().show_popup_alert("Total : "+jsonArray.length()+" \nFiltered "+filtered,getActivity());

                                circleOptions.center(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
//                                mMap.addCircle(circleOptions);

                                Log.e("approved", "" + filtered);

                                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));

                                if (filtered == 0) {
                                    AppController.getInstance().show_popup_alert_without_title("We have carefully selected the delivery stops to serve you better.\n\nSorry, there are no active delivery stops on your route.", getActivity());
                                }


                            } catch (JSONException e) {

                                Log.e("e", "" + e.toString());

                                StackTraceElement[] stackTrace = e.getStackTrace();
                                String msg = e.toString();
                                for (int i = 0; i < stackTrace.length; i++) {

                                    msg = msg+"\n"+stackTrace[i];
                                }

                                AppController.getInstance().show_popup_alert(msg,getActivity());
                            }

                            loading.dismiss();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (isVisible()) {


                    String error_msg = "";

                    Log.e("error", "" + volleyError.toString());

                    loading.dismiss();

                    if (volleyError instanceof NoConnectionError) {
                        error_msg = "No Internet Connection";
                        build_no_internet_msg();

                    } else if (volleyError instanceof TimeoutError) {
                        error_msg = "Server is taking too long to respond";

                    } else if (volleyError instanceof AuthFailureError) {
                        error_msg = "Error Occured, Please try later";

                    } else if (volleyError instanceof ServerError) {
                        error_msg = "Server Error, Please try later";

                    } else if (volleyError instanceof NetworkError) {
                        error_msg = "Network Error, Please try later";

                    } else if (volleyError instanceof ParseError) {
                        error_msg = "Error Occured, Please try later";
                    }

                    Snackbar snackbar = Snackbar
                            .make(parent_layout, error_msg, Snackbar.LENGTH_LONG);
                    snackbar.show();
                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // TODO Auto-generated method stub

                final Map<String, String> params = new HashMap<String, String>();


//                params.put("southwest_lat", "-73.71036357402416");
//                params.put("southwest_lng", "-67.09595639258623");
//                params.put("northeast_lat", "-33.91036357402416");
//                params.put("northeast_lng", "-57.09595639258623");

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        LatLng sw;
                        LatLng ne;

                        sw = mMap.getProjection().getVisibleRegion().latLngBounds.southwest;
                        ne = mMap.getProjection().getVisibleRegion().latLngBounds.northeast;

                        params.put("southwest_lat",sw.latitude+"");
                        params.put("southwest_lng", sw.longitude+"");
                        params.put("nortsearch forheast_lat", ne.latitude+"");
                        params.put("northeast_lng", ne.longitude+"");

                        Log.e("params", params.toString());
                    }
                });



                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        AppController.getInstance().getRequestQueue().add(get_pitstop);
    }

    public void build_no_internet_msg() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Your Internet seems to be turned OFF, do you want to turn it ON?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);


            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();

            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));

    }

    private void search_restaurant_for_pickup() {

        Log.e("url", "search_restaurant_for_pickup: " + getString(R.string.base_url) + getString(R.string.search_rest));

        search_restaurant_for_pickup = new StringRequest(Request.Method.POST, getString(R.string.base_url) + getString(R.string.search_rest),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (isVisible()) {

                            View customMarkerView;
                            TextView time;
                            Bitmap returnedBitmap;
                            Canvas canvas;
                            Drawable drawable;
                            LatLngBounds.Builder builder = new LatLngBounds.Builder();


                            Log.e("search_Rest_pickup", "" + response);

                            try {
                                JSONArray jsonArray = new JSONArray(response);


                                if (jsonArray.length() == 0) {
                                    restaurants.dismiss();

                                    showAlert_forSearch();

                                }

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    Seq.add(jsonArray.getJSONObject(i).getString("restaurant_id"));
                                    Names.add(jsonArray.getJSONObject(i).getString("restaurant_name"));
                                    rest_lat.add(jsonArray.getJSONObject(i).getString("restaurant_latitude"));
                                    rest__long.add(jsonArray.getJSONObject(i).getString("restaurant_langitude"));
                                    Times.add("na");
                                    Photo_Url.add(jsonArray.getJSONObject(i).getString("image"));

                                    if (option_selected == 2) {

                                        customMarkerView = layoutInflater.inflate(R.layout.custom_restaurant, null);
                                        time = (TextView) customMarkerView.findViewById(R.id.time);
                                        time.setText(Names.get(i));

                                        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                                        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
                                        customMarkerView.buildDrawingCache();
                                        returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                                                Bitmap.Config.ARGB_8888);
                                        canvas = new Canvas(returnedBitmap);
                                        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
                                        drawable = customMarkerView.getBackground();
                                        if (drawable != null)
                                            drawable.draw(canvas);
                                        customMarkerView.draw(canvas);

                                        mMap.addMarker(new MarkerOptions()
                                                .position(new LatLng(Double.parseDouble(rest_lat.get(i)), Double.parseDouble(rest__long.get(i))))
                                                .icon(BitmapDescriptorFactory.fromBitmap(returnedBitmap))).setTag("restaurant_pin " + Seq.get(i));

                                        builder.include(new LatLng(Double.parseDouble(rest_lat.get(i)), Double.parseDouble(rest__long.get(i))));

                                    }
                                }


                            } catch (JSONException e) {
                                restaurants.dismiss();
                                Log.e("Exception", e.toString());

                                showAlert_forSearch();

                            }

                            list_popup_progressBar.setVisibility(View.GONE);
                            restaurantAdapter.notifyDataSetChanged();

                            if (option_selected == 2) {

                                try {
                                    LatLngBounds bounds = builder.build();
                                    Log.e("bounds", bounds.toString());
                                    mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 200));
                                } catch (Exception e) {
                                    Log.e("Exception", e.toString());

                                }
                                loading.dismiss();
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                if (isVisible()) {

                    String error_msg = "";

                    Log.e("error", "" + volleyError.toString());


                    if (volleyError instanceof NoConnectionError) {
                        error_msg = "No Internet Connection";
                        build_no_internet_msg();

                    } else if (volleyError instanceof TimeoutError) {
                        error_msg = "Server is taking too long to respond";

                    } else if (volleyError instanceof AuthFailureError) {
                        error_msg = "Error Occured, Please try later";

                    } else if (volleyError instanceof ServerError) {
                        error_msg = "Server Error, Please try later";

                    } else if (volleyError instanceof NetworkError) {
                        error_msg = "Network Error, Please try later";

                    } else if (volleyError instanceof ParseError) {
                        error_msg = "Error Occured, Please try later";
                    }
                    restaurants.dismiss();
                    if (option_selected == 2) {
                        loading.dismiss();
                    }

//                    Toast.makeText(getActivity(), error_msg, Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(getActivity(), " " + error_msg + " ", Toast.LENGTH_SHORT);
                    toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                    toast.show();
                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // TODO Auto-generated method stub

                Map<String, String> params = new HashMap<String, String>();

                params.put("name", input_restaurant_name.trim());
                params.put("area", input_locality.trim());

                params.put("latitude", "" + mLastLocation.getLatitude());
                params.put("langitude", "" + mLastLocation.getLongitude());

//                params.put("latitude", "" + "18.9255979");
//                params.put("langitude", "" + "72.8227448");

                Log.e("params", params.toString());


                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        AppController.getInstance().getRequestQueue().add(search_restaurant_for_pickup);

        Log.e("searchrestpickup", "sent");

    }

    private void showAlert_forSearch() {

        String pop_msg;

        if (input_restaurant_name.equals("")) {
            pop_msg = "Sorry, we do not have any Outlet to serve you in this area...\n\nWe are working on adding new Outlet everyday...";

        } else {
//            pop_msg="Sorry, we don't serve food from their restaurant as of now. Please search the location where you want food to be delivered to see the restaurants whose food we serve in that area.";
            pop_msg = "Sorry, we don't serve from this Outlet as of now.\n\nPlease search the location from where you would like to pickup.";
        }

//        java.util.Date date = new java.util.Date();

//        pop_msg = " Res: -"+input_restaurant_name.trim() + "-\n" +
//                " Loc: -"+input_locality.trim() + "-\n" +
//                mLastLocation.getLatitude() + "\n" +
//                mLastLocation.getLongitude() + "\n" +
//                date.toString();

        AppController.getInstance().show_popup_alert_without_title(pop_msg, getActivity());

    }

    private void get_restaurants_within_3km() {

        Log.e("get_restaurants_url", "" + getString(R.string.base_url) + getString(R.string.res3km));


        get_restaurants_within_3km = new StringRequest(Request.Method.POST, getString(R.string.base_url) + getString(R.string.res3km),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (isVisible()) {


                            View customMarkerView;
                            TextView time;
                            Bitmap returnedBitmap;
                            Canvas canvas;
                            Drawable drawable;
                            LatLngBounds.Builder builder = new LatLngBounds.Builder();


                            Log.e("response for 3km", "" + response);

                            try {
                                JSONArray jsonArray = new JSONArray(response);


                                if (jsonArray.length() == 0) {
                                    restaurants.dismiss();

                                    showAlert(" ");

                                }

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    Seq.add(jsonArray.getJSONObject(i).getString("restaurant_id"));
                                    Names.add(jsonArray.getJSONObject(i).getString("restaurant_name"));
                                    rest_lat.add(jsonArray.getJSONObject(i).getString("restaurant_latitude"));
                                    rest__long.add(jsonArray.getJSONObject(i).getString("restaurant_langitude"));
                                    Times.add("na");
                                    Photo_Url.add(jsonArray.getJSONObject(i).getString("image"));

                                    if (option_selected == 2) {

                                        customMarkerView = layoutInflater.inflate(R.layout.custom_restaurant, null);
                                        time = (TextView) customMarkerView.findViewById(R.id.time);
                                        time.setText(Names.get(i));

                                        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                                        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
                                        customMarkerView.buildDrawingCache();
                                        returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                                                Bitmap.Config.ARGB_8888);
                                        canvas = new Canvas(returnedBitmap);
                                        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
                                        drawable = customMarkerView.getBackground();
                                        if (drawable != null)
                                            drawable.draw(canvas);
                                        customMarkerView.draw(canvas);

                                        mMap.addMarker(new MarkerOptions()
                                                .position(new LatLng(Double.parseDouble(rest_lat.get(i)), Double.parseDouble(rest__long.get(i))))
                                                .icon(BitmapDescriptorFactory.fromBitmap(returnedBitmap))).setTag("restaurant_pin " + Seq.get(i));

                                        builder.include(new LatLng(Double.parseDouble(rest_lat.get(i)), Double.parseDouble(rest__long.get(i))));

                                    }
                                }


                            } catch (JSONException e) {
                                restaurants.dismiss();
                                Log.e("Exception", e.toString());

//                                showAlert("No restaurant were found");
                                showAlert(" ");


                            }

                            list_popup_progressBar.setVisibility(View.GONE);
                            restaurantAdapter.notifyDataSetChanged();

                            if (option_selected == 2) {

                                try {
                                    LatLngBounds bounds = builder.build();
                                    Log.e("bounds", bounds.toString());
                                    mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 200));
                                } catch (Exception e) {
                                    Log.e("Exception", e.toString());

                                }
                                loading.dismiss();
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                if (isVisible()) {


                    String error_msg = "";

                    Log.e("error", "" + volleyError.toString());


                    if (volleyError instanceof NoConnectionError) {
                        error_msg = "No Internet Connection";
                        build_no_internet_msg();

                    } else if (volleyError instanceof TimeoutError) {
                        error_msg = "Server is taking too long to respond";

                    } else if (volleyError instanceof AuthFailureError) {
                        error_msg = "Error Occured, Please try later";

                    } else if (volleyError instanceof ServerError) {
                        error_msg = "Server Error, Please try later";

                    } else if (volleyError instanceof NetworkError) {
                        error_msg = "Network Error, Please try later";

                    } else if (volleyError instanceof ParseError) {
                        error_msg = "Error Occured, Please try later";
                    }
                    restaurants.dismiss();
                    if (option_selected == 2) {
                        loading.dismiss();
                    }

//                    Toast.makeText(getActivity(), error_msg, Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(getActivity(), " " + error_msg + " ", Toast.LENGTH_SHORT);
                    toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                    toast.show();
                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // TODO Auto-generated method stub

                Map<String, String> params = new HashMap<String, String>();

                if (option_selected == 2) {
                    params.put("latitude", "" + mLastLocation.getLatitude());
                    params.put("langitude", "" + mLastLocation.getLongitude());
                } else {
                    params.put("latitude", "" + delivery_location.getLatitude());
                    params.put("langitude", "" + delivery_location.getLongitude());
                }

                Log.e("params", params.toString());

                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        AppController.getInstance().getRequestQueue().add(get_restaurants_within_3km);

        Log.e("restaurants_within_3km", "" + get_restaurants_within_3km.toString());

    }


    private void get_delivery_boy_location() {

        String url;

        url = getString(R.string.base_url) + getString(R.string.deliveryboylocation) + "?deliveryboy_id=" + current_orders_Delivered_by.get(current_position) + "&order_id=" + current_orders_Seq.get(current_position);

        if (!drawn) {

            loading.show();
        }


        Log.e("url", url);


        get_delivery_boy_location = new StringRequest(Request.Method.GET, url,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (isVisible()) {

                            Log.e("response", response);


//                        String response ="{ \"latitude\": \"12.95861944164913\" , \"langitude\":\"77.51813147217035\"}";

                            try {

                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.has("status") && !(Boolean) jsonObject.get("status")) {
                                    loading.dismiss();
                                    Toast toast = Toast.makeText(getActivity(), " Location not available ", Toast.LENGTH_LONG);
                                    toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                                    toast.show();


                                } else {

                                    delivery_boy_location.setLatitude(Double.parseDouble(jsonObject.getString("latitude")));
                                    delivery_boy_location.setLongitude(Double.parseDouble(jsonObject.getString("langitude")));

                                    if (ConnectivityReceiver.isConnected()) {
                                        // draw path between delivery boy and destination ,
                                        if (!drawn) {
                                            String url = getDirectionsUrl(new LatLng(delivery_boy_location.getLatitude(), delivery_boy_location.getLongitude()), new LatLng(destination_location.getLatitude(), destination_location.getLongitude()));
                                            Log.e("DownloadTask", "called from 1757");
                                            DownloadTask downloadTask = new DownloadTask();
                                            downloadTask.execute(url, "delivery_boy_destination");
                                            Log.e("t", "1");
                                        } else {

                                            update_delivery_boy_location();

                                        }

                                    } else {
//                                    Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_LONG);
                                        loading.dismiss();
                                        Toast toast = Toast.makeText(getActivity(), " No Internet Connection ", Toast.LENGTH_LONG);
                                        toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                                        toast.show();
                                        build_no_internet_msg();
                                    }

                                }

                            } catch (Exception e) {
                                Log.e("getdeliveryboylocation", "" + e.toString());
                            }
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                if (isVisible()) {


                    String error_msg = "";

                    Log.e("error", "" + volleyError.toString());

                    loading.dismiss();

                    if (volleyError instanceof NoConnectionError) {
                        error_msg = "No Internet Connection";
                        build_no_internet_msg();

                    } else if (volleyError instanceof TimeoutError) {
                        error_msg = "Server is taking too long to respond";

                    } else if (volleyError instanceof AuthFailureError) {
                        error_msg = "Error Occured, Please try later";

                    } else if (volleyError instanceof ServerError) {
                        error_msg = "Server Error, Please try later";

                    } else if (volleyError instanceof NetworkError) {
                        error_msg = "Network Error, Please try later";

                    } else if (volleyError instanceof ParseError) {
                        error_msg = "Error Occured, Please try later";
                    }

//                    Toast.makeText(getActivity(), error_msg, Toast.LENGTH_LONG);
                    Toast toast = Toast.makeText(getActivity(), " " + error_msg + " ", Toast.LENGTH_LONG);
                    toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                    toast.show();
                }

            }
        });

        if(tracking)
        {
            AppController.getInstance().getRequestQueue().add(get_delivery_boy_location);
        }
    }

    private void update_delivery_boy_location() {

        if (isVisible()) {


            float new_distance;
            float old_time;
            Location temp_delivery_boy_location = new Location("temp_delivery_boy_location");

            //TODO replace map target with the newly fetched location
            temp_delivery_boy_location.setLatitude(delivery_boy_location.getLatitude());
            temp_delivery_boy_location.setLongitude(delivery_boy_location.getLongitude());

            delivery_boy_marker.remove();

            new_distance = ((int) temp_delivery_boy_location.distanceTo(destination_location));

            Log.e("del b dest tim before", delivery_boy_destination_time + "");

            if (delivery_boy_destination_time.contains("h")) {

                old_time = Float.parseFloat(delivery_boy_destination_time.substring(0, delivery_boy_destination_time.indexOf(" ")));
                old_time = old_time * 60;
                old_time = old_time + Float.parseFloat(delivery_boy_destination_time.substring(delivery_boy_destination_time.indexOf("r ") + 2, delivery_boy_destination_time.indexOf("m") - 1));

            } else {

                old_time = Float.parseFloat(delivery_boy_destination_time.substring(0, delivery_boy_destination_time.indexOf(" ")));

            }

            View customMarkerView = layoutInflater.inflate(R.layout.custom_delivery, null);
            TextView time = (TextView) customMarkerView.findViewById(R.id.time);
            time.setText(((int) (new_distance * old_time / Float.parseFloat(delivery_boy_destination_distance))) + " mins");
            ((View)time.getParent()).setVisibility(View.INVISIBLE);

            customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
            customMarkerView.buildDrawingCache();
            Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                    Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(returnedBitmap);
            canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
            Drawable drawable = customMarkerView.getBackground();
            if (drawable != null)
                drawable.draw(canvas);
            customMarkerView.draw(canvas);

            delivery_boy_marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(temp_delivery_boy_location.getLatitude(), temp_delivery_boy_location.getLongitude()))
                    .icon(BitmapDescriptorFactory.fromBitmap(returnedBitmap)));


            delivery_boy_marker.setTag("delivery_boy_destination");

//        delivery_boy_destination_distance=""+delivery_boy_location.distanceTo(destination_location);
//
//        Log.e("delivery_boy_distance",delivery_boy_destination_distance+"");
//        Log.e("delivery_boy_time",delivery_boy_destination_time+"");
        }
    }


    private void update_user_location() {

        if (isVisible()) {


            float new_distance;
            float old_time;
            Location temp_user_location = new Location("temp_delivery_boy_location");

            //TODO replace map target with the user's new location
            temp_user_location.setLatitude(mMap.getCameraPosition().target.latitude);
            temp_user_location.setLongitude(mMap.getCameraPosition().target.longitude);


            destination_marker.remove();

            new_distance = ((int) temp_user_location.distanceTo(destination_location));

            Log.e("user dest tim before", user_destination_time + "");

            if (user_destination_time.contains("h")) {

                old_time = Float.parseFloat(user_destination_time.substring(0, user_destination_time.indexOf(" ")));
                old_time = old_time * 60;
                old_time = old_time + Float.parseFloat(user_destination_time.substring(user_destination_time.indexOf("r ") + 2, user_destination_time.indexOf("m") - 1));

            } else {
                old_time = Float.parseFloat(user_destination_time.substring(0, user_destination_time.indexOf(" ")));

            }

            View customMarkerView;
            TextView time;


            customMarkerView = layoutInflater.inflate(R.layout.custom_pitstop, null);
            time = (TextView) customMarkerView.findViewById(R.id.time);
            time.setText(((int) (new_distance * old_time / Float.parseFloat(user_destination_distance))) + " mins");
            if(hideTime)
            {
                ((View)time.getParent()).setVisibility(View.INVISIBLE);
            }

            if (Integer.parseInt(current_orders_Order_type.get(current_position)) == 3) {
                customMarkerView = layoutInflater.inflate(R.layout.custom_marker, null);
                time = (TextView) customMarkerView.findViewById(R.id.time);
                time.setText(current_orders_Names.get(current_position) + " \n " + ((int) (new_distance * old_time / Float.parseFloat(user_destination_distance))) + " mins");
            }


            customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
            customMarkerView.buildDrawingCache();
            Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                    Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(returnedBitmap);
            canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
            Drawable drawable = customMarkerView.getBackground();
            if (drawable != null)
                drawable.draw(canvas);
            customMarkerView.draw(canvas);

            destination_marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(destination_location.getLatitude(), destination_location.getLongitude()))
                    .icon(BitmapDescriptorFactory.fromBitmap(returnedBitmap)));


            destination_marker.setTag("user_destination");
//
//        user_destination_distance=""+mLastLocation.distanceTo(destination_location);
//
//        Log.e("delivery_boy_distance",user_destination_distance+"");
//        Log.e("delivery_boy_time",user_destination_time+"");
        }
    }

    private void start_tracking() {

        handler = new Handler();
        t = new Runnable() {
            @Override
            public void run() {
                //Do something after 20 seconds
                Log.e("start_tracking", "start_tracking");

                get_delivery_boy_location();
                handler.postDelayed(this, 5000);
            }
        };
        handler.postDelayed(t, 0);


    }

    private void get_restaurants_related_to_pitstop() {

        final String url = getString(R.string.base_url) + getString(R.string.get_restaurants_related_to_pitstop) + selected_pitstop;

        Log.e("api call", "get_restaurants_related_to_pitstop : " + getString(R.string.base_url) + getString(R.string.get_restaurants_related_to_pitstop) + selected_pitstop);

        get_restaurants_related_to_pitstop = new StringRequest(Request.Method.GET, getString(R.string.base_url) + getString(R.string.get_restaurants_related_to_pitstop) + selected_pitstop,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (isVisible()) {

                            Log.e("response", response);
                            //AppController.getInstance().show_popup_alert(url+"\n\n"+response,getActivity());

                            //response = "[{\"restaurant_id\":\"25\",\"restaurant_name\":\"Wraps and Rolls - Pali Market\",\"restaurant_address\":\"Pali address\",\"restaurant_phone\":\"4312233221\",\"restaurant_mobile\":\"4444444444\",\"restaurant_email\":\"444@34.gg\",\"image\":\"\",\"restaurant_latitude\":\"19.0549\",\"restaurant_langitude\":\"72.8359\",\"restaurant_branch\":\"Mumbai\",\"restaurant_manager\":\"35\",\"preparation_time\":\"5\",\"enabled\":\"1\",\"deactivatefrom\":null,\"deactivateto\":null,\"fromtime\":\"10:30:00\",\"totime\":\"23:59:00\",\"commission\":\"10\",\"penalty\":\"0\",\"reimb\":\"\",\"servicetax\":\"15\",\"days\":\"a:8:{i:0;s:3:\\\"all\\\";i:1;s:6:\\\"sunday\\\";i:2;s:6:\\\"monday\\\";i:3;s:7:\\\"tuesday\\\";i:4;s:9:\\\"wednesday\\\";i:5;s:8:\\\"thursday\\\";i:6;s:6:\\\"friday\\\";i:7;s:8:\\\"saturday\\\";}\",\"delivery_charge\":\"\",\"delete\":\"0\",\"distance\":\"1.912321743496144\"},{\"restaurant_id\":\"27\",\"restaurant_name\":\"Apsara Ice Cream - Matunga\",\"restaurant_address\":\"Shop 1, Radha Bhuvan, King's Circle, 471 Adenwala Road, Matunga East, Mumbai\",\"restaurant_phone\":\"022 24022233\",\"restaurant_mobile\":\"\",\"restaurant_email\":\"56@56.23\",\"image\":\"\",\"restaurant_latitude\":\"19.0259\",\"restaurant_langitude\":\"72.8553\",\"restaurant_branch\":\"Mumbai\",\"restaurant_manager\":\"37\",\"preparation_time\":\"1\",\"enabled\":\"1\",\"deactivatefrom\":\"1970-01-01\",\"deactivateto\":\"1970-01-01\",\"fromtime\":\"11:00:00\",\"totime\":\"23:30:00\",\"commission\":\"10\",\"penalty\":\"0\",\"reimb\":\"\",\"servicetax\":\"15\",\"days\":\"a:8:{i:0;s:3:\\\"all\\\";i:1;s:6:\\\"sunday\\\";i:2;s:6:\\\"monday\\\";i:3;s:7:\\\"tuesday\\\";i:4;s:9:\\\"wednesday\\\";i:5;s:8:\\\"thursday\\\";i:6;s:6:\\\"friday\\\";i:7;s:8:\\\"saturday\\\";}\",\"delivery_charge\":\"\",\"delete\":\"0\",\"distance\":\"1.125508873831086\"},{\"restaurant_id\":\"48\",\"restaurant_name\":\"Garcia's Famous Pizza\",\"restaurant_address\":\"shop no 32, Dedhiaya mansion, v.s.marg nr gammon house,, Prabhadevi, Mumbai, Maharashtra 400025\",\"restaurant_phone\":\"022 6585 2829\",\"restaurant_mobile\":\"\",\"restaurant_email\":\"kkkk@dddd.com\",\"image\":\"\",\"restaurant_latitude\":\"19.0151\",\"restaurant_langitude\":\"72.8269\",\"restaurant_branch\":\"Mumbai\",\"restaurant_manager\":\"58\",\"preparation_time\":\"5\",\"enabled\":\"1\",\"deactivatefrom\":null,\"deactivateto\":null,\"fromtime\":\"11:00:00\",\"totime\":\"23:30:00\",\"commission\":\"10\",\"penalty\":\"0\",\"reimb\":\"\",\"servicetax\":\"15\",\"days\":\"a:8:{i:0;s:3:\\\"all\\\";i:1;s:6:\\\"sunday\\\";i:2;s:6:\\\"monday\\\";i:3;s:7:\\\"tuesday\\\";i:4;s:9:\\\"wednesday\\\";i:5;s:8:\\\"thursday\\\";i:6;s:6:\\\"friday\\\";i:7;s:8:\\\"saturday\\\";}\",\"delivery_charge\":\"\",\"delete\":\"0\",\"distance\":\"1.1180581693536948\"}]";

                            if (!response.contains("restaurants  could not be found for the pitstop")) {
                                try {
                                    JSONArray jsonArray = new JSONArray(response);

                                    if (jsonArray.length() == 0) {
                                        restaurants.dismiss();

                                        showAlert("Pitstop id " + selected_pitstop);

                                    }

                                    Seq.clear();
                                    Names.clear();
                                    Times.clear();
                                    Photo_Url.clear();
                                    rest_lat.clear();
                                    rest__long.clear();

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        Seq.add(jsonArray.getJSONObject(i).getString("restaurant_id"));
                                        Names.add(jsonArray.getJSONObject(i).getString("restaurant_name"));
                                        Times.add("na");
                                        Photo_Url.add(jsonArray.getJSONObject(i).getString("image"));

                                        rest_lat.add(jsonArray.getJSONObject(i).getString("restaurant_latitude"));
                                        rest__long.add(jsonArray.getJSONObject(i).getString("restaurant_langitude"));
                                    }


                                } catch (Exception e) {

                                    Log.e("Exception", "get_restaurants_related_to_pitstop" + e.toString());
//                                    AppController.getInstance().show_popup_alert(e.toString(),getActivity());
                                }

                                list_popup_progressBar.setVisibility(View.GONE);
                                restaurantAdapter.notifyDataSetChanged();
                            } else {

                                list_popup_progressBar.setVisibility(View.GONE);
                                restaurants.dismiss();
//                                Toast.makeText(getActivity(), "No Resturants found for the Pitstop", Toast.LENGTH_LONG).show();

                                showAlert("Pitstop id " + selected_pitstop);

                            }

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                if (isVisible()) {


                    String error_msg = "";

                    list_popup_progressBar.setVisibility(View.GONE);
                    restaurants.dismiss();

                    Log.e("error", "" + volleyError.toString());

                    loading.dismiss();

                    if (volleyError instanceof NoConnectionError) {
                        error_msg = "No Internet Connection";
                        build_no_internet_msg();

                    } else if (volleyError instanceof TimeoutError) {
                        error_msg = "Server is taking too long to respond";

                    } else if (volleyError instanceof AuthFailureError) {
                        error_msg = "Error Occured, Please try later";

                    } else if (volleyError instanceof ServerError) {
                        error_msg = "Server Error, Please try later";

                    } else if (volleyError instanceof NetworkError) {
                        error_msg = "Network Error, Please try later";

                    } else if (volleyError instanceof ParseError) {
                        error_msg = "Error Occured, Please try later";
                    }

//                    Toast.makeText(getActivity(), error_msg, Toast.LENGTH_LONG).show();
                    Toast toast = Toast.makeText(getActivity(), " " + error_msg + " ", Toast.LENGTH_LONG);
                    toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                    toast.show();
                }
            }
        });

        AppController.getInstance().getRequestQueue().add(get_restaurants_related_to_pitstop);
    }

    @Override
    public void onClick(View v) {

//        Toast toast = Toast.makeText(getActivity()," Welcome ",Toast.LENGTH_LONG);
//        toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
//        toast.show();

        searchable = true;
        hideTime = false;

        if (handler != null) {
            handler.removeCallbacks(t);
        }

        address_bar.setVisibility(View.VISIBLE);
        pin.setVisibility(View.VISIBLE);
        go.setVisibility(View.VISIBLE);
        track_order_bar.setVisibility(View.GONE);
        mMap.clear();

        Log.e("view was clicked", v.getId() + "");


        reset_all_buttons();

        switch (v.getId()) {
            case R.id.in_car_bg:
                //in car
                flow = 2;
                ask_location_permission();

                break;

            case R.id.here_bg:
                //here
                flow = 5;
                ask_location_permission();

                break;

            case R.id.pickup_bg:
                //Ill pickup
                flow = 3;
                ask_location_permission();
                break;

            case R.id.home_bg:
                //Home
                loading.show();
                option_selected = 3;
                home_img.setImageResource(R.drawable.ic_home_black_36dp_red);
                home_bg.setBackgroundResource(R.color.bg);
                home_text.setTypeface(null, Typeface.BOLD);
                home_text.setTextColor(getResources().getColor(R.color.colorPrimary));
                add_address.setVisibility(View.VISIBLE);
                address_bar.setVisibility(View.GONE);
                pin.setVisibility(View.GONE);

                loading.dismiss();

                get_home_Addresses();

                break;

        }


    }

    private void show_here_locations_list() {

        Log.e("get_here_locations_url", "" + getString(R.string.base_url) + getString(R.string.here_locations_list) + "?city=" + current_city);

        loading.show();

        get_here_locations_list = new StringRequest(Request.Method.GET, getString(R.string.base_url) + getString(R.string.here_locations_list) + "?city=" + current_city,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        loading.dismiss();

                        here_id.clear();
                        here_locations.clear();
                        here_latlong.clear();

                        try {

                            JSONArray jsonArray = new JSONArray(response);

                            for (int i = 0; i < jsonArray.length(); i++) {
                                here_id.add(jsonArray.getJSONObject(i).get("id").toString());
                                here_locations.add(jsonArray.getJSONObject(i).get("name").toString());

                            }

                            here_adapter.notifyDataSetChanged();
                            here_Dialog.show();


                        } catch (Exception e) {

                            AppController.getInstance().show_popup_alert_without_title("Here location feature is not available in this region", getActivity());

                            Log.e("Exception", "" + e.toString());
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                loading.dismiss();

                if (isVisible()) {

                    String error_msg = "";

                    Log.e("error", "" + volleyError.toString());


                    if (volleyError instanceof NoConnectionError) {
                        error_msg = "No Internet Connection";
                        build_no_internet_msg();

                    } else if (volleyError instanceof TimeoutError) {
                        error_msg = "Server is taking too long to respond";

                    } else if (volleyError instanceof AuthFailureError) {
                        error_msg = "Error Occured, Please try later";

                    } else if (volleyError instanceof ServerError) {
                        error_msg = "Server Error, Please try later";

                    } else if (volleyError instanceof NetworkError) {
                        error_msg = "Network Error, Please try later";

                    } else if (volleyError instanceof ParseError) {
                        error_msg = "Error Occured, Please try later";
                    }
                    restaurants.dismiss();
                    if (option_selected == 2) {
                        loading.dismiss();
                    }

                    Toast toast = Toast.makeText(getActivity(), " " + error_msg + " ", Toast.LENGTH_SHORT);
                    toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                    toast.show();
                }

            }
        });

        AppController.getInstance().getRequestQueue().add(get_here_locations_list);

        Log.e("get_here_locations_list", "sent");


    }

    private void get_home_Addresses() {

        address_list.show();

        address_orders_progressBar.setVisibility(View.VISIBLE);

        address_Seq.clear();
        address_Names.clear();
        address_Addresses.clear();

        addressAdapter.notifyDataSetChanged();

        Log.e("get_Addresses_url", "" + getString(R.string.base_url) + getString(R.string.get_Addresses) + AppController.getInstance().sharedPreferences.getString("id", ""));

        get_Addresses = new StringRequest(Request.Method.GET, getString(R.string.base_url) + getString(R.string.get_Addresses) + AppController.getInstance().sharedPreferences.getString("id", ""),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (isVisible()) {

                            Log.e("response", "" + response);


                            try {
                                jsonArray = new JSONArray(response);


                                if (jsonArray.length() == 0) {
                                    address_list.dismiss();
                                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    builder.setMessage("You have not yet added any address. Do you want to add one now?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                                            Bundle b = new Bundle();
                                            b.putString("new", "yes");
                                            startActivity(new Intent(getActivity(), AddEditAddress.class).putExtras(b));


                                        }
                                    }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                                            dialog.cancel();
                                        }
                                    });
                                    final AlertDialog alert = builder.create();
                                    alert.show();
                                    alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                                    alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));

                                }

                                String temp_address = "";


                                for (int i = 0; i < jsonArray.length(); i++) {
                                    address_Seq.add(jsonArray.getJSONObject(i).getString("id"));
                                    address_Names.add(jsonArray.getJSONObject(i).getString("company"));

                                    temp_address = jsonArray.getJSONObject(i).getString("address1") + "," + jsonArray.getJSONObject(i).getString("address2") + ","
                                            + jsonArray.getJSONObject(i).getString("location0") + ","
                                            + jsonArray.getJSONObject(i).getString("location1") + ","
                                            + jsonArray.getJSONObject(i).getString("location2") + ","
//                                            +jsonArray.getJSONObject(i).getString("zip")+","
                                            + jsonArray.getJSONObject(i).getString("city_state").replace(",", ", ") + "-" + jsonArray.getJSONObject(i).getString("zip");

                                    temp_address = temp_address.replaceAll(",,,,", ", ");
                                    temp_address = temp_address.replaceAll(",,,", ", ");
                                    temp_address = temp_address.replaceAll(",,", ", ");
                                    temp_address = temp_address.replaceAll(" ,", ", ");

                                    address_Addresses.add(temp_address);

                                }

                                address_orders_progressBar.setVisibility(View.GONE);
                                addressAdapter.notifyDataSetChanged();

                            } catch (JSONException e) {

                                address_orders_progressBar.setVisibility(View.GONE);

                                Log.e("e", e.toString());

                            }
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                if (isVisible()) {


                    String error_msg = "";

                    Log.e("error", "" + volleyError.toString());

                    address_list.dismiss();

                    if (volleyError instanceof NoConnectionError) {
                        error_msg = "No Internet Connection";
                        build_no_internet_msg();

                    } else if (volleyError instanceof TimeoutError) {
                        error_msg = "Server is taking too long to respond";

                    } else if (volleyError instanceof AuthFailureError) {
                        error_msg = "Error Occured, Please try later";

                    } else if (volleyError instanceof ServerError) {
                        error_msg = "Server Error, Please try later";

                    } else if (volleyError instanceof NetworkError) {
                        error_msg = "Network Error, Please try later";

                    } else if (volleyError instanceof ParseError) {
                        error_msg = "Error Occured, Please try later";
                    }

//                        Toast.makeText(getActivity(),error_msg,Toast.LENGTH_LONG).show();
                    Toast toast = Toast.makeText(getActivity(), " " + error_msg + " ", Toast.LENGTH_LONG);
                    toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                    toast.show();
                }
            }
        });

        AppController.getInstance().getRequestQueue().add(get_Addresses);
    }


    private void reset_all_buttons() {

        Log.e("change", "icons");

        in_car_img.setImageResource(R.drawable.ic_drive_eta_black_36dp);
        here_img.setImageResource(R.drawable.ic_nature_people_black_36dp);
        pickup_img.setImageResource(R.drawable.ic_shopping_basket_black_36dp);
        home_img.setImageResource(R.drawable.ic_home_black_36dp);

        in_car_bg.setBackgroundColor(getResources().getColor(R.color.white));
        here_bg.setBackgroundColor(getResources().getColor(R.color.white));
        pickup_bg.setBackgroundColor(getResources().getColor(R.color.white));
        home_bg.setBackgroundColor(getResources().getColor(R.color.white));

        in_car_text.setTypeface(null, Typeface.NORMAL);
        here_text.setTypeface(null, Typeface.NORMAL);
        pickup_text.setTypeface(null, Typeface.NORMAL);
        home_text.setTypeface(null, Typeface.NORMAL);

        in_car_text.setTextColor(getResources().getColor(R.color.black));
        here_text.setTextColor(getResources().getColor(R.color.black));
        pickup_text.setTextColor(getResources().getColor(R.color.black));
        home_text.setTextColor(getResources().getColor(R.color.black));

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.e("onMapReady5", "onMapReady6");
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(true);


//        mMap.setTrafficEnabled(true);

        View locationButton = ((View) mapFragment.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        locationButton.setVisibility(View.GONE);

//        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
//
//        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
//        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
//        rlp.setMargins(0, 0, 30, 30);
//
//        locationButton.setLayoutParams(rlp);

        locationButton.setVisibility(View.GONE);
        ((View) mapFragment.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2")).setBackground(getActivity().getResources().getDrawable(R.drawable.ic_account_circle_black_36dp));

        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.setOnMarkerClickListener(this);

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        } else {

            mMap.setMyLocationEnabled(true);

            locationButton.setVisibility(View.GONE);

            if (!last_long.equals("empty")) {
                zoom_value = (((mMap.getMaxZoomLevel() + mMap.getMinZoomLevel()) / 2) + ((mMap.getMaxZoomLevel() + mMap.getMinZoomLevel()) / 4)) + "";
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Float.parseFloat(last_lat), Float.parseFloat(last_long + "")), Float.parseFloat(zoom_value)));

                Log.e("movedCamera", mMap.getCameraPosition().zoom + "");
            }

        }


        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {

                my_location.setImageResource(R.drawable.ic_my_location_black_48dp_gray);

                if (!locked) {

                    action_hint.setText("Getting Address...");
                    address.setText("....");

                    LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
//                Log.e("bounds",bounds.toString());

                }


//                Log.e("center",mMap.getCameraPosition().target+"");

                if (mLastLocation != null) {
//                    Log.e("mLastLocation2",mLastLocation.getLatitude()+","+mLastLocation.getLongitude());

                    Location current_location = new Location(mLastLocation);
                    current_location.setLatitude(mMap.getCameraPosition().target.latitude);
                    current_location.setLongitude(mMap.getCameraPosition().target.longitude);

                    if (mLastLocation.distanceTo(current_location) < 1) {
//                        Log.e("distanceTo","was less");
                        my_location.setImageResource(R.drawable.ic_my_location_black_48dp);
                    }

//                    Log.e("distance",mLastLocation.distanceTo(current_location)+"");
                }


            }
        });


        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {

//
//                if(polyline.size()!=0)
//                {
//                    Log.e("polyline ", polyline.toString());
//                    Log.e("isLocationOnPath ", com.google.maps.android.PolyUtil.isLocationOnPath(new LatLng(mMap.getCameraPosition().target.latitude,mMap.getCameraPosition().target.longitude),polyline,false,20)+"");
//
//                }

                if (!locked && (option_selected == 0 || option_selected == 1)) {

                    if (!ConnectivityReceiver.isConnected()) {
//                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_LONG);
                        Toast toast = Toast.makeText(getActivity(), " No Internet Connection ", Toast.LENGTH_LONG);
                        toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                        toast.show();

                        build_no_internet_msg();
                    } else {
                        get_address();
                    }

                }
            }
        });

        final ViewTreeObserver vto = pin.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                if (isVisible()) {

                    int result = 0;
                    int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
                    if (resourceId > 0) {
                        result = getResources().getDimensionPixelSize(resourceId);
                    }

                    Log.e("status bar height ", result + "");

                    ViewGroup.LayoutParams layoutParams = pin.getLayoutParams();
                    ViewGroup.MarginLayoutParams address_bar_params = (ViewGroup.MarginLayoutParams) address_bar.getLayoutParams();
                    layoutParams.height = ((getActivity().getResources().getDisplayMetrics().heightPixels / 2) - linearLayout.getHeight() - getActivity().findViewById(R.id.toolbar).getHeight() - address_bar_params.topMargin - result) + convertDpToPixel(10);
                    pin.setLayoutParams(layoutParams);
                    pin.setVisibility(View.VISIBLE);


                    try {
                        vto.removeOnGlobalLayoutListener(this);
                    } catch (Exception e) {
                        pin.getViewTreeObserver();
                        Log.e("vto Exception", e.toString());
                    }

                }

            }
        });


    }

    public static int convertDpToPixel(float dp) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + getString(R.string.google_maps_key);

        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {

        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.e("Exception ", "while downloading url" + e.toString());

            String temp_msg = e.toString();

            for (int i = 0; i < e.getStackTrace().length; i++) {
                temp_msg = temp_msg + "\n" + e.getStackTrace()[i];
            }

            AppController.getInstance().show_popup_alert_without_title(temp_msg, getActivity());

        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        marker_tag = marker.getTag().toString();
        marker.showInfoWindow();

        Log.e("marker", marker.getTag().toString());

        String selected_restaurant_seq;
        int selected_restaurant;

        if (marker.getTag().toString().contains("Pistiop")) {
            //get all the restaurants linked to the selected Delivery Point and show them
            if (option_selected == 0) {

                LatLng latLng;

                Seq.clear();
                Names.clear();
                Times.clear();
                Photo_Url.clear();

                restaurantAdapter.notifyDataSetChanged();

                latLng = marker.getPosition();

                delivery_location.setLatitude(latLng.latitude);
                delivery_location.setLongitude(latLng.longitude);

                String tag = marker.getTag().toString();

                selected_pitstop_name = tag.substring(tag.indexOf("_") + 1, tag.length());
                tag = tag.substring(0, tag.indexOf("_"));

                Log.e("PitsopClick", marker.getTag().toString() + " : " + selected_pitstop_name + " : " + tag);

                selected_pitstop = Integer.parseInt(tag.replaceAll("[^0-9]", ""));

                try
                {
                    AppController.getInstance().collection_point_latitude = collectionPoints.get(selected_pitstop+"").latitude+"";
                    AppController.getInstance().collection_point_langitude = collectionPoints.get(selected_pitstop+"").longitude+"";

                }catch (Exception e)
                {
                    Log.e("Exception",e.toString());
                }

                Log.e("collection_points", AppController.getInstance().collection_point_latitude+" "+AppController.getInstance().collection_point_langitude);


                Log.e("popup","C");
                restaurants.show();
                if (!ConnectivityReceiver.isConnected()) {
//                    Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_LONG);
                    Toast toast = Toast.makeText(getActivity(), " No Internet Connection ", Toast.LENGTH_LONG);
                    toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                    toast.show();

                    build_no_internet_msg();
                } else {

                    String url = getDirectionsUrl(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), new LatLng(latLng.latitude, latLng.longitude));
                    Log.e("DownloadTask", "called from 2748");
                    DownloadTask downloadTask = new DownloadTask();
                    downloadTask.execute(url, "pitstop_just_time");
                    get_restaurants_related_to_pitstop();
                }

                list_popup_progressBar.setVisibility(View.VISIBLE);
            }
        } else if (marker.getTag().toString().contains("restaurant_pin")) {

            Log.e("restaurant_pin", "restaurant_pin was clicked");

            selected_restaurant_seq = "" + Integer.parseInt(marker.getTag().toString().replaceAll("[^0-9]", ""));
            selected_restaurant = Seq.indexOf(selected_restaurant_seq);
            restaurant_b = new Bundle();
            restaurant_i = new Intent(getActivity(), Menus.class);
            restaurant_b.putString("restaurant_id", selected_restaurant_seq + "");
            restaurant_b.putString("restaurant_name", Names.get(selected_restaurant));
            AppController.getInstance().option_selected = option_selected;
            AppController.getInstance().delivery_location.setLatitude(Double.parseDouble(rest_lat.get(selected_restaurant)));
            AppController.getInstance().delivery_location.setLongitude(Double.parseDouble(rest__long.get(selected_restaurant)));
            AppController.getInstance().restaurant_id = selected_restaurant_seq + "";
            AppController.getInstance().full_address = "-";


            String url = getDirectionsUrl(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), new LatLng(AppController.getInstance().delivery_location.getLatitude(), AppController.getInstance().delivery_location.getLongitude()));
            Log.e("DownloadTask", "called from 2770");
            DownloadTask downloadTask = new DownloadTask();

            downloadTask.execute(url, "just_time");

            loading.show();

        }

        return false;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Log.e("click", latLng.toString());
    }

    public void toggle_traffic(Boolean traffic) {

        mMap.setTrafficEnabled(traffic);
    }

    /**
     * A class to download data from Google Directions URL
     */
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        String time_of = "";

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                time_of = url[1];
                query_url = url[0];
                data = downloadUrl(url[0]);

                Log.e("google map", "query url" + url[0]);

            } catch (Exception e) {

                String temp_msg = e.toString();

                for (int i = 0; i < e.getStackTrace().length; i++) {
                    temp_msg = temp_msg + "\n" + e.getStackTrace()[i];
                }

                AppController.getInstance().show_popup_alert_without_title(temp_msg, getActivity());
            }

//            Log.e("data",data);

            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

//            AppController.getInstance().show_popup_alert(deviceDetails+"\n"+"Link : "+query_url,getActivity());

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result, time_of);


        }
    }


    /**
     * A class to parse the Google Directions in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        String time_of = "";

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            time_of = jsonData[1];

            try {

                jObject = new JSONObject(jsonData[0]);
                parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);

//                Log.e("jObject", jObject.toString());

            } catch (Exception e) {

                String temp_msg = e.toString();

                for (int i = 0; i < e.getStackTrace().length; i++) {
                    temp_msg = temp_msg + "\n" + e.getStackTrace()[i];
                }

                AppController.getInstance().show_popup_alert_without_title(temp_msg, getActivity());
            }

//            Log.e("routes",routes.toString());

            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            AppController.getInstance().travel_time = parser.get_total_time();

            Log.e("time from google map", "" + AppController.getInstance().travel_time);

            if(time_of.contains("pitstop_just_time"))
            {
                AppController.getInstance().pitstopTravelTime =  AppController.getInstance().travel_time_mins;

                Log.e("pitstopTravelTime",AppController.getInstance().pitstopTravelTime);

            }


            if (!time_of.contains("just_time") && !time_of.contains("pitstop_just_time")) {
                locked = true;

                LatLngBounds.Builder builder = new LatLngBounds.Builder();

                PolylineOptions lineOptions = null;
                lineOptions = new PolylineOptions();

                lineOptions.zIndex(999999);

                switch (option_selected) {
                    case 0:

                        View customMarkerView;
                        TextView time;
                        Bitmap returnedBitmap;
                        Canvas canvas;
                        Drawable drawable;

                        customMarkerView = layoutInflater.inflate(R.layout.custom_marker, null);
                        time = (TextView) customMarkerView.findViewById(R.id.time);
                        time.setText(parser.get_total_time());

                        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
                        customMarkerView.buildDrawingCache();
                        returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                                Bitmap.Config.ARGB_8888);
                        canvas = new Canvas(returnedBitmap);
                        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
                        drawable = customMarkerView.getBackground();
                        if (drawable != null)
                            drawable.draw(canvas);
                        customMarkerView.draw(canvas);

                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude))
                                .icon(BitmapDescriptorFactory.fromBitmap(returnedBitmap))).setTag("selected_pin");

                        CircleOptions circleOptions = new CircleOptions();
                        // Specifying the center of the circle
                        circleOptions.center(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
                        // Radius of the circle
                        circleOptions.radius(4000);
                        // Border color of the circle
                        circleOptions.strokeColor(Color.TRANSPARENT);
                        circleOptions.strokeWidth(0);
                        // Fill color of the circle
                        circleOptions.fillColor(0x3000ff00);
                        // Adding the circle to the GoogleMap
//                        mMap.addCircle(circleOptions);

                        Log.e("circle_drawn", mLastLocation.getLatitude() + " , " + mLastLocation.getLongitude());

                        delivery_location.setLatitude(mMap.getCameraPosition().target.latitude);
                        delivery_location.setLongitude(mMap.getCameraPosition().target.longitude);


                        lineOptions.color(Color.parseColor("#4285f4"));

                        break;

                    case 4:

                        Log.e("t2", time_of);

                        if (time_of.equals("user_destination")) {

                            user_destination_time = parser.get_total_time();

                            customMarkerView = layoutInflater.inflate(R.layout.custom_pitstop, null);
                            time = (TextView) customMarkerView.findViewById(R.id.time);
                            time.setText(user_destination_time);

                            if(hideTime)
                            {
                                ((View)time.getParent()).setVisibility(View.INVISIBLE);
                            }

                            if (Integer.parseInt(current_orders_Order_type.get(current_position)) == 3) {
                                customMarkerView = layoutInflater.inflate(R.layout.custom_marker, null);
                                time = (TextView) customMarkerView.findViewById(R.id.time);
                                time.setText(current_orders_Names.get(current_position) + " \n " + user_destination_time);
                            }


                            customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                            customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
                            customMarkerView.buildDrawingCache();
                            returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                                    Bitmap.Config.ARGB_8888);
                            canvas = new Canvas(returnedBitmap);
                            canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
                            drawable = customMarkerView.getBackground();
                            if (drawable != null)
                                drawable.draw(canvas);
                            customMarkerView.draw(canvas);

                            destination_marker = mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(destination_location.getLatitude(), destination_location.getLongitude()))
                                    .icon(BitmapDescriptorFactory.fromBitmap(returnedBitmap)));

                            destination_marker.setTag("user_destination");

                            lineOptions.color(Color.parseColor("#4285f4"));

                            user_destination_distance = "" + destination_location.distanceTo(mLastLocation);

                            Log.e("usr_dest_dist", user_destination_distance + "");
                            Log.e("user_destination_time", user_destination_time + "");

                            builder.include(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
                            builder.include(new LatLng(destination_location.getLatitude(), destination_location.getLongitude()));
                            user_drawn = true;

                        } else if (time_of.equals("delivery_boy_destination")) {

                            delivery_boy_destination_time = parser.get_total_time();

                            customMarkerView = layoutInflater.inflate(R.layout.custom_delivery, null);
                            time = (TextView) customMarkerView.findViewById(R.id.time);
                            time.setText(delivery_boy_destination_time);
                            ((View)time.getParent()).setVisibility(View.INVISIBLE);

                            customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                            customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
                            customMarkerView.buildDrawingCache();
                            returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                                    Bitmap.Config.ARGB_8888);
                            canvas = new Canvas(returnedBitmap);
                            canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
                            drawable = customMarkerView.getBackground();
                            if (drawable != null)
                                drawable.draw(canvas);
                            customMarkerView.draw(canvas);

                            delivery_boy_marker = mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(delivery_boy_location.getLatitude(), delivery_boy_location.getLongitude()))
                                    .icon(BitmapDescriptorFactory.fromBitmap(returnedBitmap)));


                            delivery_boy_marker.setTag("user_destination");

                            lineOptions.color(Color.parseColor("#000080"));

                            delivery_boy_destination_distance = "" + delivery_boy_location.distanceTo(destination_location);

                            Log.e("delivery_boy_distance", delivery_boy_destination_distance + "");
                            Log.e("delivery_boy_time", delivery_boy_destination_time + "");

                            builder.include(new LatLng(delivery_boy_location.getLatitude(), delivery_boy_location.getLongitude()));

                            drawn = true;

                        }


                }

                ArrayList<LatLng> points = null;

                polyline.clear();
                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<LatLng>();


                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                        builder.include(position);
                        polyline.add(position);
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(8);

                }

                // Drawing polyline in the Google Map for the i-th route

                action_hint.setText("Selected Destination");
                go.setVisibility(View.INVISIBLE);
                pin.setVisibility(View.GONE);
                address_bar.setVisibility(View.GONE);
                loading.dismiss();
                mMap.addPolyline(lineOptions);


                builder.include(new LatLng(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude));
                builder.include(mMap.getProjection().getVisibleRegion().latLngBounds.northeast);
                builder.include(mMap.getProjection().getVisibleRegion().latLngBounds.southwest);
                builder.include(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));


                bounds = builder.build();


                if (option_selected != 0) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));
                }

                Log.e("bound", 200 + "");
            } else {
                loading.dismiss();

                if (!time_of.contains("pitstop_just_time")) {
                    Log.e(" total_time", AppController.getInstance().travel_time + "");
                    Log.e("opened from", "f1 3117");
                    loading.dismiss();
                    restaurants.dismiss();
                    restaurant_i.putExtras(restaurant_b);
                    startActivity(restaurant_i);

                    Log.e("travel_time_mins", "A" + AppController.getInstance().travel_time_mins);

                    address_bar.setVisibility(View.GONE);
                    pin.setVisibility(View.GONE);

                    mMap.clear();

                } else {

                    pitstop_title.setText("Delivery Point is " + AppController.getInstance().travel_time + " away");
//                    Toast.makeText(getActivity(),"Please select a restaurant",Toast.LENGTH_LONG).show();
                    Log.e(" pitstop_just_time", AppController.getInstance().travel_time + "");
                }
            }

            Log.e("condition", "option_selected==0 is " + (option_selected == 0) + "  !marker_tag.contains(Pistiop) " + !marker_tag.contains("Pistiop") + " value was" + marker_tag);

            if (option_selected == 0 && !marker_tag.contains("Pistiop")) {
                loading.show();
                Log.e("called", "from 3080");
                get_pitstop();

            }
        }
    }

    void get_address() {

        if (isVisible()) {

            new DownloadAddressTask().execute();

        }

    }


    private class DownloadAddressTask extends AsyncTask<String, Void, String> {

        Double latitude = mMap.getCameraPosition().target.latitude;
        Double longitude = mMap.getCameraPosition().target.longitude;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... url) {

            String data = "";

            full_address = "";

            try {

                addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                if (addresses != null) {
                    String address_value = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();

//                  Log.e("addresses","getMaxAddressLineIndex"+addresses.get(0).getMaxAddressLineIndex());

                    for (int j = 0; j <= addresses.get(0).getMaxAddressLineIndex(); j++) {
                        full_address = full_address + " " + addresses.get(0).getAddressLine(j);
                    }
//
//        Log.e("full address","value"+full_address);
//        Log.e("full address",addresses.get(0).toString());
//        Log.e("address",address_value);
//        Log.e("city",city);
//        Log.e("state",state);
//        Log.e("country",country);
//        Log.e("postalCode",postalCode);
//        Log.e("knownName",knownName);


                }

            } catch (Exception e) {
                Log.e("addresses Exception", e.toString());
            }


            return data;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            address.setText(full_address);

            if (option_selected == 0) {
                action_hint.setText("Enter your trip destination...");

            } else if (option_selected == 1) {
                action_hint.setText("Select Delivery Location");
            }

        }
    }


    public void get_current_city() {

//        mLastLocation.setLatitude(Double.parseDouble("18.9545"));
//        mLastLocation.setLongitude(Double.parseDouble("72.8144"));

        if (isVisible()) {

            full_address = "";

            try {

                addresses = geocoder.getFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                if (addresses != null) {
                    String address_value = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();

                    Log.e("addresses", "getMaxAddressLineIndex" + addresses.get(0).getMaxAddressLineIndex());

                    for (int j = 0; j <= addresses.get(0).getMaxAddressLineIndex(); j++) {
                        full_address = full_address + " " + addresses.get(0).getAddressLine(j);
                    }
////
                    Log.e("full address", "value" + full_address);
                    Log.e("full address", addresses.get(0).toString());
                    Log.e("address", address_value);
                    Log.e("city", city);
                    Log.e("state", state);
                    Log.e("country", country);
                    Log.e("postalCode", postalCode);
                    Log.e("knownName", knownName);


                    current_city = city;

                    locked = false;
                    option_selected = 1;
                    here_img.setImageResource(R.drawable.ic_nature_people_black_36dp_red);
                    here_bg.setBackgroundResource(R.color.bg);
                    here_text.setTypeface(null, Typeface.BOLD);
                    here_text.setTextColor(getResources().getColor(R.color.colorPrimary));
                    show_here_locations_list();

                    address.setText(full_address);

                    if (option_selected == 0) {
                        action_hint.setText("Enter your trip destination...");

                    } else if (option_selected == 1) {
                        action_hint.setText("Select Delivery Location");
                    }

                }

            } catch (Exception e) {
                Log.e("addresses Exception", e.toString());
                loading.dismiss();
                AppController.getInstance().show_popup_alert_without_title("Unable to get current city at the moment. Please try later.", getActivity());
            }

        }
    }


    public void get_current_orders() {
        current_orders_Seq.clear();
        current_orders_Names.clear();
        current_orders_Items.clear();
        current_orders_Cost.clear();
        current_orders_Passcode.clear();
        current_orders_Order_type.clear();
        current_orders_Delivered_by.clear();
        current_orders_Status.clear();
        rms.clear();
        current_orders_name.clear();
        current_orders_ph.clear();
        current_orders_shipping_lat.clear();
        current_orders_shipping_long.clear();

        currentOrderAdapter.notifyDataSetChanged();
        current_orders.show();
        if (!ConnectivityReceiver.isConnected()) {
//            Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_LONG);
            Toast toast = Toast.makeText(getActivity(), " No Internet Connection ", Toast.LENGTH_LONG);
            toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
            toast.show();
            build_no_internet_msg();
        } else {
            make_get_current_orders_request();
        }

        //TODO uncoment this

        current_orders_progressBar.setVisibility(View.VISIBLE);
    }

    public void make_get_current_orders_request() {

        Log.e("url", "make_get_current_orders_request " + getString(R.string.base_url) + getString(R.string.orderlistnot_shipped) + "/" + AppController.getInstance().sharedPreferences.getString("id", ""));

        current_orders_request = new StringRequest(Request.Method.GET, getString(R.string.base_url) + getString(R.string.orderlistnot_shipped) + "/" + AppController.getInstance().sharedPreferences.getString("id", ""),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (isVisible()) {

                            loading.dismiss();
//
//            String  response = "[{\n" +
//                    "\t\"order_number\": \"147445058760\",\n" +
//                    "\t\"restaurant_name\": \"Cafe Coffee Day\",\n" +
//                    "\t\"order_type\": \"1\",\n" +
//                    "\t\"passcode\": \"Dhoni\",\n" +
//                    "\t\"delivered_by\": \"1\",\n" +
//                    "\t\"total\": \"123\",\n" +
//                    "\t\"items\": \"Burger, Gobi, Fried Rice\",\n" +
//                    "\t\"shipping_lat\": \"12.947915294178697\",\n" +
//                    "\t\"shipping_long\": \"77.52567283809185\",\n" +
//                    "\t\"phone\": \"7259575485\",\n" +
//                    "\t\"name\": \"Rakesh\"\n" +
//                    "},\n" +
//                    "{\n" +
//                    "\t\"order_number\": \"26324523434\",\n" +
//                    "\t\"restaurant_name\": \"MCD\",\n" +
//                    "\t\"order_type\": \"2\",\n" +
//                    "\t\"passcode\": \"Library\",\n" +
//                    "\t\"delivered_by\": \"0\",\n" +
//                    "\t\"total\": \"253\",\n" +
//                    "\t\"items\": \"Chicken, Cool Drinks, Soup\",\n" +
//                    "\t\"shipping_lat\": \"12.947915294178697\",\n" +
//                    "\t\"shipping_long\": \"77.52567283809185\",\n" +
//                    "\t\"phone\": \"9343265399\",\n" +
//                    "\t\"name\": \"Nithin\"\n" +
//                    "},\n" +
//                    "{\n" +
//                    "\t\"order_number\": \"4252123\",\n" +
//                    "\t\"restaurant_name\": \"Starbucks\",\n" +
//                    "\t\"order_type\": \"3\",\n" +
//                    "\t\"passcode\": \"TeamWork\",\n" +
//                    "\t\"delivered_by\": \"0\",\n" +
//                    "\t\"total\": \"642\",\n" +
//                    "\t\"items\": \"Chicken, Cool Drinks, Soup\",\n" +
//                    "\t\"shipping_lat\": \"12.947915294178697\",\n" +
//                    "\t\"shipping_long\": \"77.52567283809185\",\n" +
//                    "\t\"phone\": \"9343265399\",\n" +
//                    "\t\"name\": \"Nithin\"\n" +
//                    "}]";

                            Log.e("response", response);


                            try {
                                JSONArray jsonArray = new JSONArray(response);


                                if (jsonArray.length() == 0) {
                                    current_orders.dismiss();

                                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    builder.setMessage("You have not yet ordered anything!").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                                            dialog.cancel();

                                        }
                                    });
                                    final AlertDialog alert = builder.create();
                                    alert.show();
                                    alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                                    alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                                }

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    try {
                                        current_orders_Names.add(jsonArray.getJSONObject(i).get("restaurant_name") + "");
                                        current_orders_Seq.add(jsonArray.getJSONObject(i).get("order_number") + "");
                                        current_orders_Order_type.add(jsonArray.getJSONObject(i).get("order_type") + "");
                                        current_orders_Passcode.add(jsonArray.getJSONObject(i).get("passcode") + "");
                                        current_orders_Delivered_by.add(jsonArray.getJSONObject(i).get("delivered_by") + "");
                                        current_orders_Status.add(jsonArray.getJSONObject(i).get("status") + "");
                                        rms.add(jsonArray.getJSONObject(i).get("restaurant_manager_status") + "");
                                        current_orders_name.add(jsonArray.getJSONObject(i).get("name").toString() + "");
                                        current_orders_Cost.add(jsonArray.getJSONObject(i).get("total_cost") + "");
                                        current_orders_Items.add(" ");

                                        if (jsonArray.getJSONObject(i).get("order_type").toString().equals("3")) {
                                            current_orders_ph.add(jsonArray.getJSONObject(i).get("restaurant_phone").toString() + "");

                                        } else {

                                            current_orders_ph.add(jsonArray.getJSONObject(i).get("phone").toString() + "");
                                        }


                                        current_orders_shipping_lat.add(jsonArray.getJSONObject(i).get("shipping_lat") + "");
                                        current_orders_shipping_long.add(jsonArray.getJSONObject(i).get("shipping_long") + "");

                                    } catch (Exception e) {
                                        Log.e("Exception", "make_get_current_orders_request" + e.toString());
                                    }

                                }
                                Log.e("phone numbers", current_orders_ph.toString());


                            } catch (Exception e) {
                                Log.e("Exception", e.toString());
                                if (response.contains("order list  could not be found for the customer")) {
                                    current_orders.dismiss();

                                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    builder.setMessage("You have not yet ordered anything!").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                                            dialog.cancel();

                                        }
                                    });
                                    final AlertDialog alert = builder.create();
                                    alert.show();
                                    alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                                    alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                                }
                            }

                            current_orders_progressBar.setVisibility(View.GONE);
                            currentOrderAdapter.notifyDataSetChanged();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                current_orders_progressBar.setVisibility(View.GONE);

                String error_msg = "";

                Log.e("error", "" + volleyError.toString());

                loading.dismiss();

                if (volleyError instanceof NoConnectionError) {
                    error_msg = "No Internet Connection";
                    build_no_internet_msg();

                } else if (volleyError instanceof TimeoutError) {
                    error_msg = "Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError) {
                    error_msg = "Error Occured, Please try later";

                } else if (volleyError instanceof ServerError) {
                    error_msg = "Server Error, Please try later";

                } else if (volleyError instanceof NetworkError) {
                    error_msg = "Network Error, Please try later";

                } else if (volleyError instanceof ParseError) {
                    error_msg = "Error Occured, Please try later";
                }

//                Toast.makeText(getActivity(), error_msg, Toast.LENGTH_LONG);
                Toast toast = Toast.makeText(getActivity(), " " + error_msg + " ", Toast.LENGTH_LONG);
                toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                toast.show();
            }
        });

        AppController.getInstance().getRequestQueue().add(current_orders_request);
    }


    /**
     * Method to display the location on UI
     */
    private void displayLocation() {

//		mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {

            double latitude = mLastLocation.getLatitude();
            double longitude = mLastLocation.getLongitude();

            try {
                add_user_location();

            } catch (Exception e) {
                Log.e("add_user_location excep", e.toString());
            }
            //incomplete insert user location into the database.......


            if (option_selected == 4 && user_drawn) {
                //calculate new time and update the marker
                update_user_location();

            }


        } else {
            Log.e("error", "Couldn't get the location. Make sure location is enabled on the device");

        }
    }

    private void add_user_location() {

        Log.e("add_user_location", "" + getString(R.string.base_url) + getString(R.string.addlocation));

        adduserslocation = new StringRequest(Request.Method.POST, getString(R.string.base_url) + getString(R.string.addlocation),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("location insert", "" + response);
//                        AppController.getInstance().show_popup_alert("Customer Location Inserted \n"+mLastLocation.getLatitude()+" "+mLastLocation.getLongitude(),getActivity());

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {


            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // TODO Auto-generated method stub

                Map<String, String> params = new HashMap<String, String>();

                params.put("customer_id", "" + AppController.getInstance().sharedPreferences.getString("id", ""));
                params.put("latitude", "" + mLastLocation.getLatitude());
                params.put("langitude", "" + mLastLocation.getLongitude());

                Log.e("params", "" + params.toString());

                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        AppController.getInstance().getRequestQueue().add(adduserslocation);

        Log.e("location insert", "request sent");
    }

    /**
     * Creating google api client object
     */
    protected synchronized void buildGoogleApiClient() {

//        Log.e("buildGoogleApiClient","buildGoogleApiClient");


        mGoogleApiClient = new GoogleApiClient.Builder(getActivity()).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
    }

    /**
     * Creating location request object
     */
    protected void createLocationRequest() {
//        Log.e("createLocationRequest","createLocationRequest");


        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    /**
     * Method to verify google play services on the device
     */
    private boolean checkPlayServices() {
//        Log.e("checkPlayServices","checkPlayServices");


        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(), PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
//                Toast.makeText(getActivity(), "This device is not supported.", Toast.LENGTH_LONG).show();
                Toast toast = Toast.makeText(getActivity(), " This device is not supported. ", Toast.LENGTH_LONG);
                toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                toast.show();

            }
            return false;
        }
        return true;
    }

    /**
     * Starting the location updates
     */
    protected void startLocationUpdates() {

//        Log.e("startLocationUpdates","startLocationUpdates");


        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, locationListener);
        }


    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates() {
//        Log.e("stopLocationUpdates","stopLocationUpdates");

        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, locationListener);
    }

    /**
     * Google api callback methods
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
//        Log.e("onConnectionFailed", "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {
        // Once connected with google api, get the location

//        Log.e("onConnected","onConnected");

        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
//        Log.e("onConnectionSuspended","onConnectionSuspended");

        mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();

        tracking = true;

        if (mMap == null) {

            mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }

        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        checkPlayServices();

        // Resuming the periodic location updates
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }

        if(AppController.getInstance().collection_point_latitude.length()!=0 && AppController.getInstance().collection_point_langitude.length()!=0)
        {
            AppController.getInstance().collection_point_latitude = "" ;
            AppController.getInstance().collection_point_langitude = "" ;
        }


//        Log.e("height",getActivity().getResources().getDisplayMetrics().heightPixels+"");

    }


    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();

//            Log.e("onStart","connect");

        }

    }

    @Override
    public void onStop() {
        super.onStop();

//        if (handler != null) {
//            handler.removeCallbacks(t);
//        }


//        Log.e("onStop", "onStop");
//        Log.e("isHidden()", isHidden() + "");
//        Log.e("isVisible", isVisible() + "");
        Log.e("location", AppController.getInstance().sharedPreferences.getString("last_lat", "empty") + ", " + AppController.getInstance().sharedPreferences.getString("last_long", "empty"));
//        Log.e("last_lat",  + "");
//        Log.e("last_long",  + "");
        //
        // if (mGoogleApiClient.isConnected())
        // {
        // mGoogleApiClient.disconnect();
        // }
    }

    @Override
    public void onPause() {
        super.onPause();
        // stopLocationUpdates();
//        Log.e("paused", "paused");

//        if (handler != null) {
//            handler.removeCallbacks(t);
//        }

        tracking = false;
        mapFragment.onPause();

    }

    private void buildAlertMessageNoGps() {
        // TODO Auto-generated method stub

//        pin.setVisibility(View.GONE);
//        address_bar.setVisibility(View.GONE);

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                user_msg.setVisibility(View.GONE);
                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                loading.dismiss();

            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();
                loading.dismiss();
                user_msg.setText("Please enable GPS");

                user_msg.setVisibility(View.VISIBLE);

            }
        });


        final AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));

    }

    private void showAlert(String pop_msg) {

        AppController.getInstance().show_popup_alert_without_title("Sorry, we don't have any active Outlet to serve you from this Delivery Point at this time.\n\nPlease select another Delivery Point.",getActivity());

//        AlertDialog.Builder alert_builder = new AlertDialog.Builder(getActivity());
//        View dialogView = layoutInflater.inflate(R.layout.no_restaurant_popup, null);
//        alert_builder.setView(dialogView);
//
//        alert_builder.setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
//            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
//
//                dialog.cancel();
//
//
//            }
//        });
//        final AlertDialog alert = alert_builder.create();
//
//        TextView msg = (TextView) dialogView.findViewById(R.id.msg);
//        TextView rest_not_found_msg = (TextView) dialogView.findViewById(R.id.rest_not_found_msg);

//        java.util.Date date = new java.util.Date();
//
//        msg.setText(pop_msg + "\n" + date.toString());

//        alert.show();

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 9 && data != null) {

            address.setText(data.getExtras().getString("Address"));
            mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(Float.parseFloat(data.getExtras().getString("Lat") + ""), Float.parseFloat(data.getExtras().getString("Long") + ""))));

        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {

            try {
                loading.dismiss();

                Log.e("resultCode: ", resultCode + "");

                if (data != null && resultCode != 0) {

                    address.setText(data.getExtras().getString("Address"));
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble(data.getExtras().getString("Lat") + ""), Double.parseDouble(data.getExtras().getString("Long") + ""))));

                }

            } catch (Exception e) {
                String temp_msg = e.toString();

                for (int i = 0; i < e.getStackTrace().length; i++) {
                    temp_msg = temp_msg + "\n" + e.getStackTrace()[i];
                }


//                AppController.getInstance().show_popup_alert_without_title(temp_msg,getActivity());
            }
        }

    }


    private class AddressToLatLng extends AsyncTask {

        List<Address> addresses;
        Boolean exception_occured = false;

        @Override
        protected Object doInBackground(Object[] params) {

            try {

                addresses = geocoder.getFromLocationName(google_full_address, 1);
                if (addresses.size() == 0) {
                    exception_occured = true;
                }

            } catch (Exception e) {

                exception_occured = true;
                Log.e("Exception A", e.toString());
            }

            return null;
        }


        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            if (exception_occured) {
                loading.dismiss();
                Toast toast = Toast.makeText(getActivity(), " Error parsing address ", Toast.LENGTH_LONG);
                toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                toast.show();

            } else {

                delivery_location.setLatitude(addresses.get(0).getLatitude());
                delivery_location.setLongitude(addresses.get(0).getLongitude());

                Seq.clear();
                Names.clear();
                Times.clear();
                Photo_Url.clear();
                rest_lat.clear();
                rest__long.clear();

                restaurantAdapter.notifyDataSetChanged();
                Log.e("popup","D");
                restaurants.show();
                loading.dismiss();
                list_popup_progressBar.setVisibility(View.VISIBLE);

                get_restaurants_within_3km();

            }


        }
    }


}
