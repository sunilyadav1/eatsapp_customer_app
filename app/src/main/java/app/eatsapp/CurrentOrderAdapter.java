package app.eatsapp;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by developer.nithin@gmail.com
 */
public class CurrentOrderAdapter extends BaseAdapter {
    Activity activity;

    ArrayList<String> Seq;
    ArrayList<String> Names;
    ArrayList<String> Items;
    ArrayList<String> Cost;
    ArrayList<String> Passcode;
    ArrayList<String> current_orders_Order_type;
    LayoutInflater inflater;
    String rs;

    public CurrentOrderAdapter(Activity activity, ArrayList<String> Seq, ArrayList<String> Names, ArrayList<String> Items,ArrayList<String>  Cost,ArrayList<String>  Passcode,ArrayList<String> current_orders_Order_type) {
        // TODO Auto-generated constructor stub
        this.activity = activity;
        this.Seq = Seq;
        this.Names = Names;
        this.Items = Items;
        this.Cost = Cost;
        this.Passcode = Passcode;
        this.current_orders_Order_type = current_orders_Order_type;
        rs= activity.getString(R.string.Rs)+" ";


    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return Seq.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int id) {
        // TODO Auto-generated method stub
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2) {
        // TODO Auto-generated method stub
        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.current_order_cell, null);
        }


        TextView order_no = (TextView) convertView.findViewById(R.id.order_no);
        TextView total_cost_current_order = (TextView) convertView.findViewById(R.id.total_cost_current_order);

        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView item_names = (TextView) convertView.findViewById(R.id.item_names);
        TextView passcode = (TextView) convertView.findViewById(R.id.passcode);

        name.setText(Names.get(position));
        order_no.setText("# "+Seq.get(position));
        total_cost_current_order.setText(rs+" "+Cost.get(position));

        if(current_orders_Order_type.get(position).toString().contains("1"))
        {
            item_names.setText("DRIVE THROUGH");

        }else if(current_orders_Order_type.get(position).toString().contains("2"))
        {
            item_names.setText("HERE");

        }else if(current_orders_Order_type.get(position).toString().contains("3"))
        {
            item_names.setText("PICK UP");

        }else if(current_orders_Order_type.get(position).toString().contains("4"))
        {
            item_names.setText("DOORSTEP");

        }


        passcode.setText("Passcode : "+Passcode.get(position));

        return convertView;
    }

}
