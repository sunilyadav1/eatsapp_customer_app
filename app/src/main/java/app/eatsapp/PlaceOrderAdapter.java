package app.eatsapp;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by developer.nithin@gmail.com
 */
public class PlaceOrderAdapter extends BaseAdapter
{
    Activity activity;

    ArrayList<String> Seq;
    ArrayList<String>  Names;
    ArrayList<String> Prices;
    LayoutInflater inflater;
    String rs;

    public PlaceOrderAdapter(Activity activity,ArrayList<String> Seq, ArrayList<String> Names, ArrayList<String> Prices)
    {
        // TODO Auto-generated constructor stub
        this.activity = activity;
        this.Seq =Seq;
        this.Names = Names;
        this.Prices = Prices;
        rs= activity.getString(R.string.Rs)+" ";


    }

    @Override
    public int getCount()
    {
        // TODO Auto-generated method stub
        return Seq.size();
    }

    @Override
    public Object getItem(int position)
    {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int id)
    {
        // TODO Auto-generated method stub
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2)
    {
        // TODO Auto-generated method stub
        if(inflater == null)
        {
            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if(convertView ==null)
        {
            convertView = inflater.inflate(R.layout.order_cell, null);
        }

        TextView name =(TextView)convertView.findViewById(R.id.name);
        TextView cost=(TextView)convertView.findViewById(R.id.cost);

//        name.setText(Names.get(position)+" ( "+AppController.getInstance().final_quantity.get(Seq.get(position))+" )");
//        cost.setText(rs+(AppController.getInstance().final_quantity.get(Seq.get(position))*Integer.parseInt(Prices.get(position))));

        name.setText(Names.get(position));
        cost.setText(rs+Prices.get(position));

        return convertView;
    }

}