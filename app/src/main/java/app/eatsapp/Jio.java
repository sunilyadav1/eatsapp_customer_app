package app.eatsapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by developer.nithin@gmail.com
 */
public class Jio extends Activity {


    WebView mWebView;
    ProgressDialog progressDialog;

    String order_id ="123123123",timestamp = "20170405125217";
    String checksumSeed = "tSLfi8BMxohvWJTCfwd1cZuARH78myF21JAdgdNhvixbj7o6+uIA38WFm7VHQ0aGu8LyQYv8tRPyN+Ba0+nRLuBLZXK4PH2gxkSvJ7Jnhof0NJr3IktRPSUZIQi6hcFZxoElSuOD8KzxCkagJfxT/u4vLeGqdskKl3p46RxJAJvOp7VutGHK2MG1HE7X68E/cKJrEUk7v0vI+kUp2Mfyh7GE8wZ9enYBrkY6olGYS9pLHLv/zGAZzAZVadblK1+3";
    String data="";
    String checksum ="";
    StringRequest get_jio_pageRequest;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the layout from video_main.xml

        AppController.getInstance().set_task_bg(this);

        data = "10000002|1.00|"+order_id+"|WEB|100001000014146||"+getString(R.string.jio_url)+"response.php|"+timestamp+"|PURCHASE";
        checksum = new EncryptionUtil().hmacDigest(data,checksumSeed,"HmacSHA256");

        Log.e("jio", "v2 "+checksum);
        setContentView(R.layout.payment);
        AppController.getInstance().set_task_bg(this);
        mWebView = (WebView) findViewById(R.id.webview);

        progressDialog= new ProgressDialog(this);
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
//        progressDialog.show();

        try {

            mWebView.setWebViewClient(new WebViewClient());
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.getSettings().setAppCacheEnabled(true);
            mWebView.getSettings().setDomStorageEnabled(true);
            mWebView.loadUrl(getString(R.string.jio_url)+"purchase.php");

//            mWebView.setWebChromeClient(new WebChromeClient() {
//
//                public void onProgressChanged(WebView view, int progress) {
//
//                    Log.e("onProgressChanged", ""+progress );
//
//                    progressDialog.show();
//
//                    if (progress == 100) {
//
//                        progressDialog.dismiss();
//                        Log.e("onProgressChanged", "100"+mWebView.getUrl() );
//
////                        if(mWebView.getUrl().contains("https://app.eatsapp.in/jio_test/response.php"))
////                        {
////                            setResult(RESULT_OK);
////                            finish();
////                        }
//
//                    }
//                }
//
//            });




        } catch (Exception e) {
            Log.e("Exception",e.toString());
        }


        get_jio_page();
    }


    public void get_jio_page()
    {

        Log.e("get_jio_page_url", "" +"https://testpg.rpay.co.in/reliance-webpay/v1.0/jiopayments");

        get_jio_pageRequest = new StringRequest(Request.Method.POST, "https://testpg.rpay.co.in/reliance-webpay/v1.0/jiopayments",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response","get_jio_page "+response);
//                        mWebView.loadData(response, "text/html", "UTF-8");
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("VolleyError","get_jio_page "+error.getMessage());
                    }
                }){
            @Override
            protected Map<String, String> getParams()
            {
                // TODO Auto-generated method stub

                Map<String, String> params = new HashMap<String, String>();

                params.put("merchantid","100001000014146");
                params.put("clientid","10000002");
                params.put("channel","WEB");
                params.put("returl",getString(R.string.jio_url)+"response.php");
                params.put("checksum",checksum);
                params.put("token","");
                params.put("transaction.extref",order_id);
                params.put("transaction.timestamp",timestamp);
                params.put("transaction.txntype","PURCHASE");
                params.put("transaction.amount","1.00");
                params.put("transaction.currency","INR");
                params.put("subscriber.mobilenumber","820569065");

                Log.e("params", "" + params.toString());


                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };



        AppController.getInstance().getRequestQueue().add(get_jio_pageRequest);
    }



}
