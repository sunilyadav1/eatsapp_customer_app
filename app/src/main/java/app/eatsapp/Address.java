package app.eatsapp;

/**
 * Created by developer.nithin@gmail.com
 */
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;


import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;

import java.util.Map;


public class Address extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    RelativeLayout parent_layout;

    Dialog loading;

    RelativeLayout add_new_address;

    AddressAdapter addressAdapter;
    ListView listView;
    ArrayList<String> Seq= new ArrayList<String>();
    ArrayList<String>  Names= new ArrayList<String>();
    ArrayList<String> Addresses= new ArrayList<String>();

    StringRequest get_Addresses,delete_address;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    JSONArray jsonArray;

    View fragment_view;

    Address mContext;

    Bundle b;



    public Address() {
        // Required empty public constructor
    }

    public static Profile newInstance(String param1, String param2) {
        Profile fragment = new Profile();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragment_view = inflater.inflate(R.layout.fragment_address, container, false);

        loading = new Dialog(getActivity());
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading.setContentView(R.layout.loading);


        addressAdapter= new AddressAdapter(getActivity(),Seq,Names,Addresses,mContext,true);

        listView=(ListView)fragment_view.findViewById(R.id.listView);
        parent_layout=(RelativeLayout) fragment_view.findViewById(R.id.parent_layout);
        add_new_address=(RelativeLayout)fragment_view.findViewById(R.id.add_new_address);
        listView.setAdapter(addressAdapter);

        addressAdapter.notifyDataSetChanged();

        add_new_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                b = new Bundle();
                b.putString("new","yes");
                startActivity(new Intent(getActivity(),AddEditAddress.class).putExtras(b));
            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("clicled","listView");
            }
        });


        get_Addresses();

        return fragment_view;
    }

    private void get_Addresses() {

        loading.show();

        Seq.clear();
        Names.clear();
        Addresses.clear();

        Log.e("get_Addresses_url", "" +getString(R.string.base_url)+getString(R.string.get_Addresses) +AppController.getInstance().sharedPreferences.getString("id",""));

        get_Addresses  = new StringRequest(Request.Method.GET,getString(R.string.base_url)+getString(R.string.get_Addresses) +AppController.getInstance().sharedPreferences.getString("id","") ,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        loading.dismiss();

                        Log.e("response", "" +response);

                        if(response.contains("User address could not be found"))
                        {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage("You have not yet added any address. Do you want to add one now?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                                    b = new Bundle();
                                    b.putString("new","yes");
                                    startActivity(new Intent(getActivity(),AddEditAddress.class).putExtras(b));


                                }
                            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                                    dialog.cancel();
                                }
                            });
                            final AlertDialog alert = builder.create();
                            alert.show();
                            alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                            alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));

                        }else
                        {
                            try {
                                jsonArray = new JSONArray(response);

                                String temp_address = "";


                                for (int i = 0;i<jsonArray.length();i++)
                                {
                                    Seq.add(jsonArray.getJSONObject(i).getString("id"));
                                    Names.add(jsonArray.getJSONObject(i).getString("company"));

                                    temp_address = jsonArray.getJSONObject(i).getString("address1")+","+jsonArray.getJSONObject(i).getString("address2")+","
                                            +jsonArray.getJSONObject(i).getString("location0")+","
                                            +jsonArray.getJSONObject(i).getString("location1")+","
                                            +jsonArray.getJSONObject(i).getString("location2")+","
//                                            +jsonArray.getJSONObject(i).getString("zip")+","
                                            +jsonArray.getJSONObject(i).getString("city_state").replace(",",", ")+"-"+jsonArray.getJSONObject(i).getString("zip");

                                    temp_address = temp_address.replaceAll(",,,,",", ");
                                    temp_address = temp_address.replaceAll(",,,",", ");
                                    temp_address = temp_address.replaceAll(",,",", ");
                                    temp_address = temp_address.replaceAll(" ,",", ");

                                    Log.e("temp_address",",,"+temp_address.indexOf(",,"));

                                    Addresses.add(temp_address);

                                }


                                addressAdapter.notifyDataSetChanged();

                            } catch (JSONException e) {

                                Log.e("e",  e.toString());

                            }
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String error_msg="";

                Log.e("error", "" + volleyError.toString());

                loading.dismiss();

                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured, Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error, Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error, Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured, Please try later";
                }

                Toast toast = Toast.makeText(getActivity()," "+error_msg+" ",Toast.LENGTH_LONG);
                toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                toast.show();

            }
        }) ;

        AppController.getInstance().getRequestQueue().add(get_Addresses);
    }

    public void build_no_internet_msg() {



        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Your Internet seems to be turned OFF, do you want to turn it ON?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();

            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));


    }



    @Override
    public void onResume() {
        super.onResume();

        if(AppController.getInstance().added==true)
        {
            AppController.getInstance().added=false;
            get_Addresses();
        }



    }

    public void delete_address(final String id) {

        Log.e("delete_address_url", "" +getString(R.string.base_url)+getString(R.string.deleteadd));

        loading.show();
        delete_address = new StringRequest(Request.Method.POST,getString(R.string.base_url)+getString(R.string.deleteadd),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        loading.dismiss();

                        Log.e("response", "" +response);


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String error_msg="";

                Log.e("error", "" + volleyError.toString());

                loading.dismiss();

                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured, Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error, Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error, Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured, Please try later";
                }

                Snackbar snackbar = Snackbar
                        .make(parent_layout, error_msg, Snackbar.LENGTH_LONG) ;
                snackbar.show();

            }
        }){
            @Override
            protected Map<String, String> getParams()
            {
                // TODO Auto-generated method stub

                Map<String, String> params = new HashMap<String, String>();

                params.put("customers_address_bank_id",id);
                Log.e("params", "" + params.toString());
                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");



                return params;
            }

        };

        AppController.getInstance().getRequestQueue().add(delete_address);

    }
}