package app.eatsapp;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by developer.nithin@gmail.com for parsing time from google map direction api
 */
public class Test extends Activity {

    Button check;
    EditText value;
    String temp_name="";
    int total_time=0;
    String delivery_boy_destination_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppController.getInstance().set_task_bg(this);

        setContentView(R.layout.test);

        check=(Button)findViewById(R.id.check);
        value=(EditText)findViewById(R.id.value);


        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                map_time_data_parse();
                Float old_time=0f;

                delivery_boy_destination_time = value.getText().toString();

//                Log.e("del_boy_dest_tim",delivery_boy_destination_time);
//                Log.e("old_time extracted",delivery_boy_destination_time.substring(0,delivery_boy_destination_time.indexOf(" "))+"");

                if(delivery_boy_destination_time.contains("h"))
                {
                    old_time = Float.parseFloat(delivery_boy_destination_time.substring(0,delivery_boy_destination_time.indexOf(" ")));

                    old_time=old_time*60;

                }else
                {
                    old_time = Float.parseFloat(delivery_boy_destination_time.substring(0,delivery_boy_destination_time.indexOf(" ")));

                }


                Log.e("final old_time",old_time+"");


            }
        });



    }

    private void map_time_data_parse() {

        temp_name=value.getText().toString();
        total_time=0;

        if(temp_name.contains("hours"))
        {
            total_time =total_time+Integer.parseInt(temp_name.substring(0,temp_name.indexOf("h")-1))*60+Integer.parseInt(temp_name.substring(temp_name.indexOf("s")+2,temp_name.indexOf("m")-1));


        }else if(temp_name.contains("hour"))
        {
            total_time =total_time+Integer.parseInt(temp_name.substring(0,temp_name.indexOf("h")-1))*60+Integer.parseInt(temp_name.substring(temp_name.indexOf("r")+2,temp_name.indexOf("m")-1));

        }else
        {
            total_time=total_time+Integer.parseInt(temp_name.replaceAll("[^0-9]", ""));
        }


        Log.e("temp_name",temp_name);
        Log.e("total_time",total_time+"");
    }
}
