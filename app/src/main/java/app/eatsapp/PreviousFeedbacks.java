package app.eatsapp;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import java.util.Date;
import java.util.HashMap;

import java.util.Map;

/**
 * Created by developer.nithin@gmail.com
 */
public class PreviousFeedbacks extends AppCompatActivity{

    RelativeLayout parent_layout;

    Dialog loading,promo_popup;

    RelativeLayout add_new_address;

    PreviousFeedbackAdapter previousFeedbackAdapter ;
    ListView listView;
    ArrayList<String> Seq= new ArrayList<String>();
    ArrayList<String>  Feedbacks= new ArrayList<String>();   
    ArrayList<String>  Time= new ArrayList<String>();

    StringRequest get_feedbacks,update_Feedbacks,delete_Feedback;

    String clicked_id="",changed_feedback="";
     
   
    PreviousFeedbacks mContext;

    TextView apply,title;
    EditText value;

    Bundle b;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat sdf_new = new SimpleDateFormat("dd-MM-yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppController.getInstance().set_task_bg(this);
        
        setContentView(R.layout.previous_feedback);

        mContext = this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Previous Feedback");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loading = new Dialog(PreviousFeedbacks.this);
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading.setContentView(R.layout.loading);


        previousFeedbackAdapter= new PreviousFeedbackAdapter(PreviousFeedbacks.this,Seq,Feedbacks,mContext,Time);

        listView=(ListView)findViewById(R.id.listView);
        parent_layout=(RelativeLayout) findViewById(R.id.parent_layout);
        add_new_address=(RelativeLayout)findViewById(R.id.add_new_address);
        listView.setAdapter(previousFeedbackAdapter);

        promo_popup = new Dialog(this);
        promo_popup.requestWindowFeature(Window.FEATURE_NO_TITLE);
        promo_popup.setContentView(R.layout.edit_feedback_popup);
        apply = (TextView) promo_popup.findViewById(R.id.apply);
        title = (TextView) promo_popup.findViewById(R.id.title);
        value = (EditText) promo_popup.findViewById(R.id.value);

        previousFeedbackAdapter.notifyDataSetChanged();

        get_Feedbacks();

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(value.getText().toString().length()==0)
                {
//                    Toast.makeText(PreviousFeedbacks.this,"Feedback cant be empty",Toast.LENGTH_LONG).show();
                    Toast toast = Toast.makeText(PreviousFeedbacks.this," Feedback cant be empty ",Toast.LENGTH_LONG);
                    toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                    toast.show();
                }else
                {
                    changed_feedback=value.getText().toString();
                    update_Feedbacks();
                }
            }
        });
        
    }

    private void get_Feedbacks() {

        Seq.clear();
        Feedbacks.clear();
        Time.clear();

        loading.show();

        Log.e("get_Feedbacks_url", "" +getString(R.string.base_url)+getString(R.string.getfeedbacks));


        get_feedbacks = new StringRequest(Request.Method.POST,getString(R.string.base_url)+getString(R.string.getfeedbacks),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("response", "" +response);

                        try {
                            String data_value =  new JSONObject(response).get("data").toString();
                            JSONArray jsonArray = new JSONArray(data_value);

                            for (int i = 0;i<jsonArray.length();i++)
                            {
                                Seq.add(jsonArray.getJSONObject(i).getString("id"));
                                Feedbacks.add(jsonArray.getJSONObject(i).getString("user_feedback"));

                                try {

                                    Time.add(sdf_new.format((Date)sdf.parse(jsonArray.getJSONObject(i).getString("date"))));

                                }catch (Exception e) {
                                    Log.e("Exception", "" +e.toString());
                                }



                            }

                            previousFeedbackAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            Log.e("JSONException", "" +e.toString());
                        }

                        loading.dismiss();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String error_msg="";

                Log.e("error", "" + volleyError.toString());

                loading.dismiss();

                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured, Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error, Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error, Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured, Please try later";
                }

                Snackbar snackbar = Snackbar
                        .make(parent_layout, error_msg, Snackbar.LENGTH_LONG) ;
                snackbar.show();

            }
        }){
            @Override
            protected Map<String, String> getParams()
            {
                // TODO Auto-generated method stub

                Map<String, String> params = new HashMap<String, String>();
                params.put("customer_id",""+AppController.getInstance().sharedPreferences.getString("id",""));
                Log.e("params", "" + params.toString());

                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        AppController.getInstance().getRequestQueue().add(get_feedbacks);

    }
    public void build_no_internet_msg() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(PreviousFeedbacks.this);
        builder.setMessage("Your Internet seems to be turned OFF, do you want to turn it ON?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();

            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        alert.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));


    }

    private void update_Feedbacks() {

        Log.e("update_Feedbacks_url", "" +getString(R.string.base_url)+getString(R.string.updateFeedbacks));

        promo_popup.dismiss();

        loading.show();

        update_Feedbacks = new StringRequest(Request.Method.POST,getString(R.string.base_url)+getString(R.string.updateFeedbacks),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("response", "" +response);

                         if(response.contains("Success"))
                         {
//                             Toast.makeText(PreviousFeedbacks.this,"Feedback has been updated",Toast.LENGTH_LONG).show();
                             Toast toast = Toast.makeText(PreviousFeedbacks.this," Feedback has been updated ",Toast.LENGTH_LONG);
                             toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                             toast.show();
                             get_Feedbacks();
                         }else
                         {
//                             Toast.makeText(PreviousFeedbacks.this,"Feedback update failed",Toast.LENGTH_LONG).show();
                             Toast toast = Toast.makeText(PreviousFeedbacks.this," Feedback update failed ",Toast.LENGTH_LONG);
                             toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                             toast.show();
                         }

                        loading.dismiss();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String error_msg="";

                Log.e("error", "" + volleyError.toString());

                loading.dismiss();

                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured, Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error, Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error, Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured, Please try later";
                }

                Snackbar snackbar = Snackbar
                        .make(parent_layout, error_msg, Snackbar.LENGTH_LONG) ;
                snackbar.show();

            }
        }){
            @Override
            protected Map<String, String> getParams()
            {
                // TODO Auto-generated method stub

                Map<String, String> params = new HashMap<String, String>();
                params.put("id",""+clicked_id);
                params.put("user_feedback",changed_feedback);

                Log.e("params",""+params.toString());
                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        AppController.getInstance().getRequestQueue().add(update_Feedbacks);


    }

    public void show_popup(String id,String feedback_value)
    {
        value.setText(feedback_value);
        clicked_id=id;
        promo_popup.show();

    }


    public void delete_feedback(final int position)
    {

        Log.e("delete_feedback_url", "" +getString(R.string.base_url)+getString(R.string.deleteFeedabcks));

        loading.show();

        delete_Feedback = new StringRequest(Request.Method.POST,getString(R.string.base_url)+getString(R.string.deleteFeedabcks),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("response", "" +response);

                        if(response.contains("Success"))
                        {
//                            Toast.makeText(PreviousFeedbacks.this,"Feedback has been deleted",Toast.LENGTH_LONG).show();
                            Toast toast = Toast.makeText(PreviousFeedbacks.this," Feedback has been deleted ",Toast.LENGTH_LONG);
                            toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                            toast.show();
                            get_Feedbacks();
                        }else
                        {
//                            Toast.makeText(PreviousFeedbacks.this,"Feedback deletion failed",Toast.LENGTH_LONG).show();
                            Toast toast = Toast.makeText(PreviousFeedbacks.this," Feedback deletion failed ",Toast.LENGTH_LONG);
                            toast.getView().setBackgroundResource(R.color.colorPrimaryDark);
                            toast.show();
                        }

                        loading.dismiss();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String error_msg="";

                Log.e("error", "" + volleyError.toString());

                loading.dismiss();

                if (volleyError instanceof NoConnectionError)
                {
                    error_msg="No Internet Connection";
                    build_no_internet_msg();

                }else if(volleyError instanceof TimeoutError)
                {
                    error_msg="Server is taking too long to respond";

                } else if (volleyError instanceof AuthFailureError)
                {
                    error_msg="Error Occured, Please try later" ;

                } else if (volleyError instanceof ServerError)
                {
                    error_msg="Server Error, Please try later";

                } else if (volleyError instanceof NetworkError)
                {
                    error_msg="Network Error, Please try later";

                } else if (volleyError instanceof ParseError)
                {
                    error_msg="Error Occured, Please try later";
                }

                Snackbar snackbar = Snackbar
                        .make(parent_layout, error_msg, Snackbar.LENGTH_LONG) ;
                snackbar.show();

            }
        }){
            @Override
            protected Map<String, String> getParams()
            {
                // TODO Auto-generated method stub

                Map<String, String> params = new HashMap<String, String>();
                params.put("id",""+Seq.get(Integer.parseInt(position+"")));
                Log.e("params", "" + params.toString());

                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        AppController.getInstance().getRequestQueue().add(delete_Feedback);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
