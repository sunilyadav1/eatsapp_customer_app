package app.eatsapp;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by developer.nithin@gmail.com
 */
public class TermsList extends AppCompatActivity {

    ListView listView;
    TermsAdapter adapter;
    ArrayList<String> list = new ArrayList<String>();
    private Toolbar toolbar;
    Dialog loading;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppController.getInstance().set_task_bg(this);

        Log.e("Terms","1");

        setContentView(R.layout.termslist);

        loading = new Dialog(TermsList.this);
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading.setContentView(R.layout.loading);

        list.add("About Us");
        list.add("Terms of Service");
        list.add("Privacy Policy");
        list.add("Cancellations and Refunds Policy");
        list.add("Disclaimers and Indemnification");
        list.add("Contact Us");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Terms & Conditions");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView=(ListView)findViewById(R.id.listView);

        adapter =new TermsAdapter(TermsList.this,list);
        listView.setAdapter(adapter);

        adapter.notifyDataSetChanged();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                loading.show();
                switch (position)
                {
                    case 0: ;
                        openBrowser("https://eatsapp.in/page16.html#header2-44");
                    break;

                    case 1: ;
                        openBrowser("https://eatsapp.in/page14.html#header5-3n");
                        break;

                    case 2: ;
                        openBrowser("https://eatsapp.in/page14.html#header5-3o");
                        break;

                    case 3: ;
                        openBrowser("https://eatsapp.in/page14.html#header5-3p");
                        break;

                    case 4: ;
                        openBrowser("https://eatsapp.in/page14.html#header5-3q");
                        break;

                    case 5: ;
                        openBrowser("https://eatsapp.in/page15.html#header5-q");
                        break;

                        default:break;

                }
            }
        });
    }

    private void openBrowser(String url) {

        Toast.makeText(TermsList.this, "Please Wait !", Toast.LENGTH_SHORT).show();

        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        CustomTabsIntent customTabsIntent = builder.build();

        builder.setToolbarColor(ContextCompat.getColor(TermsList.this, R.color.white));
        builder.setShowTitle(false);
        customTabsIntent.intent.setPackage("com.android.chrome");
        customTabsIntent.launchUrl(TermsList.this, Uri.parse(url));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        loading.dismiss();
    }
}
