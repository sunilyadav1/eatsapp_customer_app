package app.eatsapp;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by developer.nithin@gmail.com
 */
public class NotificationAdapter extends BaseAdapter {
    Activity activity;

    ArrayList<String> Seq;
    ArrayList<String> Details;
    ArrayList<String> Time;
    LayoutInflater inflater;

    public NotificationAdapter(Activity activity, ArrayList<String> Seq, ArrayList<String> Details, ArrayList<String> Time) {
        // TODO Auto-generated constructor stub
        this.activity = activity;
        this.Seq = Seq;
        this.Details = Details;
        this.Time = Time;

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return Seq.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int id) {
        // TODO Auto-generated method stub
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2) {
        // TODO Auto-generated method stub
        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.notification_row, null);
        }

        TextView details = (TextView) convertView.findViewById(R.id.details);
        TextView time = (TextView) convertView.findViewById(R.id.time);


        details.setText(Details.get(position));
        time.setText(Time.get(position));


        return convertView;
    }

}