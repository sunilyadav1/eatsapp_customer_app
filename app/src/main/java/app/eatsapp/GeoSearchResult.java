package app.eatsapp;

/**
 * Created by developer.nithin@gmail.com
 */
import android.location.Address;

public class GeoSearchResult
{
    private Address address;

    public GeoSearchResult(Address address)
    {
        this.address = address;
    }

    public String getAddress()
    {

        String display_address = "";

        display_address += address.getAddressLine(0) + "\n";

        for (int i = 1; i < address.getMaxAddressLineIndex(); i++)
        {
            display_address += address.getAddressLine(i) + ", ";
        }

        display_address = display_address.substring(0, display_address.length() - 2);

        return display_address;
    }

    public String get_place_name()
    {
        return address.getAddressLine(0);
    }
    public String getLat()
    {
        String lat = address.getLatitude() + "";
        return lat;
    }

    public String getLong()
    {
        String lng = address.getLongitude() + "";
        return lng;
    }

    public String toString()
    {
        String display_address = "";

        if (address.getFeatureName() != null)
        {
            display_address += address + ", ";
        }

        for (int i = 0; i < address.getMaxAddressLineIndex(); i++)
        {
            display_address += address.getAddressLine(i);
        }

        return display_address;
    }
}