package app.eatsapp;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;

/**
 * Created by developer.nithin@gmail.com
 */
public class Crop extends AppCompatActivity {

    CropImageView cropImageView;
    Button ok,back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.crop);
        AppController.getInstance().set_task_bg(this);

        cropImageView=(CropImageView)findViewById(R.id.cropImageView);

        cropImageView.setImageBitmap(BitmapFactory.decodeFile(getIntent().getStringExtra("uri")));
        cropImageView.rotateImage(getCameraPhotoOrientation(Crop.this,Uri.parse( getIntent().getStringExtra("uri"))));

//        if(getIntent().getBooleanExtra("rotate",false))
//        {
//            cropImageView.rotateImage(90);
//        }

        ok=(Button)findViewById(R.id.ok);
        back=(Button)findViewById(R.id.back);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppController.getInstance().yourBitmap=cropImageView.getCroppedImage();

                setResult(RESULT_OK);
                finish();
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppController.getInstance().yourBitmap=cropImageView.getCroppedImage();

                setResult(RESULT_CANCELED);
                finish();
            }
        });



    }

    public int getCameraPhotoOrientation(Context context, Uri imageUri){
        int rotate = 0;
        try {
            context.getContentResolver().notifyChange(imageUri, null);
            File imageFile = new File(imageUri.getPath());

            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }

            Log.e("RotateImage", "Exif orientation: " + orientation);
            Log.e("RotateImage", "Rotate value: " + rotate);
        } catch (Exception e) {
            Log.e("Exception",  e.toString());

        }
        return rotate;
    }



}
